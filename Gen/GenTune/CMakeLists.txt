###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/GenTune
-----------
#]=======================================================================]

gaudi_add_module(GenTune
    SOURCES
        src/RivetAnalysisHandler.cpp
    LINK
        YODA::YODA
        Rivet::Rivet
        FastJet::FastJet
        LHCb::GenEvent
        Gaudi::GaudiAlgLib
        Gauss::pythia6forgauss
        Gauss::GeneratorsLib
)

gaudi_add_tests(QMTest)
