/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FlatNParticles.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/IRndmGenSvc.h"

// from Event 
#include "Event/GenHeader.h"

// local
#include "FlatNParticles.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FlatNParticles
//
// 2008-05-19 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( FlatNParticles )

//=============================================================================
// Initialize method
//=============================================================================
StatusCode FlatNParticles::initialize( ) {
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;

  // Initialize the number generator
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;  
  sc = m_flatGenerator.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if ( ! sc.isSuccess() ) 
    return Error( "Cannot initialize flat generator" ) ;    

  if ( m_minNumberOfParticles.value() > m_maxNumberOfParticles.value() ) 
    return Error( "Max number of particles < min number of particles !" ) ;
  else if ( 0 == m_maxNumberOfParticles.value() ) 
    return Error( "Number of particles to generate set to zero !" ) ;
    
  info() << "Number of particles per event chosen randomly between " 
         << m_minNumberOfParticles.value() << " and " << m_maxNumberOfParticles.value()
         << endmsg ;  
         
  return sc ;
}

//=============================================================================
// Compute the number of particles
//=============================================================================
unsigned int FlatNParticles::numberOfPileUp( ) {
  return ( m_minNumberOfParticles.value() + 
    (unsigned int) ( m_flatGenerator() * 
                     ( 1 + m_maxNumberOfParticles.value() - m_minNumberOfParticles.value() ) ) ) ;
}
