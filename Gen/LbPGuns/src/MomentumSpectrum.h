/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MomentumSpectrum.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_MOMENTUMSPECTRUM_H
#define PARTICLEGUNS_MOMENTUMSPECTRUM_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from ParticleGuns
#include "LbPGuns/IParticleGunTool.h"
#include "GaudiKernel/RndmGenerators.h"

#include "Event/GenHeader.h"

// from ROOT
#include "TH1.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TFile.h"

/** @class MomentumSpectrum MomentumSpectrum.h "MomentumSpectrum.h"
 *
 *  \brief Particle gun with momentum spectrum given by 3D histogram
 *
 * Options:
 * - PdgCodes: PDG numbers of particles to be produced by particle gun
 * - InputFile: Name of the file containing the histogram
 * - HistogramPath: Path to the histogram in the file
 *
 *  @author Dan Johnson (adapted from Michel De Cian)
 *  @date   2014-02-03
 */
class MomentumSpectrum : public extends<GaudiTool, IParticleGunTool> {
 public:

  using extends::extends;

  /// Initialize particle gun parameters
  StatusCode initialize() override;

  /// Generation of particles
  void generateParticle( Gaudi::LorentzVector & momentum ,
                         Gaudi::LorentzVector & origin ,
                         int & pdgId ) override;

  /// Print counters
  void printCounters( ) override { ; } ;

 private:
  /// Pdg Codes of particles to generate (Set by options)
  Gaudi::Property<std::vector<int>> m_pdgCodes{this,"PdgCodes",{},"Pdg Codes of particles to generate"};

  /// Masses of particles to generate
  std::vector<double>      m_masses;

  /// Names of particles to generate
  std::vector<std::string> m_names;

  /// Flat random number generator
  Rndm::Numbers m_flatGenerator ;

  /// Name of input file
  Gaudi::Property<std::string> m_inputFileName{this,"InputFile","","Name of input file"};

  /// Path of histogram in input file
  Gaudi::Property<std::string> m_histoPath{this,"HistogramPath","","Path of histogram in input file"};

  /// Variables used to bin the histogram
  Gaudi::Property<std::string> m_binningVars{this,"BinningVariables","","Variables used to bin the histogram"};

  /// Save run and event number
  bool newEvent(const LHCb::GenHeader* evt);
  longlong m_runnumber = -1;
  longlong m_evtnumber = -1;

  /// Histogram pointers (ready for TH2D if using ptpz binning or TH3D if using pxpypz binning
  TH1* m_hist{nullptr};
  TH2D* m_hist2d{nullptr};
  TH3D* m_hist3d{nullptr};


};

#endif // PARTICLEGUNS_MOMENTUMSPECTRUM_H
