###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/LbPGuns
-----------
#]=======================================================================]

gaudi_add_header_only_library(LbPGunsLib)

gaudi_add_module(LbPGuns
    SOURCES
        src/BeamShape.cpp
        src/Cosmics.cpp
        src/FixedMomentum.cpp
        src/FlatNParticles.cpp
        src/FlatPtRapidity.cpp
        src/GaussianTheta.cpp
        src/GenericGun.cpp
        src/MaterialEval.cpp
        src/MomentumRange.cpp
        src/MomentumSpectrum.cpp
        src/OTCosmic.cpp
        src/ParticleGun.cpp
        src/RichPatternGun.cpp
    LINK
        Gaudi::GaudiAlgLib
        LHCb::DetDescLib
        LHCb::GenEvent
        LHCb::LHCbKernel
        Gauss::GeneratorsLib
        Gauss::LbPGunsLib
)
