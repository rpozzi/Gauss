/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:00 GMT
#include "Mint/IDalitzEvent.h"

bool EqualEvent(const IDalitzEvent* a, const IDalitzEvent* b){

  if(0 == a && 0 == b) return true;
  if(0 == a && 0 != b) return false;
  if(0 != a && 0 == b) return false;

  if( a->eventPattern() != b->eventPattern()) return false;

  for(unsigned int i=0; i< a->eventPattern().size(); i++){
    if(a->p(i) != b->p(i)) return false;
  }

  return true;
}

//
