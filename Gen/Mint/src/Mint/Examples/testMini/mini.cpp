/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Mint/FitParameter.h"
#include "Mint/Minimiser.h"
#include "Mint/Minimisable.h"

using namespace MINT;

class toyFitFun : public Minimisable{
  FitParameter _fp;
public:
  toyFitFun() : _fp("x"){}
  double getVal(){return (_fp - 4.)*(_fp - 4.);}
};

int main(){

  toyFitFun f;
  Minimiser mini(&f);
  mini.doFit();

  return 0;
}
