/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IGETREALEVENT_WITHSMOOTHY_HH
#define IGETREALEVENT_WITHSMOOTHY_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:54 GMT

#include "Mint/IReturnRealWithSmoothy.h"
#include "Mint/IEventAccess.h"

namespace MINT{

template<typename EVENT>
class IGetRealEventWithSmoothy
  : virtual public IEventAccess<EVENT>
    , virtual public IGetRealEvent<EVENT>
    , virtual public IReturnRealWithSmoothy
  {
  public:
    //  virtual double getVal()=0;
    //  virtual double RealVal()=0;
    // virtual double SmootherLargerRealVal()=0;
    virtual ~IGetRealEventWithSmoothy(){}
  };
  
}//namespace MINT

#endif
//
