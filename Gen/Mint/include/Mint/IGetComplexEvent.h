/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IGETCOMPLEXEVENT_HH
#define IGETCOMPLEXEVENT_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:54 GMT

#include "Mint/IReturnComplex.h"
#include "Mint/IEventAccess.h"
//#include <complex>

namespace MINT{

template<typename EVENT>
class IGetComplexEvent 
: virtual public IEventAccess<EVENT>
, virtual public IReturnComplex
{
 public:
  //  virtual double getVal()=0;
  //  virtual std::complex<double> ComplexVal()=0;
};

}//namespace MINT
#endif
//
