/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MergedEventsFilter.h,v 1.1 2008-05-06 08:27:55 gcorti Exp $
#ifndef GENERATORS_MERGEDEVENTSFILTER_H
#define GENERATORS_MERGEDEVENTSFILTER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

class IFullGenEventCutTool;

/** @class MergedEventsFilter MergedEventsFilter.h component/MergedEventsFilter.h
 *
 *
 *  @author Gloria CORTI
 *  @date   2008-04-30
 */
class MergedEventsFilter : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;
  
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:

  Gaudi::Property<std::string> m_hepMCEventLocation{this,"HepMCEventLocation",LHCb::HepMCEventLocation::Default,"Input TES for HepMC events"} ;    
  Gaudi::Property<std::string> m_genCollisionLocation{this,"GenCollisionLocation",LHCb::GenCollisionLocation::Default,"Input TES for GenCollisions"} ;  

  Gaudi::Property<std::string> m_fullGenEventCutToolName{this,"FullGenEventCutTool","","Name of event cut tool"}; 
  IFullGenEventCutTool* m_fullGenEventCutTool{nullptr};     ///< Pointer to event cut tool

};
#endif // GENERATORS_MERGEDEVENTSFILTER_H
