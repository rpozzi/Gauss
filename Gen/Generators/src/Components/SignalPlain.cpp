/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "SignalPlain.h"

// from Gaudi

// Event
#include "Event/HepMCEvent.h"
#include "Event/GenCollision.h"
#include "Event/GenFSR.h"
#include "Event/GenCountersFSR.h"

// Kernel
#include "MCInterfaces/IGenCutTool.h"
#include "MCInterfaces/IDecayTool.h"

// from Generators
#include "Generators/IProductionTool.h"
#include "GenEvent/HepMCUtils.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SignalPlain
//
// 2005-08-18 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( SignalPlain )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SignalPlain::SignalPlain( const std::string& type, const std::string& name,
                          const IInterface* parent )
  : Signal( type, name , parent ) { ; }

//=============================================================================
// Destructor
//=============================================================================
SignalPlain::~SignalPlain( ) { ; }

//=============================================================================
// Generate Set of Event for Minimum Bias event type
//=============================================================================
bool SignalPlain::generate( const unsigned int nPileUp ,
                            LHCb::HepMCEvents * theEvents ,
                            LHCb::GenCollisions * theCollisions ) {
  StatusCode sc ;
  bool result = false ;
  // Memorize if the particle is inverted
  bool isInverted = false ;
  bool hasFlipped = false ;
  bool hasFailed = false ;
  LHCb::GenCollision * theGenCollision( 0 ) ;
  HepMC::GenEvent * theGenEvent( 0 ) ;
  LHCb::GenFSR* genFSR = nullptr;
  if(m_FSRName != ""){
    IDataProviderSvc* fileRecordSvc = svc<IDataProviderSvc>("FileRecordDataSvc", true);
    genFSR = getIfExists<LHCb::GenFSR>(fileRecordSvc, m_FSRName);
    if(!genFSR) warning() << "Could not find GenFSR at " << m_FSRName << endmsg;
  }

  for ( unsigned int i = 0 ; i < nPileUp ; ++i ) {
    prepareInteraction( theEvents , theCollisions , theGenEvent,
                        theGenCollision ) ;

    sc = m_productionTool -> generateEvent( theGenEvent , theGenCollision ) ;
    if ( sc.isFailure() ) Exception( "Could not generate event" ) ;

    if ( ! result ) {
      // Decay particles heavier than the particles to look at
      decayHeavyParticles( theGenEvent , m_signalQuark , m_signalPID ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

      // Check if one particle of the requested list is present in event
      ParticleVector theParticleList ;
      if ( checkPresence( m_pids , theGenEvent , theParticleList ) ) {

        // establish correct multiplicity of signal
        if ( ensureMultiplicity( theParticleList.size() ) ) {

          // choose randomly one particle and force the decay
          hasFlipped = false ;
          isInverted = false ;
          hasFailed  = false ;
          HepMC::GenParticle * theSignal =
            chooseAndRevert( theParticleList , isInverted , hasFlipped , hasFailed ) ;
          if ( hasFailed ) {
            HepMCUtils::RemoveDaughters( theSignal ) ;
            Error( "Skip event" ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
            return false ;
          }

          theParticleList.clear() ;
          theParticleList.push_back( theSignal ) ;

          if ( ! hasFlipped ) {

            m_nEventsBeforeCut++ ;
            if(genFSR) genFSR->incrementGenCounter(LHCb::GenCountersFSR::BeforeLevelCut, 1);

            // count particles in 4pi
            updateCounters( theParticleList , m_nParticlesBeforeCut ,
                            m_nAntiParticlesBeforeCut , false , false ) ;

            bool passCut = true ;
            if ( 0 != m_cutTool )
              passCut = m_cutTool -> applyCut( theParticleList , theGenEvent ,
                                               theGenCollision ) ;

            if ( passCut && ( ! theParticleList.empty() ) ) {
              if ( ! isInverted ) {
                m_nEventsAfterCut++ ;
                if(genFSR) genFSR->incrementGenCounter( LHCb::GenCountersFSR::AfterLevelCut, 1);
              }

              if ( isInverted ) {
                ++m_nInvertedEvents ;
                if(genFSR) genFSR->incrementGenCounter(LHCb::GenCountersFSR::EvtInverted, 1);
              }

              // Count particles passing the generator level cut with pz > 0
              updateCounters( theParticleList , m_nParticlesAfterCut ,
                              m_nAntiParticlesAfterCut , true , isInverted ) ;

              if ( m_cleanEvents ) {
                sc = isolateSignal( theSignal ) ;
                if ( ! sc.isSuccess() ) Exception( "Cannot isolate signal" ) ;
              }
              theGenEvent ->
                set_signal_process_vertex( theSignal -> end_vertex() ) ;

              theGenCollision -> setIsSignal( true ) ;

              // Count signal B and signal Bbar
              if ( theSignal -> pdg_id() > 0 ) {
                ++m_nSig ;
                if(genFSR) genFSR->incrementGenCounter( LHCb::GenCountersFSR::EvtSignal, 1);
              }
              else {
                ++m_nSigBar ;
                if(genFSR) genFSR->incrementGenCounter( LHCb::GenCountersFSR::EvtantiSignal, 1);
              }


              // Update counters
              GenCounters::updateHadronCounters( theGenEvent , m_bHadC ,
                                                 m_antibHadC , m_cHadC ,
                                                 m_anticHadC , m_bbCounter ,
                                                 m_ccCounter ) ;
              GenCounters::updateExcitedStatesCounters( theGenEvent ,
                                                        m_bExcitedC ,
                                                        m_cExcitedC ) ;

              if(genFSR) GenCounters::updateHadronFSR( theGenEvent, genFSR, "Acc");


              result = true ;
            } else {
              // event does not pass cuts
              HepMCUtils::RemoveDaughters( theSignal ) ;
            }
          } else {
            // has flipped:
            HepMCUtils::RemoveDaughters( theSignal ) ;
            theSignal -> set_pdg_id( - ( theSignal -> pdg_id() ) ) ;
          }
        }
      }
    }
  }

  return result ;
}

