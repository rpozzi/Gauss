/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENERATORS_TRIANGZSMEARVERTEX_H
#define GENERATORS_TRIANGZSMEARVERTEX_H 1


// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Event/BeamParameters.h"
#include "Generators/IVertexSmearingTool.h"

/** @class TriangZSmearVertex TriangZSmearVertex.h "TriangZSmearVertex.h"
 *
 *  Tool to smear vertex with triangular smearing along the z-axis and Gaussian
 *  smearing for the other axis (as in BeamSpotSmearVertex). Concrete
 *  implementation of a IVertexSmearingTool.
 *
 *  @author Saverio Mariani: samarian@cern.ch
 *  @date   2018-05-03
 */


/*
class Triangular : public Rndm::Flat {
public :  
  using Rndm::Flat::Flat; 

  double shoot() const { 
    return 0.5 * ( Rnd::Flat::Param::shoot() + Rndm::Flat::Param::shoot() ) ; 
   }

};
*/

class TriangZSmearVertex  : public GaudiTool, virtual public IVertexSmearingTool {
  
 public:
  /// Standard constructor
  TriangZSmearVertex ( const std::string& type, const std::string& name,
			   const IInterface* parent  ) ;

  /// Standard Destructor
  virtual ~TriangZSmearVertex( );

   /// Initialize method
   StatusCode initialize( ) override;
   //
   ///** Implements IVertexSmearingTool::smearVertex.
   // *  Does the same than BeamSpotSmearVertex::smearVertex for the x and y
   // *  direction but generates flat distribution for the z-coordinate of
   // *  the primary vertex.
   // */

   StatusCode smearVertex( LHCb::HepMCEvent * theEvent ) override;

 private:
  
 Gaudi::Property<std::string> m_beamParameters{this,"BeamParameters",
   LHCb::BeamParametersLocation::Default,"Location of BeamParameters"} ;

 Gaudi::Property<double> m_xcut{this, "Xcut", 4., "this times SigmaX"} ;

 Gaudi::Property<double> m_ycut{this, "Ycut", 4., "this times SigmaY"} ;

 Gaudi::Property<double> m_zmin{this, "ZMin", -541. * Gaudi::Units::mm, 
    "Minimum value for the z coordinate of the vertex"} ;

 Gaudi::Property<double> m_zmax{this, "ZMax", -341. * Gaudi::Units::mm, "Maximum value for the z coordinate of the vertex"} ;

 Gaudi::Property<int> m_zDir{this, "BeamDirection", 1, "Direction of the beam to take into account TOF vs nominal IP8,  can have \
 only values -1 or 1, or 0 to switch off the TOF and set time of \
 interaction to zero (default = 1, as for beam 1)"} ;

  
  Rndm::Numbers m_gaussDist ; ///< Gaussian random number generator
  Rndm::Numbers m_triangDist ; ///< Flat random number generator

  
};




#endif  //  GENERATORS_TRIANGZSMEARVERTEX_H
