/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FlatSmearVertex.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
// Include files 

// local
#include "FlatSmearVertex.h"

// from Gaudi
#include "GaudiKernel/IRndmGenSvc.h" 
#include "GaudiKernel/Vector4DTypes.h"
#include "GaudiKernel/Transform3DTypes.h"

// from Event
#include "Event/HepMCEvent.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FlatSmearVertex
//
// 2005-08-17 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( FlatSmearVertex )

//=============================================================================
// Initialize 
//=============================================================================
StatusCode FlatSmearVertex::initialize( ) {
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;
  
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  if ( m_xmin.value() > m_xmax.value() ) return Error( "xMin > xMax !" ) ;
  if ( m_ymin.value() > m_ymax.value() ) return Error( "yMin > yMax !" ) ;  
  if ( m_zmin.value() > m_zmax.value() ) return Error( "zMin > zMax !" ) ;
  
  sc = m_flatDist.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  
  std::string infoMsg = " applying TOF of interaction with ";
  if ( m_zDir.value() == -1 ) {
    infoMsg = infoMsg + "negative beam direction";
  } else if ( m_zDir.value() == 1 ) {
    infoMsg = infoMsg + "positive beam direction";
  } else if ( m_zDir.value() == 0 ) {
    infoMsg = " with TOF of interaction equal to zero ";
  } else {
    return Error("BeamDirection can only be set to -1 or 1, or 0 to switch off TOF");
  }

  info() << "Smearing of interaction point with flat distribution "
         << " in x, y and z " << endmsg;
  info() << infoMsg << endmsg;
  info() << " with " << m_xmin.value() / Gaudi::Units::mm 
         << " mm <= x <= " << m_xmax.value() / Gaudi::Units::mm << " mm, "
         << m_ymin.value() / Gaudi::Units::mm << " mm <= y <= " 
         << m_ymax.value() / Gaudi::Units::mm << " mm and "
         << m_zmin.value() / Gaudi::Units::mm << " mm <= z <= " 
         << m_zmax.value() / Gaudi::Units::mm << " mm." << endmsg;

  if ( ! sc.isSuccess() ) 
    return Error( "Could not initialize flat random number generator" ) ;

  release( randSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return sc ;
}
 
//=============================================================================
// Smearing function
//=============================================================================
StatusCode FlatSmearVertex::smearVertex( LHCb::HepMCEvent * theEvent ) {
  double dx , dy , dz , dt ;
  
  dx = m_xmin.value() + m_flatDist( ) * ( m_xmax.value() - m_xmin.value() ) ;
  dy = m_ymin.value() + m_flatDist( ) * ( m_ymax.value() - m_ymin.value() ) ;
  dz = m_zmin.value() + m_flatDist( ) * ( m_zmax.value() - m_zmin.value() ) ;
  dt = m_zDir.value() * dz/Gaudi::Units::c_light ;

  Gaudi::LorentzVector dpos( dx , dy , dz , dt ) ;
  
  HepMC::GenEvent::vertex_iterator vit ;
  HepMC::GenEvent * pEvt = theEvent -> pGenEvt() ;
  for ( vit = pEvt -> vertices_begin() ; vit != pEvt -> vertices_end() ; 
        ++vit ) {
    Gaudi::LorentzVector pos ( (*vit) -> position() ) ;
    pos += dpos ;

    if (m_tilt.value()) {
      Gaudi::LorentzVector negT( 0, 0, (fabs(m_zmax.value()-m_zmin.value())<1e-3 ? -m_zmax.value() : 0), 0 );
      Gaudi::LorentzVector posT( 0, 0, (fabs(m_zmax.value()-m_zmin.value())<1e-3 ? +m_zmax.value() : 0), 0 );
      Gaudi::RotationX rotX( m_zmax.value()==m_zmin.value() ? m_tiltAngle.value() : 0 );
      pos = pos + negT;
      pos = rotX(pos);
      pos = pos + posT;
    }
    (*vit) -> set_position( HepMC::FourVector( pos.x() , pos.y() , pos.z() ,
                                               pos.t() ) ) ;
  }

  return StatusCode::SUCCESS ;      
}

