/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: StandAloneDecayTool.h,v 1.3 2007-09-09 19:33:10 robbep Exp $
#ifndef GENERATORS_STANDALONEDECAYTOOL_H
#define GENERATORS_STANDALONEDECAYTOOL_H 1

// Include files
// from Gaudi
#include "Generators/Signal.h"


/** @class StandAloneDecayTool StandAloneDecayTool.h component/StandAloneDecayTool.h
 *  Class to generate decay with only the decay tool
 *
 *  @author Patrick Robbe
 *  @date   2006-04-18
 */
class StandAloneDecayTool : public Signal {
public:
  /// Standard constructor
  using Signal::Signal;
  
  StatusCode initialize( ) override;    ///< Tool initialization

  bool generate( const unsigned int nPileUp ,
                 LHCb::HepMCEvents * theEvents ,
                 LHCb::GenCollisions * theCollisions ) override;

private:
  double m_signalMass{0.} ; ///< Mass of the particle to decay

  Gaudi::Property<bool> m_inclusive{this,"Inclusive",false,"Generate inclusive decay"} ;  ///< Generate inclusive decay
};
#endif // GENERATORS_STANDALONEDECAYTOOL_H
