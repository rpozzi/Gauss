/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HISTOSMEARVERTEX_H
#define HISTOSMEARVERTEX_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include "Generators/IVertexSmearingTool.h"
#include "TH3.h"

/** @class SmearVertex SmearVertex.h "SmearVertex.h"
 *
 *  Tool to smear vertex with external input distribution.
 *  Concrete implementation of a IVertexSmearingTool.
 *
 *  @author Dominik Mitzel
 *  @date   2014-08-02
 */
class HistoSmearVertex : public extends<GaudiTool,IVertexSmearingTool> {
 public:
  /// Standard constructor
  using extends::extends;

  /// Initialize method
  StatusCode initialize( ) override;

  /** Implements IVertexSmearingTool::smearVertex.
   */
  StatusCode smearVertex( LHCb::HepMCEvent * theEvent ) override;

 private:
  /// Direction of the beam to take into account TOF vs nominal IP8, can have
  /// only values -1 or 1, or 0 to switch off the TOF and set time of
  /// interaction to zero (default = 1, as for beam 1)
  Gaudi::Property<int> m_zDir{this,"BeamDirection",0,
  "Direction of the beam to take into account TOF vs nominal IP8, can have \
   only values -1 or 1, or 0 to switch off the TOF and set time of\
   interaction to zero (default = 1, as for beam 1)"};
  TH3* m_hist;
  Gaudi::Property<std::string> m_inputFileName{this,"InputFileName","","Input file name"};
  Gaudi::Property<std::string> m_histoPath{this,"HistogramPath","","Histogram Path"};
};
#endif // PARTICLEGUNS_SMEARVERTEX_H
