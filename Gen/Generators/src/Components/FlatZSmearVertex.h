/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FlatZSmearVertex.h,v 1.5 2010-05-09 17:05:30 gcorti Exp $
#ifndef GENERATORS_FLATZSMEARVERTEX_H
#define GENERATORS_FLATZSMEARVERTEX_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/PhysicalConstants.h"

#include "Generators/IVertexSmearingTool.h"

// from Event
#include "Event/BeamParameters.h"

/** @class FlatZSmearVertex FlatZSmearVertex.h "FlatZSmearVertex.h"
 *
 *  Tool to smear vertex with flat smearing along the z-axis and Gaussian
 *  smearing for the other axis (as in BeamSpotSmearVertex). Concrete
 *  implementation of a IVertexSmearingTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-24
 */
class FlatZSmearVertex : public extends<GaudiTool,IVertexSmearingTool> {
 public:
  /// Standard constructor
  using extends::extends;

  /// Initialize method
  StatusCode initialize( ) override;

  /** Implements IVertexSmearingTool::smearVertex.
   *  Does the same than BeamSpotSmearVertex::smearVertex for the x and y
   *  direction but generates flat distribution for the z-coordinate of
   *  the primary vertex.
   */
  StatusCode smearVertex( LHCb::HepMCEvent * theEvent ) override;

 private:
  Gaudi::Property<std::string> m_beamParameters{this,"BeamParameters",LHCb::BeamParametersLocation::Default,"BeamParameters"} ; ///< Location of beam parameters (set by options)

  /// Number of sigma above which to cut for x-axis smearing (set by options)
  Gaudi::Property<double> m_xcut{this,"Xcut",4.,"this times SigmaX"}   ;

  /// Number of sigma above which to cut for y-axis smearing (set by options)
  Gaudi::Property<double> m_ycut{this,"Ycut",4.,"this times SigmaY"}   ;

  /// Minimum value for the z coordinate of the vertex (set by options)
  Gaudi::Property<double> m_zmin{this,"ZMin",-1500. * Gaudi::Units::mm,"ZMin"}   ;

  /// Maximum value for the z coordinate of the vertex (set by options)
  Gaudi::Property<double> m_zmax{this,"ZMax", 1500. * Gaudi::Units::mm,"ZMax"}   ;

  /// Direction of the beam to take into account TOF vs nominal IP8, can have
  /// only values -1 or 1, or 0 to switch off the TOF and set time of
  /// interaction to zero (default = 1, as for beam 1)
  Gaudi::Property<int> m_zDir{this,"BeamDirection",1,"BeamDirection"};

  Rndm::Numbers m_gaussDist ; ///< Gaussian random number generator

  Rndm::Numbers m_flatDist ; ///< Flat random number generator
};
#endif // GENERATORS_FLATZSMEARVERTEX_H
