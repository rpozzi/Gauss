/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FixedTarget.h,v 1.3 2005-12-31 17:32:01 robbep Exp $
#ifndef GENERATORS_COLLIDINGBEAMS_H
#define GENERATORS_COLLIDINGBEAMS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"

#include "Generators/IBeamTool.h"

// From Event
#include "Event/BeamParameters.h"

// Forward declarations
class IRndmGenSvc ;

/** @class FixedTarget FixedTarget.h "FixedTarget.h"
 *
 *  Tool to compute beam values with only one beam colliding to a fixed
 *  target. Concrete implementation of a IBeamTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-18
 */
class FixedTarget : public extends<GaudiTool,IBeamTool> {
 public:
  /// Standard constructor
  using extends::extends;
  
  /// Initialize method
  StatusCode initialize( ) override;

  /// Implements IBeamTool::getMeanBeams. See CollidingBeams::getMeanBeams
  void getMeanBeams( Gaudi::XYZVector & pBeam1 ,
                     Gaudi::XYZVector & pBeam2 ) const override;

  /// Implements IBeamTool::getBeams. See CollidingBeams::getBeams
  void getBeams( Gaudi::XYZVector & pBeam1 ,
                 Gaudi::XYZVector & pBeam2 ) override;

 private:
  Gaudi::Property<std::string> m_beamParameters{this,"BeamParameters",LHCb::BeamParametersLocation::Default,"BeamParameters"} ; ///< Location of beam parameters (set by options)

  Rndm::Numbers m_gaussianDist ; ///< Gaussian random number generator
};
#endif // GENERATORS_FIXEDTARGET_H
