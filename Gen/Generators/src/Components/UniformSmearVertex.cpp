/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: UniformSmearVertex.cpp,v 1.5 2008-07-24 22:05:38 robbep Exp $
// Include files 

// local
#include "UniformSmearVertex.h"

// from Gaudi
#include "GaudiKernel/IRndmGenSvc.h" 
#include "GaudiKernel/Vector4DTypes.h"

// from Event
#include "Event/HepMCEvent.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UniformSmearVertex
//
// 2007-09-07 : M.Ferro-Luzzi
// 2007-09-27 : G.Corti, use twopi from PhysicalConstants.h instead of 
//              hardcoded number
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( UniformSmearVertex )

//=============================================================================
// Initialize 
//=============================================================================
StatusCode UniformSmearVertex::initialize( ) {
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;
  
  if ( !(m_zmin.value() < m_zmax.value()) ) return Error( "zMin >= zMax !" ) ;
  if ( !(m_rmax.value() > 0.    ) ) return Error( "rMax <=  0  !" ) ;
  m_deltaz =  m_zmax.value() - m_zmin.value()       ;
  m_rmaxsq = m_rmax.value()*m_rmax.value()          ;

  IRndmGenSvc* randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = m_flatDist.initialize( randSvc , Rndm::Flat( 0.0 , 1.0 ) ) ;
  if ( ! sc.isSuccess() ) 
    return Error( "Could not initialize flat random number generator" ) ;

  std::string infoMsg = " applying TOF of interaction with ";
  if ( m_zDir.value() == -1 ) {
    infoMsg = infoMsg + "negative beam direction";
  } else if ( m_zDir.value() == 1 ) {
    infoMsg = infoMsg + "positive beam direction";
  } else if ( m_zDir.value() == 0 ) {
    infoMsg = " with TOF of interaction equal to zero ";
  } else {
    return Error("BeamDirection can only be set to -1 or 1, or 0 to switch off TOF");
  }

  info() << "Smearing of interaction point with flat distribution "
         << " in x, y and z " << endmsg;
  info() << infoMsg << endmsg;
  if( msgLevel(MSG::DEBUG) ) {
    debug() << " with r less than " << m_rmax.value() / Gaudi::Units::mm 
            << " mm." << endmsg ;
    debug() << " with z between " << m_zmin.value() / Gaudi::Units::mm 
            << " mm and " << m_zmax.value() / Gaudi::Units::mm << " mm." << endmsg ;
  } else {
    info() << " with r <= " << m_rmax.value() / Gaudi::Units::mm << " mm, "
           << m_zmin.value() / Gaudi::Units::mm << " mm <= z <= " 
           << m_zmax.value() / Gaudi::Units::mm << " mm." << endmsg;
  }

  release( randSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
 
  return sc ;
}

//=============================================================================
// Smearing function
//=============================================================================
StatusCode UniformSmearVertex::smearVertex( LHCb::HepMCEvent * theEvent ) {
  double dx , dy , dz, dt, rsq, r, th ;
  
  // generate flat in z, r^2 and theta:
  dz  = m_deltaz   * m_flatDist( ) + m_zmin.value() ;
  rsq = m_rmaxsq   * m_flatDist( )          ;
  th  = Gaudi::Units::twopi * m_flatDist( ) ;
  r   = sqrt(rsq) ;
  dx  = r*cos(th) ;  
  dy  = r*sin(th) ;
  dt  = m_zDir.value() * dz/Gaudi::Units::c_light ;
  Gaudi::LorentzVector dpos( dx , dy , dz , dt ) ;
  
  HepMC::GenEvent::vertex_iterator vit ;
  HepMC::GenEvent * pEvt = theEvent -> pGenEvt() ;
  for ( vit = pEvt -> vertices_begin() ; vit != pEvt -> vertices_end() ; 
        ++vit ) {
    Gaudi::LorentzVector pos ( (*vit) -> position() ) ;
    pos += dpos ;
    (*vit) -> set_position( HepMC::FourVector( pos.x() , pos.y() , pos.z() ,
                                               pos.t() ) ) ;
  }

  return StatusCode::SUCCESS ;      
}

