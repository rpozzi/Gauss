/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Usertran.cpp,v 1.1.1.1 2006-04-24 21:45:50 robbep Exp $
// access BcGen common Usertran
#include "LbBcVegPy/Usertran.h"

// set pointer to zero at start
Usertran::USERTRAN* Usertran::s_usertran =0;

// Constructor
Usertran::Usertran() : m_dummy( 0 ) , m_realdummy( 0. ) { }

// Destructor
Usertran::~Usertran() { }

// access ishower in common
int& Usertran::ishower() {
  init(); // check COMMON is initialized
  return s_usertran->ishower;
}

// access idpp in common
int& Usertran::idpp() {
  init(); // check COMMON is initialized
  return s_usertran->idpp;
}



