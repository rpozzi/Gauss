/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//--------------------------------------------------------------------------
//
//  X -> N Particles with the AMPGEN generator
//	Tim Evans 
//------------------------------------------------------------------------

// EvtGen
#include "EvtGenBase/EvtPatches.hh"
#include "EvtGenBase/EvtParticle.hh"
#include "EvtGenModels/EvtLbAmpGen.hh"
#include "EvtGenBase/EvtId.hh"
#include "EvtGenBase/EvtPDL.hh"
#include "EvtGenBase/EvtRandom.hh"
#include "EvtGenBase/EvtReport.hh"
#include "EvtGenBase/EvtRandom.hh"
#include "EvtGenBase/EvtVector4R.hh"
#include "EvtGenBase/EvtVector3R.hh"
#include "EvtGenBase/EvtRadCorr.hh"

// Gaudi
#include "GaudiKernel/System.h"
#include "GaudiKernel/GaudiException.h"

// C++
#include <string>
#include <fstream>
#include <sstream>
#include <sys/stat.h>
#include <dlfcn.h>
#include <iomanip> 
#include <complex.h>
#include <stdexcept>

std::string EvtLbAmpGen::getName(){
  return "LbAmpGen";
}

EvtDecayBase* EvtLbAmpGen::clone(){
  return new EvtLbAmpGen;
}

void EvtLbAmpGen::init(){
  // The argument is the decfile is the name of the shared library to load
  if ( getNArg() != 1 && getNArg() != 4 ){
    GaudiException( "Not enough arguments in DECFILE", "EvtGen",StatusCode::FAILURE );
  }
  if( getNArg() == 4 ) m_polarised = true; 
  std::cout << "LBAMPGEN called with nArgs = " << getNArg() << " lib = " << getArgStr(0) << std::endl; 

  m_handle = dlopen( ("libLb"+getArgStr(0)+".so").c_str(), RTLD_NOW );
  if( m_handle == nullptr ){
    GaudiException( "Library not found " + getArgStr(0), "EvtGen",StatusCode::FAILURE );
  }
  // The total amplitude is always referred to as FCN
  if( ! m_polarised && !m_fcn.set(m_handle, "FCN") ){
    GaudiException("Library does not contain FCN","EvtGen",StatusCode::FAILURE );
  }
  if( m_polarised && !m_fcnPol.set(m_handle, "FCN_extPol") ){
    GaudiException("Library does not contain FCN","EvtGen",StatusCode::FAILURE );
  }
  if( m_polarised ){
    m_px = getArg(1);
    m_py = getArg(2);
    m_pz = getArg(3);
    std::cout << "Polarised with p = " << m_px << " " << m_py << " " << m_pz << std::endl; 
  }
  // set the buffer size to 4* 4 vectors + 1 proper time. 
  m_evtBuffer.resize(4 * getNDaug() + 1);
}

void EvtLbAmpGen::initProbMax(){
  /* the probability can be normalised when generating the 
     source code, hence the max probability is always 1. */
  setProbMax(1.);
}

void rotateToZFrame( std::vector<double>& evt, const EvtVector4R& parent)
{
  EvtVector3R z_axis( parent.get(1), parent.get(2), parent.get(3) ); 
  EvtVector3R beam_axis(0,0,1);
  EvtVector3R y_axis = cross(beam_axis, z_axis); 
  EvtVector3R x_axis = cross(y_axis,z_axis);
  x_axis /= x_axis.d3mag();
  y_axis /= y_axis.d3mag();
  z_axis /= z_axis.d3mag();
  
  double gamma = parent.get(0) / parent.mass();
  double beta  = sqrt(1.-1./(gamma*gamma));
  for(size_t i = 0 ; i < evt.size()/4;++i)
  {
    double ex = evt[4*i+0];
    double ey = evt[4*i+1];
    double ez = evt[4*i+2];
    double eE = evt[4*i+3];
    // rotate 
    evt[4*i+0] = (x_axis.get(0) * ex + x_axis.get(1) * ey + x_axis.get(2) * ez );
    evt[4*i+1] = (y_axis.get(0) * ex + y_axis.get(1) * ey + y_axis.get(2) * ez );
    evt[4*i+2] = (z_axis.get(0) * ex + z_axis.get(1) * ey + z_axis.get(2) * ez );
    ez = evt[4*i+2];
    evt[4*i+2] = gamma * ( ez - beta * eE );
    evt[4*i+3] = gamma * ( eE - beta * ez );
  }
}

void EvtLbAmpGen::decay( EvtParticle *p){

  p->initializePhaseSpace(getNDaug(),getDaugs());
  /* We use the funny root-ordering for four momenta - should
     probably change this at some point, but quite non-trivial. */
  EvtVector4R total; 
  for( int i = 0 ; i < getNDaug() ; ++i){
    EvtVector4R p4 = p->getDaug(i)->getP4Lab();
    m_evtBuffer[4*i+0] = p4.get(1);
    m_evtBuffer[4*i+1] = p4.get(2);
    m_evtBuffer[4*i+2] = p4.get(3);
    m_evtBuffer[4*i+3] = p4.get(0);
    total += p4; 
  }
  if( m_polarised ) rotateToZFrame( m_evtBuffer, total);
  /* Put the lifetime in the event buffer for future proofing. 
     won't work naively as the accept-reject uses the same lifetime 
     multiple times. */
  m_evtBuffer[ 4*getNDaug() ] = p->getLifetime();  
  /* Get probability of event - PDFs assume particles, not antiparticles
     therefore, an additional minus sign is required 
     if generating anti particles in the P-odd amplitudes
     With assumption of no CPV in charm sector
     For systems with larger CPV, can include two different amplitudes. */
  double prob = m_polarised ? 
    m_fcnPol( &(m_evtBuffer[0]), p->getPDGId() > 0 ? 1 : -1, m_px, m_py, m_pz ) :
    m_fcn( &(m_evtBuffer[0]) ,p->getPDGId() > 0 ? 1 : -1 ) ;
  if( prob > 1 ){
    EvtGenReport(EVTGEN_ERROR,"EvtGen") << " prob > prob(max) !" << std::endl; 
  }
//  std::cout << "p(x) = " << prob << std::endl; 
  setProb( prob );
}

