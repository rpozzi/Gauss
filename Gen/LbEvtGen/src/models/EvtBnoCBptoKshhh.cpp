//--------------------------------------------------------------------------
//
// Copyright Information: See EvtGen/COPYRIGHT
//
// Module: EvtBnoCBptoKshhh
//
// Description: Model for generating flat phase space for the charmless 
//              region. Necessary due to limitations with the
//              combinatorial in the construction of the
//              invariant mass of the intermediate states.
//
// Modification history:
//
//    Pablo Baladrón     Dec 2019     Module created
//
//------------------------------------------------------------------------



#include "EvtGenBase/EvtParticle.hh"
#include "EvtGenBase/EvtPDL.hh"

#include "EvtGenModels/EvtBnoCBptoKshhh.hh"

// Constructor defining parameters from CKMfitter and PDG 2018
EvtBnoCBptoKshhh::EvtBnoCBptoKshhh() :
    _m2pi_ll(0.0),
    _m2pi_ul(1.86483),
    _m3pi_ll(0.0),
    _m3pi_ul(1.86965),
    _I(EvtComplex(1.0, 0.0)) 
{
}

void EvtBnoCBptoKshhh::init()
{
  // Check there are 0 or 4 arguments
  checkNArg(0,4);
  checkNDaug(4);

  // Assign arguments
  if (getNArg() == 4) {

    // Lower and upper 2h invariant mass limits    
    _m2pi_ll = getArg(0);
    _m2pi_ul = getArg(1);

    // Lower and upper 3h invariant mass limits
    _m3pi_ll = getArg(2);
    _m3pi_ul = getArg(3);

  }

}

void EvtBnoCBptoKshhh::decay(EvtParticle* p)
{
  // Generate 4-vectors
  p->initializePhaseSpace( getNDaug(), getDaugs() );

  // Apply charmless selection
  const EvtVector4R p1 = p->getDaug(0)->getP4();
  const EvtVector4R p2 = p->getDaug(1)->getP4();
  const EvtVector4R p3 = p->getDaug(2)->getP4();
  const EvtVector4R p4 = p->getDaug(3)->getP4();

  const double m12 = (p1+p2).mass();
  const double m14 = (p1+p4).mass();
  const double m23 = (p2+p3).mass();
  const double m34 = (p3+p4).mass();

  const double m123 = (p1+p2+p3).mass();
  const double m124 = (p1+p2+p4).mass();
  const double m134 = (p1+p3+p4).mass();
  const double m234 = (p2+p3+p4).mass();

  EvtComplex amp(0.0,0.0);

  if ( ((m12 > _m2pi_ll && m12 < _m2pi_ul) &&
	(m34 > _m2pi_ll && m34 < _m2pi_ul)) ||
       ((m14 > _m2pi_ll && m14 < _m2pi_ul) &&
	(m23 > _m2pi_ll && m23 < _m2pi_ul)) ||
       (m123 > _m3pi_ll && m123 < _m3pi_ul) ||
       (m124 > _m3pi_ll && m124 < _m3pi_ul) ||
       (m134 > _m3pi_ll && m134 < _m3pi_ul) ||
       (m234 > _m3pi_ll && m234 < _m3pi_ul) ) {

      amp = _I;

  }

  vertex(amp);

}
