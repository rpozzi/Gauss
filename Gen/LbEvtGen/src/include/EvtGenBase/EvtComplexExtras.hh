/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef EVTCOMPLEXEXTRAS_HH
#define EVTCOMPLEXEXTRAS_HH

#include <iostream>
#include <math.h>
#include "EvtGenBase/EvtConst.hh"
#include "EvtGenBase/EvtComplex.hh"

//extra functions using the GSL
EvtComplex atan(const EvtComplex& cmp);
EvtComplex chop(const EvtComplex& cmp);
EvtComplex log(const EvtComplex& cmp);
EvtComplex pow(const EvtComplex& cmp, const double index);
EvtComplex pow(const EvtComplex& cmp, const EvtComplex index);
EvtComplex sqrt(const EvtComplex& cmp);
EvtComplex sqrt_real(const double value);

std::istream& operator>>(std::istream& s, EvtComplex& c);
std::ostream& operator<<(std::ostream& s, const EvtComplex& c);

#endif
