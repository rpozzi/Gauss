/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef EVTBTOVLLPOLEMASS_H_
#define EVTBTOVLLPOLEMASS_H_

namespace qcd {
	double mb_pole(const double mb, const double scale = -1.0);//negative scale means use mb
}

#endif /*EVTBTOVLLPOLEMASS_H_*/
