/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LbEvtGen/EvtGenTool.h"
#include "LbEvtGen/EvtGenDecay.h"
#include "LbEvtGen/EvtGenInPhSpDecay.h"
#include "LbEvtGen/EvtGenDecayWithCutTool.h"
#include "LbEvtGen/ApplyPhotos.h"

// Declare various EvtGen tools
DECLARE_COMPONENT( EvtGenTool )
DECLARE_COMPONENT( EvtGenDecay )
DECLARE_COMPONENT( EvtGenInPhSpDecay )
DECLARE_COMPONENT( EvtGenDecayWithCutTool )
DECLARE_COMPONENT( ApplyPhotos )
