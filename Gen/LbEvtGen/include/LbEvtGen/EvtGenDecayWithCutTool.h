/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// from Generators
#include "LbEvtGen/EvtGenDecay.h"

class IGenCutTool;

/** @class EvtGenDecayWithCutTool EvtGenDecayWithCutTool.h "EvtGenDecayWithCutTool.h"
 *
 *  Does everything just like EvtGenDecay except it has a cut tool and overwrite the
 *  function used to generate the signal decay. It invokes EvtGenDecay::generateSignalDecay
 *  on the particle until the decay passes the cut.
 *
 *  ASSUMPTION:
 *  This obviously only makes sense if the cuttool only checks for things that depend only
 *  on the decay itself, i.e. some invariant mass of a number of decay products.
 *
 *  If the applied cut is independent of the signal particle frame, this should not bias the produced
 *  distributions in any way.
 *
 *  WARNING:
 *  While the cut tool needs to implement the usual cut tool interface, all but the
 *  first argument of applyCut(...) are passed a nullptr.
 *
 *  @author Dominik Muller
 *  @date   2018-3-12
 */
class EvtGenDecayWithCutTool : public EvtGenDecay
{
public:
  /// Standard constructor
  using EvtGenDecay::EvtGenDecay;

  /** Initialize method.
   *  Runs the EvtGenDecay initialization and gets the cut tool.
   */
  StatusCode initialize() override;

  /// Implements IDecayTool::generateSignalDecay
  // Runs EvtGenDecay::generateSignalDecay until a decay passes the provided cut.
  StatusCode generateSignalDecay( HepMC::GenParticle* theMother, bool& flip ) const override;

private:
  Gaudi::Property<std::string> m_cutToolName{this,"CutTool", "","Name of the cut tool to be applied at each decay attempt."}; ///< Name of the cut tool to be applied at each decay attempt.

  IGenCutTool* m_cutTool = nullptr;
};
