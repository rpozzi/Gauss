/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBSTARLIGHT_STARLIGHTPRODUCTION_H
#define LBSTARLIGHT_STARLIGHTPRODUCTION_H 1

using namespace std;

// Beamtool declaration.
class IBeamTool;
class upcEvent;
class starlight;
class inputParameters;

// starlight's randomGenerator does not have a virtual destructor...
//     StarLightProduction.h:30:7: warning: base class 'class randomGenerator'
//         has accessible non-virtual destructor [-Wnon-virtual-dtor]
// We know that our object derived from randomGenerator won't be destroyed by
// starlight, so simply ignore the warning.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
/**
 * Wrapper for the Gaudi random number generator.
 *
 * @class  GaudiRandomForStarLight
 * @file   StarLightProduction.h
 * @author Mingrui Zhao, Philip Ilten
 * @date   2017-02-02
 */
class GaudiRandomForStarLight : public randomGenerator {
public:

  /// Initialize the Gaudi random number generator.
  StatusCode initialize(IRndmGenSvc *svc);

  /// Finalize the Gaudi random number generator.
  StatusCode finalize();

  /// Flat random function used by StarLight, argument is not used.
  double Rndom(int) override;

 /// Destructor
  virtual ~GaudiRandomForStarLight( ) ;
  
private:

  /// Internal Gaudi random number generator.
  Rndm::Numbers m_rndm;
};
#pragma GCC diagnostic pop

/**
 * Production tool to generate events with StarLight.
 *
 * TO-DO.
 *
 * @class  StarLightProduction
 * @file   StarLightProduction.h
 * @author Mingrui Zhao, Philip Ilten
 * @date   2017-02-02
 */
class StarLightProduction : public extends<GaudiTool, IProductionTool> {
 public:
  typedef vector<string> CommandVector;

  /// Default constructor.
  using extends::extends;

  /**
   * Initialize the tool.
   *
   * First initialize the Gaudi tool and beam tool, then read the
   * default and user settings, and finally set the center-of-mass
   * energy.
   */
  StatusCode initialize() override;

  /// Finalize the tool.
  StatusCode finalize() override;

  /// Initialize the StarLight generator.
  StatusCode initializeGenerator() override;

  /// Generate an event.
  StatusCode generateEvent(HepMC::GenEvent *theEvent,
				   LHCb::GenCollision *theCollision) override;

  /**
   * Convert the StarLight HepEvt record to HepMC format.
   *
   * First dummy beams are added since the beam particles are not
   * available from StarLight. Then the particles are added and any
   * resonance is manually inserted of the process goes through a
   * resonance.
   */
  virtual StatusCode toHepMC (upcEvent &theUPCEvent, HepMC::GenEvent *theEvent);

  /**
   * Update particles properties.
   *
   * This only updates the particle properties which can be changed
   * within StarLight. The masses for 2112, 211, 111, 321, 11, 13, 15,
   * 9010221, 221, 331, 441, 225, 115, 335, 113, 223, 333, 443,
   * 100443, 553, 100553, and 200553 are set. Additionally, the widths
   * for 9010221, 221, 331, 441, 225, 115, 335, 113, 223, 333, 443,
   * 100443, 553, 100553, and 200553 are set. Finally, the spins for
   * 9010221, 221, 331, 441, 225, 115, and 335 set.
   **/
  void updateParticleProperties(const LHCb::ParticleProperty *thePP) override;

  /// This method is not implemented.
  void setStable(const LHCb::ParticleProperty *thePP) override;

  /// This method is not implemented.
  void savePartonEvent(HepMC::GenEvent *theEvent) override;

  /// This method is not implemented.
  void retrievePartonEvent( HepMC::GenEvent *theEvent) override;

  /// This method is not implemented.
  StatusCode hadronize(HepMC::GenEvent *theEvent,
			       LHCb::GenCollision *theCollision) override;

  /// This method is not implemented.
  void printRunningConditions() override;

  /// This method is not implemented.
  bool isSpecialParticle(const LHCb::ParticleProperty *thePP) const override;

  /// This method is not implemented.
  StatusCode setupForcedFragmentation(const int thePdgId) override;

  /// This method is not implemented.
  void turnOnFragmentation() override;

  /// This method is not implemented.
  void turnOffFragmentation() override;

protected:

  /// Parse the StarLight settings.
  StatusCode parseSettings(const CommandVector &settings);

private:

  GaudiRandomForStarLight m_rndm;     ///< Random number generator wrapper.
  CommandVector    m_defaultSettings{
"BEAM_1_Z = 0",
"BEAM_1_A = 0",
"BEAM_2_Z = 0",
"BEAM_2_A = 0",
"BEAM_1_GAMMA = 0",
"BEAM_2_GAMMA = 0",
"W_MAX = -1",
"W_MIN = -1",
"W_N_BINS = 500",
"RAP_MAX = 9",
"RAP_N_BINS =  2000",
"CUT_PT = 0",
"PT_MIN = 0",
"PT_MAX = 0",
"CUT_ETA = 0",
"ETA_MIN = -10",
"ETA_MAX = 10",
"PROD_MODE = 2",
"PROD_PID = 443013",
"BREAKUP_MODE = 5",
"INTERFERENCE = 0",
"IF_STRENGTH = 1.",
"INT_PT_MAX = 0.24",
"INT_PT_N_BINS = 1200"}; ///< StarLight default settings.
  Gaudi::Property<CommandVector>  m_userSettings{this,"Commands",CommandVector(),"List of commands to pass to StarLight."};    ///< StarLight user settings.
  Gaudi::Property<bool>           m_decays{this,"Decays",true,"Flag to allow StarLight to perform the decay (true)."};          ///< Allow StarLight to perform the decay.
  int              m_tries;           ///< Number of StarLight tries.
  starlight        m_generator;       ///< The standalone generator.
  inputParameters  m_pars;            ///< Generator parameters.
  int              m_res;             ///< Resonance for the process.
};

#endif // LBSTARLIGHT_STARLIGHTPRODUCTION_H
