###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import Generation 

Pythia8SoftQCDNonDiffractive = ["SoftQCD:all = off",
                                "SoftQCD:nonDiffractive = on"]

# Default value of pTHat threshold for Inclusive b simulation is 4.00GeV. To change the default value, you can uncomment the below line.
#pTHatValueInclusiveb = ["InclusivebHook:pTHatThreshold = 4.50"]
                                
gen = Generation("Generation")

gen.Inclusive.Pythia8Production.UserHooks += ["InclusivebHook"]
gen.Inclusive.Pythia8Production.Commands += Pythia8SoftQCDNonDiffractive
#gen.Inclusive.Pythia8Production.Commands += pTHatValueInclusiveb 

gen.SignalPlain.Pythia8Production.UserHooks += ["InclusivebHook"]
gen.SignalPlain.Pythia8Production.Commands += Pythia8SoftQCDNonDiffractive
#gen.SignalPlain.Pythia8Production.Commands += pTHatValueInclusiveb 

gen.SignalRepeatedHadronization.Pythia8Production.UserHooks += ["InclusivebHook"]
gen.SignalRepeatedHadronization.Pythia8Production.Commands += Pythia8SoftQCDNonDiffractive
#gen.SignalRepeatedHadronization.Pythia8Production.Commands += pTHatValueInclusiveb 

gen.Special.Pythia8Production.UserHooks += ["InclusivebHook"]
gen.Special.Pythia8Production.Commands += Pythia8SoftQCDNonDiffractive
#gen.Special.Pythia8Production.Commands += pTHatValueInclusiveb 




