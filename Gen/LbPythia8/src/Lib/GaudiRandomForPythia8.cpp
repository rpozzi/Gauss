/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GaudiRandomForPythia8.cpp,v 1.1.1.1 2007-07-31 17:02:19 robbep Exp $
// Include files

// local
#include "LbPythia8/GaudiRandomForPythia8.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GaudiRandomForPythia8
//
// 2007-07-31 : Arthur de Gromard
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
GaudiRandomForPythia8::GaudiRandomForPythia8( IRndmGenSvc * i , StatusCode &sc ) {
  sc = m_gaudiGenerator.initialize( i , Rndm::Flat( 0 , 1 ) ) ;
}
//=============================================================================
// Destructor
//=============================================================================
GaudiRandomForPythia8::~GaudiRandomForPythia8() {
  m_gaudiGenerator.finalize( ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
} 

//=============================================================================
