/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local.
#include "LbPythia8/LhcbHooks.h"

//-----------------------------------------------------------------------------
// Implementation file for class : pTDampingHook
//
// 2014-04-06 : Philip Ilten
//-----------------------------------------------------------------------------

namespace Pythia8 {
  double pTDampingHook::multiplySigmaBy(const SigmaProcess* sigmaProcessPtr,
				    const PhaseSpace* phaseSpacePtr, bool) {
    
    // Initialize.
    if (!isInit) {

      // Determine the damping factor.
      double eCM    = phaseSpacePtr->ecm();
      double pT0Ref = settingsPtr->parm("pTDampingHook:pT0Ref");
      double ecmRef = settingsPtr->parm("pTDampingHook:ecmRef");
      double ecmPow = settingsPtr->parm("pTDampingHook:ecmPow");
      double pT0    = pT0Ref * pow(eCM / ecmRef, ecmPow);
      pT20          = pT0 * pT0;

      // Determine alpha strong.
      double aSvalue = settingsPtr->parm("pTDampingHook:alphaSvalue");
      double aSorder = settingsPtr->mode("pTDampingHook:alphaSorder");
      int    aSnfmax = settingsPtr->mode("pTDampingHook:alphaSnfmax");
      alphaS.init(aSvalue, aSorder, aSnfmax, false);
      isInit = true;
    }

    // Calculate the weight.
    double wt = 1;
    
    // All 2 -> 2 hard QCD processes.
    int code = sigmaProcessPtr->code();
    if (sigmaProcessPtr->nFinal() == 2 &&
	((code >= 400 && code <= 600) || (code >= 111 && code <= 124))) {
      
      // pT scale of process. Weight pT^4 / (pT^2 + pT0^2)^2
      double pTHat     = phaseSpacePtr->pTHat();
      double pT2       = pTHat * pTHat;
      wt               = pow2(pT2 / (pT20 + pT2));
      
      // Renormalization scale and assumed alpha_strong.
      double Q2RenOld  = sigmaProcessPtr->Q2Ren();
      double alphaSOld = sigmaProcessPtr->alphaSRen();
      
      // Reweight to new alpha_strong at new scale.
      double Q2RenNew  = pT20 + Q2RenOld;
      double alphaSNew = alphaS.alphaS(Q2RenNew);
      wt              *= pow2(alphaSNew / alphaSOld);
    }
    
    // Return weight.
    return wt;
  }

  bool InclusivebHook::doVetoMPIStep(int , const Event& event)  {
  
  // Initialize.
    if (!isInit) {
      pTHatThreshold = settingsPtr->parm("InclusivebHook:pTHatThreshold");
      isInit = true;
    }

    hardB = false;
    pTHatVal = infoPtr->pTHat();

    for (int i = 0; i < event.size(); ++i) { 
     //looking for b quark at process level
      if(event[i].idAbs() == quarkID) hardB = true;  
    }
      
    //do not veto events with b or pTHat>= pTHat_threshold
    if(hardB || pTHatVal >= pTHatThreshold) return false;
    else return true;
  }

  // Access the event in the interleaved evolution
  bool InclusivebHook::doVetoPT(int , const Event& event) {
    showerB = false;
    for(int i =0; i<event.size(); i++){
      //looking for b quark at parton level
      if(event[i].idAbs()==quarkID) showerB = true;   
    }

    if (!showerB ) return true;
    else return false;
  }
}
