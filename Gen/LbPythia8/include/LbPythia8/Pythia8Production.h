/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBPYTHIA8_PYTHIA8PRODUCTION_H
#define LBPYTHIA8_PYTHIA8PRODUCTION_H 1

// LbPythia8.
#include "LbPythia8/GaudiRandomForPythia8.h"
#include "LbPythia8/BeamToolForPythia8.h"
#include "LbPythia8/LhcbHooks.h"

// Gaudi.
#include "GaudiAlg/GaudiTool.h"
#include "Generators/IProductionTool.h"
#include "Generators/ICounterLogFile.h"

// Pythia8.
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/LHAFortran.h"
#include "Pythia8Plugins/HepMC2.h"

// Event.
#include "Event/GenFSR.h"
/** 
 * Production tool to generate events with Pythia 8.
 *
 * Production of events using the Pythia 8 multi-purpose Monte Carlo generator
 * is provided by this class. THe Pythia 8 generator is a complete rewrite in
 * C++ of the previous FORTRAN Pythia 6 event generator. Currently the physics
 * manual for Pythia 8 is the same as for Pythia 6, arXiv:hep-ph/0603175. A
 * brief introduction to the specifics of Pythia 8 can be found in
 * arXiv:0710.3820 while a continually updated online manual can be found at
 * http://home.thep.lu.se/~torbjorn/pythia81html/Welcome.html.
 *
 * @class  Pythia8Production
 * @file   Pythia8Production.h
 * @author Arthur de Gromard
 * @author Philip Ilten
 * @date   2007-06-28
 */
class Pythia8Production : public extends<GaudiTool,IProductionTool> {
public:
  typedef std::vector<std::string> CommandVector ;
  
  /// Default constructor.
  using extends::extends;


  /**
   * Initialize the tool.
   *
   * Intitializes the tool, but leaves the actual Pythia 8 initialization to
   * initializeGenerator(). Here the Gaudi tool, random number generator,
   * beam tool, user hooks (if not already supplied), and XML log file are
   * initialized.
   */
  StatusCode initialize() override;

  /// Initialize the Pythia 8 generator.
  StatusCode initializeGenerator() override;

  /// Finalize the tool.
  StatusCode finalize() override;

  /// Generate an event.
  StatusCode generateEvent(HepMC::GenEvent* theEvent,
				   LHCb::GenCollision* theCollision) override;


  /**
   * Convert Pythia 8 event to HepMC format.
   *
   * This method converts the native output of Pythia 8 to the HepMC format
   * using the conversion method provided by Pythia 8. However the status codes
   * and vertex positions must be modified to match the LHCb standard. The
   * hard process information is also set.
   */
  StatusCode toHepMC(HepMC::GenEvent* theEvent,
		     LHCb::GenCollision* theCollision);

  /// Set particle stable.
  void setStable(const LHCb::ParticleProperty* thePP) override;

  /// Update a particle.
  void updateParticleProperties(const LHCb::ParticleProperty* thePP) override;

  /// Sets Pythia 8's "HadronLevel:Hadronize" flag to true.
  void turnOnFragmentation() override;

  /// Sets Pythia 8's "HadronLevel:Hadronize" flag to false.
  void turnOffFragmentation() override;

  /// Hadronize an event.
  StatusCode hadronize(HepMC::GenEvent* theEvent,
		LHCb::GenCollision* theCollision) override;

  /// Save the Pythia 8 event record.
  void savePartonEvent(HepMC::GenEvent* theEvent) override;

  /// Retrieve the Pythia 8 event record.
  void retrievePartonEvent(HepMC::GenEvent* theEvent) override;

  /**
   * Print the running conditions.
   *
   * If the message level is at MSG::DEBUG or above and the ListAllParticles
   * property is set to true all particle data is printed. If the message level
   * is MSG::VERBOSE then all settings are listed, whereas if MSG::DEBUG then
   * only the changed settings are printed. Note that this method duplicates
   * the built in functionality of Pythia 8 and should be removed.
   */
  void printRunningConditions() override;

  /**
   * Returns whether a particle has special status.
   *
   * If a particle has special status, then the particle cannot be modified.
   * This method checks if the particle is within the special particle set
   * built during construction of the class.
   */
  bool isSpecialParticle( const LHCb::ParticleProperty* thePP) const override;

  /**
   * Setup forced fragmentation.
   *
   * Used in conjuntion with the hadronize method. Here the Pythia 8 setting
   * of "PartonLevel:all" is set to off which stops both showers and
   * hadronization from being performed.
   */
  StatusCode setupForcedFragmentation(const int thePdgId) override;

  // The Pythia 8 members (needed externally).
  Pythia8::Pythia*    m_pythia{nullptr}; ///< The Pythia 8 generator.
  std::vector <Pythia8::UserHooks*> m_hooks;    ///< User hooks to veto events.
  Pythia8::LHAup*     m_lhaup{nullptr};  ///< User specified hard process.
  Pythia8::Event      m_event;  ///< The Pythia 8 event record.

  // Members needed externally.

  Gaudi::Property<std::string> m_beamToolName{this,"BeamToolName",
    "CollidingBeams","The beam tool to use."};///< The name of the beam tool.
  
protected:

  /**
   * Return the Pythia 8 ID.
   *
   * In most cases the Pythia 8 ID is equivalent to the PDG ID. However,
   * currently the LHCb PDG ID codes are out-dated so the following conversions
   * need to be made:
   * f_0(1370): 30221 -> 10221
   * Lambda_c(2625)+: 104124 -> 4124
   * Any particle that is not found within the Pythia 8 particle database is
   * assigned an ID of 0.
   */
  int pythia8Id(const LHCb::ParticleProperty* thePP);

  // Additional members.
  IBeamTool* m_beamTool{nullptr};                 ///< The Gaudi beam tool.
  BeamToolForPythia8* m_pythiaBeamTool{nullptr};  ///< The Pythia 8 beam tool.
  GaudiRandomForPythia8* m_randomEngine{nullptr}; ///< Random number generator.
  int m_nEvents{0};                         ///< Number of generated events.
  Gaudi::Property<CommandVector> m_userSettings{this,"Commands",
    {},"List of commands to pass to Pythia 8."}; ///< The user settings vector.
  Gaudi::Property<CommandVector> m_hookSettings{this,"UserHooks",
    {"pTDampingHook"},"List of Userhooks to pass to Pythia 8."}; ///< The UserHook settings vector.
  Gaudi::Property<std::string> m_tuningFile{this,"Tuning",
    "LHCbDefault.cmd","Name of the tuning file to use."};              ///< The global tuning file.
  Gaudi::Property<std::string> m_tuningUserFile{this,"UserTuning",
    "","Name of the user tuning file to use. Using the tune subrun will overwrite the default LHCb tune."};          ///< The user tuning file.
  Gaudi::Property<bool> m_validate_HEPEVT{this,"ValidateHEPEVT",
    false,"Flag to validate the Pythia 8 event record."};                ///< Flag to validate the event.
  Gaudi::Property<bool> m_listAllParticles{this,"ListAllParticles",
    false,"List all the particles being used by Pythia 8."};               ///< Flag to list all the particles.
  Gaudi::Property<bool> m_checkParticleProperties{this,"CheckParticleProperties",
    false,"Check the particle properties for consistency."} ;       ///< Flag to check particle properties.
  Gaudi::Property<bool> m_showBanner{this,"ShowBanner",
    false,"Flag to print the Pythia 8 banner at initialization."};                     ///< Flag to print the Pythia 8 banner.
  ICounterLogFile* m_xmlLogTool{nullptr};         ///< The XML log file. 
  std::set<unsigned int> m_special{
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  17,
  18,
  21,
  33,
  34,
  37,
  39,
  41,
  42,
  43,
  44,
  81,
  82,
  83,
  84,
  85,
  88,
  89,
  90,
  91,
  92,
  93,
  94,
  95,
  96,
  97,
  98,
  99,
  110,
  990,
  1101,
  1103,
  2101,
  2103,
  2201,
  2203,
  3101,
  3103,
  3201,
  3203,
  3301,
  3303,
  4101,
  4103,
  4201,
  4203,
  4301,
  4303,
  4401,
  4403,
  5101,
  5103,
  5201,
  5203,
  5301,
  5303,
  5401,
  5403,
  5501,
  5503,
  1000022,
  1000024,
  9900110,
  9900210,
  9900220,
  9900330,
  9900440,
  9902110,
  9902210
  }
;      ///< The set of special particles.
  std::set<int> m_bws;                   ///< Set of particles with a valid BW.
  /// Location where to store FSR counters (set by options)
  Gaudi::Property<std::string>  m_FSRName{this,"GenFSRLocation",
    LHCb::GenFSRLocation::Default,"Location where to store FSR counters"};
};

#endif // LBPYTHIA8_PYTHIA8PRODUCTION_H
