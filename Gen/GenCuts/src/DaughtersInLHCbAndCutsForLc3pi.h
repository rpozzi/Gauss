/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORLC3PI_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORLC3PI_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"

/** @class DaughtersInLHCbAndCutsForLc DaughtersInLHCbAndCutsForLc3pi.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb and with p and pt cuts on Lc daughters.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2012-02-07
 */
class DaughtersInLHCbAndCutsForLc3pi : public extends<GaudiTool,IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  /** Accept events with daughters in LHCb and p/pt cuts on Lc daughters
   *  (defined by min and max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndWithMinP
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Momentum Cut function
   *
   */
  bool momentumCut( const HepMC::GenParticle *, double ) const ;

  // Minimum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin",10 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for charged daughters"} ;

  // Maximum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for charged daughters"} ;

  // Minimum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin",5 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for neutral daughters"} ;

  // Maximum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for neutral daughters"} ;

  // cut value of Lc pt
  Gaudi::Property<double> m_lcptCut{this,"LcPtCuts",1500 * Gaudi::Units::MeV,"cut value of Lc pt"} ;

  // cut value on daughters min pt
  Gaudi::Property<double> m_daughtersptminCut{this,"DaughtersPtMinCut",150 * Gaudi::Units::MeV,"cut value on daughters min pt"} ;

  // cut value on daughters max pt
  Gaudi::Property<double> m_daughtersptmaxCut{this,"DaughtersPtMaxCut",150 * Gaudi::Units::MeV,"cut value on daughters max pt"} ;

  // cut value on daughters min p
  Gaudi::Property<double> m_daughterspminCut{this,"DaughtersPMinCut",1000 * Gaudi::Units::MeV,"cut value on daughters min p"} ;

 };
#endif // GENCUTS_DAUGHTERSINLHCDANDCUTSFORLC3PI_H
