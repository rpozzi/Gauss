/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DiLeptonInAcceptance.h,v 1.2 2009-08-10 13:15:56 tblake Exp $
#ifndef GENCUTS_DILEPTONINACCEPTANCE_H
#define GENCUTS_DILEPTONINACCEPTANCE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Generators
#include "MCInterfaces/IFullGenEventCutTool.h"


/** @class DiLeptonInAcceptance DiLeptonInAcceptance.h "DiLeptonInAcceptance.h"
 *  component/DiLeptonInAcceptance.h
 *
 *  Cut on events with two leptons in LHCb acceptance + minimum pT.
 *  and mass of lepton pair in sepcified range
 *
 *  Examples:
 *
 *  Generate a pair of opposite sign muons
 *  ( RequireOppositeSign = true; LeptonOneID = 13; LeptonOneID = 13 )
 *
 *  NB: this is the default
 *
 *  Generate all possible same and opposite sign combinations
 *  ( RequireOppositeSign = false; RequireSameSign = false; )
 *
 *  Generate same sign combinations
 *  ( RequireOppositeSign = false; RequireSameSign = true; )
 *
 *  Generate e+mu- and e-mu+ combinations
 *  ( LeptonOneID = 13; LeptonTwoID = 11; )
 *
 *  Generate e-mu- and e+mu+ combiantions
 *  ( LeptonOneID = 13; LeptonTwoID = 11; RequireOppositeSign = false; RequireSameSign = true; )
 *
 *
 *  Implementation of IFullGenEventCutTool.
 *
 *  @author T Blake
 *  @date   2009-08-10
 */

class DiLeptonInAcceptance : public extends<GaudiTool ,IFullGenEventCutTool> {
 public:
  /// Standard constructor
  using extends::extends;

  /** Apply cut on full event.
   *  Keep events with a pair of leptons in angular region around
   *  z axis (forward) and with a minimum pT.
   *  Implements IFullGenEventCutTool::studyFullEvent.
   */
  bool studyFullEvent( LHCb::HepMCEvents * theEvents ,
                       LHCb::GenCollisions * theCollisions ) const override;




 private:

  /// Check if the lepton is in the detector acceptance, has minimum p and pT.
  bool isInAcceptance( const HepMC::GenParticle* ,
                       const double, const double,
                       const double, const double ) const;


  /// Check if the combination of two leptons is allowed
  bool isCombination( const HepMC::GenParticle*,
                      const HepMC::GenParticle* ) const;


  /// Maximum value for theta angle of the first lepton (set by options)
  Gaudi::Property<double> m_thetaMaxLOne{this,"LeptonOneMaxTheta",400*Gaudi::Units::mrad,"Maximum value for theta angle of the first lepton"} ;

  /// Minimum value for theta angle of the first lepton (set by options)
  Gaudi::Property<double> m_thetaMinLOne{this,"LeptonOneMinTheta",10*Gaudi::Units::mrad,"Minimum value for theta angle of the first lepton"} ;

  /// Maximum value for theta angle of the first lepton (set by options)
  Gaudi::Property<double> m_thetaMaxLTwo{this,"LeptonTwoMaxTheta",400*Gaudi::Units::mrad,"Maximum value for theta angle of the first lepton"} ;

  /// Minimum value for theta angle of the first lepton (set by options)
  Gaudi::Property<double> m_thetaMinLTwo{this,"LeptonTwoMinTheta",10*Gaudi::Units::mrad,"Minimum value for theta angle of the first lepton"} ;

  /// Minimum pT of first lepton (set by options)
  Gaudi::Property<double> m_ptMinLOne{this,"LeptonOnePtMin",0*Gaudi::Units::GeV,"Minimum pT of first lepton"} ;

  /// Minimum p of first lepton (set by options)
  Gaudi::Property<double> m_pMinLOne{this,"LeptonOnePMin",0*Gaudi::Units::GeV,"Minimum p of first lepton"};

  /// Minimum pT of second lepton (set by options)
  Gaudi::Property<double> m_ptMinLTwo{this,"LeptonTwoPtMin",0*Gaudi::Units::GeV,"Minimum pT of second lepton"} ;

  /// Minimum p of second lepton (set by options)
  Gaudi::Property<double> m_pMinLTwo{this,"LeptonTwoPMin",0*Gaudi::Units::GeV,"Minimum p of second lepton"};

  /// Di Lepton mass range - Lower (set by options)
  Gaudi::Property<double> m_minMass{this,"MinMass",0*Gaudi::Units::GeV,"Di Lepton mass range - Lower"};

  /// Di Lepton mass range - Upper (set by options)
  Gaudi::Property<double> m_maxMass{this,"MaxMass",100*Gaudi::Units::GeV,"Di Lepton mass range - Upper"};

  /// Allowed lepton - first lepton (set by options)
  Gaudi::Property<int> m_leptonOnePDG{this,"LeptonIDOne",13,"Allowed lepton - first lepton"} ;

  /// Allowed lepton - second lepton (set by options)
  Gaudi::Property<int> m_leptonTwoPDG{this,"LeptonIDTwo",13,"Allowed lepton - second lepton"} ;

  /// Only allow decays with opposite sign leptons
  Gaudi::Property<bool> m_oppSign{this,"RequireOppositeSign",true,"Only allow decays with opposite sign leptons"};

  /// Only allow decays with same sign leptons. Opposite sign leptons take precedence.
  Gaudi::Property<bool> m_sameSign{this,"RequireSameSign",false,"Only allow decays with same sign leptons. Opposite sign leptons take precedence."};

  /// Preselects the events applying a cut on the doca of the two tracks
  Gaudi::Property<bool>   m_PreselDoca{this,"PreselDoca",false,"Switch to preselects the events applying a cut on the doca of the two tracks"};
  Gaudi::Property<double> m_docaCut{this,"DocaCut",0.4*Gaudi::Units::mm,"Preselects the events applying a cut on the doca of the two tracks"};

  /// Preselect the events applying a cut on the product of the two transv. momenta
  Gaudi::Property<bool> m_PreselPtProd{this,"PreselPtProd",false,"Switch to reselect the events applying a cut on the product of the two transv. momenta"};
  Gaudi::Property<double> m_ptprodMinCut{this,"PtProdMinCut",1*Gaudi::Units::GeV*1*Gaudi::Units::GeV,"Preselect the events applying a minimum cut on the product of the two transv. momenta"};
  Gaudi::Property<double> m_ptprodMaxCut{this,"PtProdMaxCut",4*Gaudi::Units::GeV*4*Gaudi::Units::GeV,"Preselect the events applying a maximum cut on the product of the two transv. momenta"};

};

#endif // GENCUTS_DILEPTONINACCEPTANCE_H
