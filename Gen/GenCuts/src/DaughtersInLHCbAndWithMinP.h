/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DaughtersInLHCbAndWithMinP.h,v 1.4 2008-05-29 14:21:59 gcorti Exp $
#ifndef GENCUTS_DAUGHTERSINLHCBAndWithMinP_H
#define GENCUTS_DAUGHTERSINLHCBAndWithMinP_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"
#include "GaudiKernel/Transform4DTypes.h"

/** @class DaughtersInLHCbAndWithMinP DaughtersInLHCbAndWithMinP.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb AndWithMinP.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Alex Shires
 *  @date   2011-03-02
 */
class DaughtersInLHCbAndWithMinP : public extends<GaudiTool,IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;
  /** Accept events with daughters in LHCb AndWithMinP (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndWithMinP
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Momentum Cut function
   *
   */
  bool momentumCut( const HepMC::GenParticle *, double ) const ;

  // Minimum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin",10 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for charged daughters"} ;

  // Maximum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for charged daughters"} ;

  // Minimum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin",5 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for neutral daughters"} ;

  // Maximum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for neutral daughters"} ;

  //min value of momentum
  Gaudi::Property<double> m_minMuonP{this,"MinMuonP",3000 * Gaudi::Units::MeV,"min value of muon momentum"};
  Gaudi::Property<double> m_minTrackP{this,"MinTrackP",1000 * Gaudi::Units::MeV,"min value of track momentum"};
};
#endif // GENCUTS_DAUGHTERSINLHCBAndWithMinP_H
