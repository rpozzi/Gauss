/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// =============================================================================
#ifndef GENERICGENCUTTOOL_H 
#define GENERICGENCUTTOOL_H 1
// =============================================================================
// Include files 
// =============================================================================
// GaudiKernel
// =============================================================================
#include "GaudiKernel/StatEntity.h"
// =============================================================================
// GaudiAlg 
// =============================================================================
#include "GaudiAlg/GaudiHistoTool.h"
// =============================================================================
// AIDA 
// =============================================================================
#include "AIDA/IHistogram2D.h"
// =============================================================================
// Generators 
// =============================================================================
#include "MCInterfaces/IGenCutTool.h"
// =============================================================================
// PartProp
// =============================================================================
#include "Kernel/iNode.h"
// =============================================================================
// LoKi
// =============================================================================
#include "LoKi/GenTypes.h"
#include "LoKi/Trees.h"
// =============================================================================
namespace 
{
  // ===========================================================================
  /// the invalid tree 
  const Decays::IGenDecay::Tree s_TREE = 
    Decays::Trees::Invalid_<const HepMC::GenParticle*>() ;
  /// the invalid node 
  const Decays::Node            s_NODE = Decays::Nodes::Invalid() ;
  /// the invalid function 
  const LoKi::GenTypes::GFun    s_FUNC = 
    LoKi::BasicFunctors<const HepMC::GenParticle*>::Constant ( -1 )  ;
  /// the invalid predicate 
  const LoKi::GenTypes::GCut    s_PRED = 
    LoKi::BasicFunctors<const HepMC::GenParticle*>::BooleanConstant ( false )  ;
  // ===========================================================================
} //                                                  end of anonymous namespace
namespace LoKi 
{
  // ===========================================================================
  /** @class   GenCutTool 
   *  
   *  Simple generic implementation of IGenCutTool interface to check if the 
   *  marked daughter are "good", e.g. in LHCbAcceptance  
   *
   *  @code
   *                                            
   *  from Cofigurable import LoKi__GenCutTool as GenCutTool 
   * 
   *  myalg.addTool ( GenCutTool , 'MyGenCutTool' ) 
   * 
   *  myAlg.MyGenCutTool.Decay = " [B0 => ^pi+ ^pi-]CC "
   *
   *  myAlg.MyGenCutTool.Cuts = { 
   *   '[pi+]cc'  : " in_range( 0.010 , GTHETA , 0.400 ) "
   *  }
   *  
   *  @endcode 
   *
   *  One can specify different crietria for different particles,
   *  ignore certain particles (e.g. neutrinos) etc...
   *
   *  @code
   *                                            
   *  from Cofigurable import LoKi__GenCutTool as GenCutTool 
   * 
   *  myalg.addTool ( GenCutTool , 'MyGenCutTool' ) 
   * 
   *  myAlg.MyGenCutTool.Decay = " [B0 -> ( K*(892)0 => ^K- ^pi+) gamma ]CC "
   *
   *  ## no cuts for gamma
   *  myAlg.MyGenCutTool.Cuts = { 
   *   '[pi+]cc'  : " in_range( 0.010 , GTHETA , 0.400 ) "
   *   '[K+]cc'   : " in_range( 0.010 , GTHETA , 0.300 ) "
   *  }
   *
   *  @endcode 
   *
   *  The tool also produce the generator cut efficiency in form of 2D-histogram,
   *  the exes and binning is specified via the confiuration.
   *  The histogram can be cofigured therough property:
   *  @code
   * 
   *  ## efficiency 2D-histogram: pt (in GeV) versus rapidity:
   *  myAlg.MyGenCutTool.XAxis= ( 0.0 , 10.0 , 20 , 'GPT/GeV') 
   *  myAlg.MyGenCutTool.YAxis= ( 2.0 ,  5.0 ,  6 , 'GY'     ) 
   *  
   *  ## efficiency 2D-histogram: pt ( in GeV) versus pseudo-rapidity:
   *  myAlg.MyGenCutTool.XAxis= ( 0.0 , 10.0 , 20 , 'GPT/GeV' ) 
   *  myAlg.MyGenCutTool.YAxis= ( 2.0 ,  5.0 ,  6 , 'GETA'    ) 
   *  
   *  @endcode 
   *  @see Gaudi::Histo1DDef  
   * 
   *  @see IGenCutTool
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   */
  class GenCutTool : 
    public extends<GaudiHistoTool, IGenCutTool>
  {
    // =========================================================================
  public:
    // =========================================================================
    /// Initialization
    StatusCode initialize () override ;
    /// Finalization 
    StatusCode finalize   () override ;
    /** Accept events with 'good' particles 
     *  @see IGenCutTool::applyCut.
     */
    virtual bool applyCut
    ( ParticleVector&              particles       , 
      const HepMC::GenEvent*    /* theEvent     */ , 
      const LHCb::GenCollision* /* theCollision */ ) const override ;
    // =========================================================================
  public:
    // =========================================================================
    /** standard constructor  
     */
    using extends::extends;
    
    // =========================================================================
  protected:
    // =========================================================================
    /// accept the particle 
    bool accept ( const HepMC::GenParticle* particle ) const ;
    // =========================================================================
  protected:
    // =========================================================================
    /// update-handler for the property "Decay" 
    void updateDescriptor ( Gaudi::Details::PropertyBase& /* p */ ) ;
    /// update-handler for the properties "Cuts","Preambulo","Factory" 
    void updateCuts       ( Gaudi::Details::PropertyBase& /* p */ ) ;
    /// update-handler for the properties "XAxis","YAxis"
    void updateHistos     ( Gaudi::Details::PropertyBase& /* p */ ) ;
    // =========================================================================
  private:
    // =========================================================================
    /// decode the decay descriptor 
    StatusCode decodeDescriptor ()  const ;
    /// decode cuts 
    StatusCode decodeCuts       ()  const ;
    /// decode histograms 
    StatusCode decodeHistos     ()  const ;
    // =========================================================================
  private:
    // =========================================================================
    struct TwoCuts
    {
      TwoCuts ( const Decays::iNode&         c1 ,
                const LoKi::GenTypes::GCuts& c2 ) ;
      TwoCuts () ;
      // =======================================================================
      /// the first  cut (particle selector) 
      Decays::Node          first   ; // the first  cut (particle selector) 
      /// the second cut (e.g. acceptance criteria) 
      LoKi::GenTypes::GCut  second  ; // the second cut (e.g. acceptance criteria) 
      /// The counter  
      StatEntity*           counter ; // the counter 
      // =======================================================================
    } ;
    /// vector of cuts 
    typedef std::vector<TwoCuts>  VTwoCuts ; // vector of cuts 
    // =========================================================================
  private:
    // =========================================================================
    /// construct preambulo string 
    std::string preambulo() const ;
    /// calculate the efficiency histo 
    StatusCode getEfficiency() ;
    // =========================================================================
  private:
    // =========================================================================
    /// the decay descriptor 
    Gaudi::Property<std::string> m_descriptor{this,"Decay","<Invaild-Decay-Descriptor>",
      &LoKi::GenCutTool::updateDescriptor,
      "The decay descriptor (with marked particles) to be tested"} ;   // the decay descriptor 
    /// the decay finder 
    mutable Decays::IGenDecay::Finder  m_finder{s_TREE}     ;   // the decay finder
    /// the LHCb-Acceptance cuts 
    typedef std::map<std::string,std::string>   CMap ;
    Gaudi::Property<CMap> m_cuts{this,"Cuts",
      {{"StableCharged"," in_range ( 0.010 , GTHETA , 0.400 ) "},
       {"Gamma"        ," in_range ( 0.005 , GTHETA , 0.400 ) "}},
       &LoKi::GenCutTool::updateCuts, "The map with selection criteria : { Node : Cuts } "} ; // the 'LHCb-Acceptance cuts
    /// the list of criteria 
    mutable VTwoCuts                   m_criteria   ;   // the list of criteria 
    // =========================================================================    
    /// the preambulo 
    Gaudi::Property<std::vector<std::string>>m_preambulo{this,"Preambulo",
    {"from GaudiKernel.SystemOfUnits import MeV,GeV,mm,cm"},
    &LoKi::GenCutTool::updateCuts, "Preambulo to be used for LoKi/Bender Hybrid factory"};// the preambulo 
    /// the factory 
    Gaudi::Property<std::string> m_factory{this,"Factory",
    "LoKi::Hybrid::GenTool:PUBLIC",
    &LoKi::GenCutTool::updateCuts, "The type/name for LoKi/Bender Hybrid factory"};// the factory 
    /// fill "efficiency" counters? 
    Gaudi::Property<bool> m_fill_counters{this,"FillCounters",true,"Fill efficiency counters (useful for debug)"} ; // fill counters?
    /// filter?
    Gaudi::Property<bool> m_filter{this,"Filter",false,"Use decay descriptor to filter decays?"}     ; // filter decays?
    // =========================================================================    
    /// the x-axis for the histogram 
    Gaudi::Property<Gaudi::Histo1DDef> m_xaxis{this,"XAxis",{0.0 , 10.0 , 20 , "GPT/GeV"},
      &LoKi::GenCutTool::updateHistos,"X-axis for efficiency histogram"} ; // the x-axis for the histogram 
    /// the y-axis for the histogram 
    Gaudi::Property<Gaudi::Histo1DDef> m_yaxis{this,"YAxis",{2.0 ,  5.0 ,  6 , "GY"},
      &LoKi::GenCutTool::updateHistos, "Y-axis for efficiency histogram"}; // the y-axis for the histogram 
    /// the x-value for histogram 
    mutable LoKi::GenTypes::GFun   m_x{s_FUNC}     ; // the x-value for histogram 
    /// the y-value for histogram 
    mutable LoKi::GenTypes::GFun   m_y{s_FUNC}     ; // the y-value for histogram 
    // =========================================================================
    /// the first  histogram ("flow")
    mutable AIDA::IHistogram2D* m_histo1{0} ;  // the first histogram ("flow")
    /// the second histogram ("accepted")
    mutable AIDA::IHistogram2D* m_histo2{0} ;  // the second histogram ("accepted")
    /// the third  histogram ("efficiency")
    mutable AIDA::IHistogram2D* m_histo3{0} ;  // the second histogram ("efficiency")
    // =========================================================================
    mutable bool m_update_decay  ;
    mutable bool m_update_cuts   ;
    mutable bool m_update_histos ;
    // =========================================================================
  } ;
  // ===========================================================================
} //                                                       end of namespace LoKi 
// =============================================================================
//                                                                       The END 
// =============================================================================
#endif // GENERICGENCUTTOOL_H
// =============================================================================
