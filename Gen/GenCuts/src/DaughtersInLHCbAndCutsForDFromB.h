/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORDFROMB_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORDFROMB_H 1

// Include files
#include "DaughtersInLHCbAndCutsForD.h"

/** @class DaughtersInLHCbAndCutsForDFromB DaughtersInLHCbAndCutsForDFromB.h
 *
 *  Tool to select Ds particles from a b-hadron with pT cuts
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2020-03-27
 */
class DaughtersInLHCbAndCutsForDFromB : public DaughtersInLHCbAndCutsForD, virtual public IGenCutTool {
public:
  /// Standard constructor
  DaughtersInLHCbAndCutsForDFromB( const std::string & type,
                                    const std::string & name,
                                    const IInterface * parent ) ;

  virtual ~DaughtersInLHCbAndCutsForDFromB() ; ///< Destructor

  /// Initialization
  virtual StatusCode initialize() override;

  /** Check that the signal Ds satisfies the cuts in
   *  SignalIsFromBDecay and DaughtersInLHCbAndCutsForD.
   *  Implements IGenCutTool::applyCut.
   */
  virtual bool applyCut( ParticleVector & theParticleVector,
                         const HepMC::GenEvent * theEvent,
                         const LHCb::GenCollision * theCollision ) const override;

private:
  /// From a b cut tool
  const IGenCutTool * m_fromBcuts;
} ;
#endif // GENCUTS_DAUGHTERSINLHCBANDCUTSFORDFROMB_H
