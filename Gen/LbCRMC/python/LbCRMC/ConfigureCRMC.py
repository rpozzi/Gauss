###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import MinimumBias, Generation, CRMCProduction,Pythia8Production
from Configurables import Special, Inclusive, SignalPlain, SignalRepeatedHadronization
from Configurables import FixedNInteractions, DaughtersInLHCbKeepOnlySignal
from Configurables import AsymmetricCollidingBeams
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import *

import math

def ConfigureCRMC( genAlgorithmName, collMode, sampleGenToolOpt):
    # Create the default settings.
    opts = {"Production" : "BEAMBEAM",
            "B1Momentum" : 5*SystemOfUnits.TeV,
            "B2Momentum" : 5*SystemOfUnits.TeV,
            "B1Particle" : 2212,
            "B2Particle" : 2212,
            "FixedTargetParticle" : 0, 
            "EvtType": 3000000}
    opts.update(sampleGenToolOpt)
    event_type=opts["EvtType"]

    gen = Generation( genAlgorithmName )
    gen.EventType = event_type

    configuration_commands = [
#        "fdpmjet pho dat $CRMC_TABS/phojet_fitpar.dat",
#        "fdpmjet dat $CRMC_TABS/dpmjet.dat",
#        "fqgsjet dat $CRMC_TABS/qgsjet.dat",
#        "fqgsjet ncs $CRMC_TABS/qgsjet.ncs",
#        "fqgsjetII03 dat $CRMC_TABS/qgsdat-II-03.lzma",
#        "fqgsjetII03 ncs $CRMC_TABS/sectnu-II-03",
        "fqgsjetII dat $CRMC_TABS/qgsdat-II-04.lzma",
        "fqgsjetII ncs $CRMC_TABS/sectnu-II-04",
        "fname check none",
        "fname initl $CRMC_TABS/epos.initl",
        "fname iniev $CRMC_TABS/epos.iniev",
        "fname inirj $CRMC_TABS/epos.inirj",
        "fname inics $CRMC_TABS/epos.inics",
        "fname inihy $CRMC_TABS/epos.inihy"
        ]

    gen.addTool( MinimumBias )
    gen.addTool( Special )
    gen.addTool( Inclusive )
    gen.addTool( SignalPlain )
    gen.addTool( SignalRepeatedHadronization )

    ## set the correct beam 2 momentum in case of asymmetric beam
    asymBeams=False;
    Beam2Momentum=opts["B2Momentum"]
    if  genAlgorithmName == "BeamGasGeneration" :
        Beam2Momentum=0
    if opts["B1Momentum"] != Beam2Momentum : asymBeams=True;

 
    genSampleTools = [ gen.MinimumBias, gen.Special, gen.Inclusive, gen.SignalPlain, gen.SignalRepeatedHadronization ]
    for genSampleTool in genSampleTools:        
        genSampleTool.ProductionTool = "CRMCProduction"
        genSampleTool.addTool( CRMCProduction )
        genSampleTool.CRMCProduction.SwitchOffEventTruncation = True
        genSampleTool.CRMCProduction.ProduceTables = False
        genSampleTool.CRMCProduction.AddUserSettingsToDefault = True
        genSampleTool.CRMCProduction.Commands += configuration_commands
        genSampleTool.CRMCProduction.Frame = "nucleon-nucleon"
        genSampleTool.CRMCProduction.CollisionMode = collMode
        genSampleTool.CRMCProduction.Beam1ID = opts["B1Particle"]
        genSampleTool.CRMCProduction.Beam2ID = opts["B2Particle"]
        genSampleTool.CRMCProduction.FixedTargetID = opts["FixedTargetParticle"]
        if asymBeams:
            genSampleTool.CRMCProduction.BeamToolName = "AsymmetricCollidingBeams"
            genSampleTool.CRMCProduction.addTool( AsymmetricCollidingBeams )
            genSampleTool.CRMCProduction.AsymmetricCollidingBeams.Beam2Momentum = Beam2Momentum


    if event_type/10000000 != 3: ## embedding signal generated with Pythia
        if "EMBED" not in opts["Production"]:
            log.warning("Embedded required in %s algorithm, but Gauss.Production is: %s (+EMBED missing)"%(genAlgorithmName, opts["Production"]))
                    
        embedgen = Generation ("EmbeddedGeneration")
        embedgen.addTool(Special)
        embedgen.addTool(Inclusive )
        embedgen.addTool(SignalPlain )
        embedgen.addTool(SignalRepeatedHadronization )

        maingen = Generation ()
        embedgen.CommonVertex = True
        embedgen.SampleGenerationTool = "Special"
        #
        embedgen.PileUpTool = "FixedNInteractions"
        embedgen.addTool( FixedNInteractions )
        embedgen.FixedNInteractions.NInteractions = 1
        #
        embedgen.Special.CutTool = "DaughtersInLHCbKeepOnlySignal"
        embedgen.Special.addTool( DaughtersInLHCbKeepOnlySignal )

        maingen.addTool(SignalPlain)
        maingen.addTool(SignalRepeatedHadronization)
        signal_pid = maingen.SignalPlain.getProp( 'SignalPIDList' )+maingen.SignalRepeatedHadronization.getProp('SignalPIDList')
        if len(signal_pid) > 0:
            embedgen.Special.DaughtersInLHCbKeepOnlySignal.SignalPID = math.fabs( signal_pid[ 0 ] )

        # Configure ProductionTool
        embedgen.Special.ProductionTool = "Pythia8Production/SignalPythia8"
        embedgen.Special.PileUpProductionTool = ""

        embedgen.Special.addTool( Pythia8Production , name = 'SignalPythia8' )
        embedgen.Special.SignalPythia8.Tuning = "LHCbDefault.cmd"
        # give correct beam parameters to Pythia8 in case of fixed target (Gas-Beam2 with embedding not supported)
        if  genAlgorithmName == "BeamGasGeneration" :
            embedgen.Special.SignalPythia8.Commands += [ 'Beams:pxB = 0.' ,
                                                         'Beams:pyB = 0.' ,
                                                         'Beams:pzB = 0.' ,
                                                         'Init:showProcesses = on' ]

        ## specific parameters for the generation of quarkonia or W/Z
        ## inclusive J/psi
        if event_type/1000000 == 24:
            embedgen.Special.SignalPythia8.Commands += [ 'SoftQCD:all=off' , 'Onia:all=on' ]
        ## other inclusive charmonium
        elif event_type/1000000 == 28:
            embedgen.Special.SignalPythia8.Commands += [ 'SoftQCD:all=off' , 'Onia:all=on' ]
        ## inclusive bottomonium
        elif event_type/1000000 == 18:
            signal_pid = gen.Special.UpsilonDaughtersInLHCb.getProp( 'SignalPID' )
            if signal_pid:
                embedgen.Special.DaughtersInLHCbKeepOnlySignal.SignalPID = math.fabs( signal_pid )
            embedgen.Special.SignalPythia8.Commands += [ 'SoftQCD:all=off' , 'Bottomonium:all=on' ]
        ## Z -> mu mu
        elif event_type == 42112000:
            embedgen.Special.DaughtersInLHCbKeepOnlySignal.SignalPID = 23
            embedgen.Special.SignalPythia8.Commands += [ 'SoftQCD:all=off' ,
                                                         "WeakSingleBoson:ffbar2gmZ = on", #Z0/gamma* production
                                                         "WeakZ0:gmZmode = 2", #Z0 only
                                                         "23:onMode = off", #turn it off
                                                         "23:onIfMatch = 13 -13" ] # only mu mu
        ## W -> mu nu_mu
        elif event_type == 42311000:
            embedgen.Special.DaughtersInLHCbKeepOnlySignal.SignalPID = 24
            embedgen.Special.SignalPythia8.Commands += [ 'SoftQCD:all=off' ,
                                                         "WeakSingleBoson:ffbar2W = on",
                                                         "24:onMode = off",
                                                         "24:onIfMatch = 13 -14",
                                                         "24:onIfMatch = -13 14" ]
        ## DY -> mu mu, mass > 2 GeV
        elif event_type == 42112010:
            embedgen.Special.DaughtersInLHCbKeepOnlySignal.SignalPID = 23
            embedgen.Special.SignalPythia8.Commands += [ 'SoftQCD:all=off' ,
                                                         "WeakSingleBoson:ffbar2gmZ = on",
                                                         "23:mMin = 2.",
                                                         "TimeShower:mMaxGamma = 2.",
                                                         "PhaseSpace:mHatMin = 2.",
                                                         "23:onMode = off",                # turn it off
                                                         "23:onIfMatch = 13 -13",          # decay to muon only
                                                         ]

        ## q g => gamma X
        elif event_type == 49000225:
            embedgen.Special.CutTool = ""
            embedgen.Special.SignalPythia8.Commands += [ 'SoftQCD:all=off' ,
                                                         'PromptPhoton:qg2qgamma = on' ,
                                                         'PhaseSpace:ptHatMin = 1.0' ,
                                                         'PhaseSpace:ptHatMax = 5.0']
            from Configurables import LoKi__FullGenEventCut, GenerationToSimulation
            embedgen.FullGenEventCutTool = "LoKi::FullGenEventCut/twoToTwoInAcc"
            embedgen.addTool( LoKi__FullGenEventCut, "twoToTwoInAcc" )
            twoToTwoInAcc = embedgen.twoToTwoInAcc
            twoToTwoInAcc.Code = "( ( count ( out1 ) > 0 ) & ( count ( out2 ) > 0 ) )"
            twoToTwoInAcc.Preambulo += [
                "from GaudiKernel.SystemOfUnits import GeV, mrad",
                "isGoodGamma = ( ('gamma' == GABSID ) & ( GTHETA < 400.0*mrad ) & ( GP > 1.0*GeV ) )",
                "FromMesonDecay = ( GNINTREE( ('gamma' == GABSID )  , HepMC.parents ) )", # gamma is from a hadron decay ?
                "out1 = ((isGoodGamma) & (FromMesonDecay==1))", # must be a good gamma not from a hadron "1 "1#"4 decay
                "out2 = ( ( GMESON ) & ( GTHETA < 400.0*mrad ) & ( GP > 1.0*GeV ) )"]
            GenerationToSimulation("GenToSim").KeepCode = "( ( GABSID == 21 ) )"

        ## q g => gamma q
        elif event_type == 49000227:
            embedgen.Special.CutTool = ""
            embedgen.Special.SignalPythia8.Commands += [ 'SoftQCD:all=off' ,
                                                         'PromptPhoton:qg2qgamma = on' ,
                                                         'PhaseSpace:ptHatMin = 0.9' ,
                                                         'PhaseSpace:ptHatMax = 11.0']
            from Configurables import LoKi__FullGenEventCut, GenerationToSimulation
            embedgen.FullGenEventCutTool = "LoKi::FullGenEventCut/twoToTwoInAcc"
            embedgen.addTool( LoKi__FullGenEventCut, "twoToTwoInAcc" )
            twoToTwoInAcc = embedgen.twoToTwoInAcc
            twoToTwoInAcc.Code = "( ( count ( out1 ) > 0 ) & ( count ( out2 ) > 0 ) )"
            twoToTwoInAcc.Preambulo += [
                "from GaudiKernel.SystemOfUnits import GeV, mrad",
                "inAcc = ( ( GTHETA < 400.0*mrad ) & ( GTHETA > 13.0*mrad ) & ( GP > 1.0*GeV ) )",
                "out1 = ( ( GBARCODE == 6 ) & ( GABSID > 0 ) & ( GABSID<7 ) & (inAcc) & ( GPT > 0.8*GeV ) )",  ##out1 == quark
                "out2 = ( ( GBARCODE == 5 ) & ( 'gamma' == GABSID ) & ( inAcc ) & ( GPT > 0.9*GeV ) )"]  ##out2 == gamma from gluon
            GenerationToSimulation("GenToSim").KeepCode = "( ( GABSID == 21 ) | ( GABSID < 7 ) | ( GBARCODE == 5 ) )"
            

        if asymBeams:
            embedgen.Special.SignalPythia8.BeamToolName = "AsymmetricCollidingBeams"
            embedgen.Special.SignalPythia8.addTool( AsymmetricCollidingBeams )
            embedgen.Special.SignalPythia8.AsymmetricCollidingBeams.Beam2Momentum = Beam2Momentum


    
