/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBCRMC_CRMCPRODUCTION_H
#define LBCRMC_CRMCPRODUCTION_H 1
#include <cmath>
#include "GaudiAlg/GaudiTool.h"

#include "GaudiKernel/RndmGenerators.h"
#include "Generators/IProductionTool.h"
#include "GaudiKernel/SystemOfUnits.h"
//epos
#include "LbCRMC/CRMCWrapper.h"
#include "GaudiKernel/Transform4DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"

// forward declarations
class CRMCWrapper ;
class IBeamTool ;


//-----------------------------------------------------------------------------
//  Interface file for class: CRMCProduction
//
//  2014-02-03 : Dmitry Popov
// 2015-08-25 : modification to go from EPOS Fortan to HepMC C++ (Laure Massacrier)
// 2020-03-27 : modification to get input particles from Gauss options and produce output in lab system (G.Graziani)
//-----------------------------------------------------------------------------


class CRMCProduction : public extends<GaudiTool,IProductionTool> {

public:
  typedef std::vector<std::string> CommandVector;

  using extends::extends;

  // Tool initialization
  StatusCode initialize() override;

  // Tool finilization
  StatusCode finalize() override;

  // Initialize the generator
  StatusCode initializeGenerator() override;

  // Generate an event
  StatusCode generateEvent(HepMC::GenEvent *theEvent, LHCb::GenCollision *theCollision) override;

  // This method is not implemented
  void setStable(const LHCb::ParticleProperty *thePP) override;

  // This method is not implemented
  void updateParticleProperties(const LHCb::ParticleProperty *thePP) override;

  // This method is not implemented
  bool isSpecialParticle(const LHCb::ParticleProperty *thePP) const override;

  // This method is not implemented
  void turnOnFragmentation() override;

  // This method is not implemented
  void turnOffFragmentation() override;

  // This method is not implemented
  StatusCode setupForcedFragmentation(const int thePdgId) override;

  // This method is not implemented
  StatusCode hadronize(HepMC::GenEvent *theEvent, LHCb::GenCollision *theCollision) override;

  // This method is not implemented
  void savePartonEvent(HepMC::GenEvent *theEvent) override;

  // This method is not implemented
  void retrievePartonEvent(HepMC::GenEvent *theEvent) override;

  // This method is not implemented
  void printRunningConditions() override;

  // Create temporary CRMC parameters file with default options
  StatusCode writeTempGeneratorParamFiles(CommandVector &options);

  // Printout out how the generator was configured to run
  void printOutGeneratorConfiguration();

private:
  CommandVector m_defaultSettings;  // Default settings
  Gaudi::Property<CommandVector> m_userSettings{this,"Commands",{},"User settings"};     // User settings

  Gaudi::Property<bool> m_printEvent{this,"PrintEvents",false,"Flag to print events on screen"};                // Flag to print events on screen
  int m_nEvents{0};                    // Generated events counter
  std::string m_tempParamFileName{""};  // Temporary file for CRMC parameters
  std::string m_tempParamFileName_backup{""};

  CRMCWrapper *m_CRMCEngine{nullptr};        // CRMC engine

  Gaudi::Property<bool> m_reseedCRMCRandGen{this,"ReSeedCRMCRandGenEveryEvt",false,"Use CRMC random generator over Gaudi"};         // Use CRMC random generator over Gaudi
  Rndm::Numbers m_random;           // Random number generator

  Gaudi::Property<std::string> m_beamToolName{this,"BeamToolName","CollidingBeams","Beam tool name"};       // Beam tool name
  IBeamTool *m_beamTool{nullptr};            // Beam tool

  Gaudi::LorentzRotation m_boostToLab ;  // boost matrix from CM to Lab system
  Gaudi::LorentzRotation m_CRMCToLab; // Lorentz matrix from CRMC to Lab system

  Gaudi::LorentzRotation RotateFromV1toV2( Gaudi::XYZVector &v1,
					   Gaudi::XYZVector &v2) {
    // build rotation matrix to bring v1 to v2 (apparenlty ROOT does not provide this function)
    Gaudi::XYZVector axis=(v1.Cross(v2)).Unit();
      //ROOT::Math::Cross(v1,v2).Unit();
    double cth=v1.Dot(v2)/std::sqrt(v1.Mag2()*v2.Mag2());
      //ROOT::Math::Dot(v1,v2) /( ROOT::Math::Mag(v1)*ROOT::Math::Mag(v2) );
    double sth=std::sqrt(1-cth*cth);
    Gaudi::LorentzRotation R;
    R.SetComponents(cth+std::pow(axis.X(),2)*(1-cth), axis.X()*axis.Y()*(1-cth)-axis.Z()*sth, axis.X()*axis.Z()*(1-cth)+axis.Y()*sth, 0.,
		    axis.X()*axis.Y()*(1-cth)+axis.Z()*sth, cth+std::pow(axis.Y(),2)*(1-cth), axis.Y()*axis.Z()*(1-cth)-axis.X()*sth, 0.,
		    axis.Z()*axis.X()*(1-cth)-axis.Y()*sth, axis.Y()*axis.Z()*(1-cth)+axis.X()*sth, cth+std::pow(axis.Z(),2)*(1-cth), 0.,
		    0., 0., 0., 1.);
    return R;
  }



  // CRMC configuration flags
  Gaudi::Property<double>      m_seed{this,"RandomSeed",-1,"Random number generator seed"}; // Random number generator seed
  Gaudi::Property<int>         m_HEModel{this,"HEModel",crmc_generators::EPOS_LHC,"Flag for the MC model"} ;// Flag for the MC model
  Gaudi::Property<std::string> m_collisionMode{this,"CollisionMode","BEAMBEAM","Type of collision ('BEAMBEAM', 'BEAMGAS' OR 'GASBEAM')"};
  Gaudi::Property<double>      m_minDecayLength{this,"MinDecayLength",1.0,"CRMC variable to determine stable particles"};          // CRMC variable to determine stable particles
  Gaudi::Property<std::string> m_paramFileName{this,"EPOSParamFileName","","Path to the CRMC parameters file"};      // Path to the CRMC parameters file
  Gaudi::Property<bool>        m_switchOffEventTruncation{this,"SwitchOffEventTruncation",true,"Switch off event truncation"};  // Switch off event truncation
  Gaudi::Property<bool>        m_produceTables{this,"ProduceTables",false,"Produce EPOS tables"}; //Produce EPOS tables
  Gaudi::Property<bool>        m_impactParameter{this,"ImpactParameter",false,"To define a user impact parameter range"}; //To define a user impact parameter range
  Gaudi::Property<double>      m_minImpactParameter{this,"MinImpactParameter",0.,"min value of the impact parameter"}; //min value of the impact parameter
  Gaudi::Property<double>      m_maxImpactParameter{this,"MaxImpactParameter",20.,"max value of the impact parameter"}; //max value of the impact parameter
  Gaudi::Property<bool>        m_addUserSettingsToDefault{this,"AddUserSettingsToDefault",false,"add user settings to default settings"}; //add user settings to default settings
  Gaudi::Property<std::string> m_frame{this,"Frame","nucleon-nucleon","give the frame for the outputs"}; //give the frame for the outputs
  Gaudi::Property<int>         m_beam1ID{this, "Beam1ID", 2212};   
  Gaudi::Property<int>         m_beam2ID{this, "Beam2ID", 2212};      
  Gaudi::Property<int>         m_fixedtargetID{this, "FixedTargetID", 0};

  // This are used internally and derived from the properties above
  int   m_projectileID;
  int   m_targetID;

  // Return the particle mass (GeV), selected from the ParticlePropertiesService by mapping CRMC id to PDG id
  double particleMass(int pID);
  // Return average nucleon energy (assigning the average nucleon mass in case of nucleus)
  double nucleonE(double px, double py,  double pz, int particleID);
  // Create default CRMC configuration file
  void createDefaultCRMCConfiguration();  

  // Fill HepMCEvent
  HepMC::GenEvent* FillHepMC(HepMC::GenEvent *theEvent);
  
};

#endif
