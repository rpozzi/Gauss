###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Sim/GaussMonitor
----------------
#]=======================================================================]

gaudi_add_module(GaussMonitor
    SOURCES
        src/BremVeloCheck.cpp
        src/CheckLifeTime.cpp
        src/CheckLifeTimeHepMC.cpp
        src/CheckLifeTimeMC.cpp
        src/DecayAnalysis.cpp
        src/EMGaussMoni.cpp
        src/GaussGenUtil.cpp
        src/GenMonitorAlg.cpp
        src/GeneratorAnalysis.cpp
        src/MCDecayCounter.cpp
        src/MCTruthFullMonitor.cpp
        src/MCTruthMonitor.cpp
        src/MonitorStepAction.cpp
        src/MonitorTiming.cpp
        src/ProductionAnalysis.cpp
        src/ProductionAsymmetry.cpp
    LINK
        LHCb::GenEvent
        LHCb::LoKiMCLib
        Run2Support::VeloDetLib
        Gauss::LoKiGenLib
        Gauss::GaussToolsLib
    GENCONF_OPTIONS
        --load-library=GaussToolsGenConfHelperLib
)
add_dependencies(GaussMonitor Gauss::GaussToolsGenConfHelperLib)
