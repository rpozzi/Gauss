/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ProductionAsymmetry.h,v 1.1 2007-05-16 17:33:29 gcorti Exp $
#ifndef PRODUCTIONASYMMETRY_H
#define PRODUCTIONASYMMETRY_H 1

// Include files
// from STL
#include <string>
#include "boost/lexical_cast.hpp"

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// from HepMC
#include "Event/HepMCEvent.h"

// from AIDA

/** @class ProductionAsymmetry ProductionAsymmetry.h
 *
 *  Based on Generator analysis algorithm
 *  Examines the production asymmetry of a list of input PIDs
 *  plots a few useful histograms known to strongly affect this
 *  asymmetry
 *
 *
 *  @author R. Lambert
 *  @date   2007-04-24
 */
class ProductionAsymmetry : public GaudiHistoAlg {
public:
  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:


private:
  Gaudi::Property<std::string>    m_dataPath{this,"Input",LHCb::HepMCEventLocation::Default,"Location of HepMC Events"};            ///< location of input data
  Gaudi::Property<double>         m_minEta{this,"MinEta",2.0,"Min pseudo rapidity acceptance"};              ///< Min pseudo rapidity acceptance
  Gaudi::Property<double>         m_maxEta{this,"MaxEta",4.9,"Max pseudo rapidity acceptance"};              ///< Max pseudo rapidity acceptance

  int            m_counter{0};             ///<counter of particles in acceptance
  int            m_nEvents{0};             ///<counter of events

  Gaudi::Property<std::string>    m_generatorName{this,"ApplyTo","" ,"specify the generator to study"};
  Gaudi::Property<std::string>    m_signalName{this,"Signal","B0 " ,"name of plots"};   ///< name of plots/graphs

  Gaudi::Property<std::vector<int>>  m_hepPID{this,"SignalPIDs",{},"list of PIDs to plot"};       ///< an option for the PID, which particles to look for.
  std::vector<LHCb::ParticleID> m_sPID;       ///<PIDs of signal particles filled from the option m_hepPID

  std::vector<int> m_counters;  ///counters of number of particles of signal types
  std::vector<int> m_asymcounters;  //counters of differences between particles and antiparticles
  std::vector<int> m_partcounters; //counters of particles and antiparticles




  // Location where to find HepMC event
  std::string m_inputData ;

  //plots for a given particle
  StatusCode someplots(const HepMC::GenParticle* plotme);

  //plots arrive with the name of the particle they were for


};
#endif // PRODUCTIONASYMMETRY_H
