/***********************************************************************************\
* (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/

// Include files
// local
#include "MakeG4UnknownIon.h"

// Kernel
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/IToolSvc.h"

// Geant4
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4IonTable.hh" 
#include "G4ProcessManager.hh"


// Declaration of the AlgTool Factory
DECLARE_COMPONENT( MakeG4UnknownIon )

//=============================================================================
// Add an ion to the G4ParticleTable using the info retrieved from G4IonTable.
//=============================================================================
void MakeG4UnknownIon::AddIonToG4Table(int pdgID) const {

  warning() << "The ion with pdgID " << pdgID << " is not present in G4ParticleTable" << endmsg;

  G4ParticleDefinition* newIon = G4IonTable::GetIonTable()->GetIon(pdgID);
  if(newIon) {
    debug() << newIon->GetParticleName() << " was added to G4ParticleTable" << endmsg;
    G4ParticleTable::GetParticleTable()->DumpTable(newIon->GetParticleName());
    G4ProcessManager* pm = newIon->GetProcessManager();
    if ( !pm ) warning() << "There is no process manager for " << newIon->GetParticleName() << endmsg;
    else pm->DumpInfo();
  }
  else warning() << "The ion with pdgID " << pdgID << 
                    " is not known to G4IonTable and will be discarded" << endmsg;

}



