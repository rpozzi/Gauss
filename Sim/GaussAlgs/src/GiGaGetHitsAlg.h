/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaGetHitsAlg.h,v 1.7 2007-01-12 15:23:42 ranjard Exp $
#ifndef GIGAGETHITSALG_H
#define GIGAGETHITSALG_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/// from Event
#include "Event/MCHit.h"
#include "Event/MCCaloHit.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichOpticalPhoton.h"
#include "Event/MCRichTrack.h"
#include "Event/MCRichSegment.h"


/** @class GiGaGetHitsAlg GiGaGetHitsAlg.h
 *
 *
 *  @author Witold Pokorski
 *  @author Gloria Corti
 *  @date   2002-08-13, last modified 2006-01-27
 */

class GiGaGetHitsAlg : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  // Print info for Silicon or Outer Tracker MCHits
  virtual void hitsTracker( const std::string det, const std::string location );

  // Print info for RICH detector: MCRichHits, Optical photons, segments and
  // tracks
  virtual void infoRICH( );

private:
  typedef std::vector<std::string> Addresses ;

  Gaudi::Property<std::string>    m_velohits{this,"VeloHits", LHCb::MCHitLocation::Velo,"TES path where to look for Velo hits"}    ;      ///< TES path where to look for Velo hits
  Gaudi::Property<std::string>    m_puvelohits{this,"PuVetoHits", LHCb::MCHitLocation::PuVeto,"TES path where to look for PileUp Veto hits"}  ;      ///< TES path where to look for PileUp Veto hits
  Gaudi::Property<std::string>    m_tthits{this,"TTHits", LHCb::MCHitLocation::TT,"TES path where to look for TT hits"}      ;      ///< TES path where to look for TT hits
  Gaudi::Property<std::string>    m_slhits{this,"SLHits", LHCb::MCHitLocation::SL,"TES path where to look for SL hits"}      ;      ///< TES path where to look for SL hits
  Gaudi::Property<std::string>    m_ithits{this,"ITHits", LHCb::MCHitLocation::IT,"TES path where to look for IT hits"}      ;      ///< TES path where to look for IT hits
  Gaudi::Property<std::string>    m_othits{this,"OTHits", LHCb::MCHitLocation::OT,"TES path where to look for OT hits"}      ;      ///< TES path where to look for OT hits
  Gaudi::Property<std::string>    m_fthits{this,"FTHits", LHCb::MCHitLocation::FT,"TES path where to look for FT hits"}      ;      ///< TES path where to look for FT hits
  Gaudi::Property<std::string>    m_uthits{this,"UTHits", LHCb::MCHitLocation::UT,"TES path where to look for UT hits"}      ;      ///< TES path where to look for UT hits
  Gaudi::Property<Addresses>      m_caloHits{this,"CaloHits",
    {LHCb::MCCaloHitLocation::Spd,
    LHCb::MCCaloHitLocation::Prs,
    LHCb::MCCaloHitLocation::Ecal,
    LHCb::MCCaloHitLocation::Hcal} ,"TES pathes where to look for Calo hits"};          ///< TES pathes where to look for Calo hits
  Gaudi::Property<std::string>    m_muonhits{this,"MuonHits", LHCb::MCHitLocation::Muon,"TES path where to look for Muon hits"}    ;      ///< TES path where to look for Muon hits
  Gaudi::Property<std::string>    m_richhits{this,"RichHits", LHCb::MCRichHitLocation::Default,"TES path where to look for RICH hits"}    ;      ///< TES path where to look for RICH hits
  Gaudi::Property<std::string>    m_richop{this,"RichOpticalPhotons", "","TES path where to look for Optical photons"}      ;      ///< TES path where to look for Optical photons
  Gaudi::Property<std::string>    m_richtracks{this,"RichTracks", "","TES path where to look for Rich tracks"}  ;      ///< TES path where to look for Rich tracks
  Gaudi::Property<std::string>    m_richsegments{this,"RichSegments", "","TES path where to look for Rich segments"};      ///< TES path where to look for Rich segments
  Gaudi::Property<std::string>    m_extraHits{this,"ExtraHits", "","Extra hits"};
  std::vector<std::string> m_caloDet{"Spd","Prs","Ecal","Hcal"}; ///< List of calorimeters detecotors

 

};
#endif // GIGAGETHITSALG_H
