/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PrintEventAlg.cpp,v 1.5 2007-01-12 15:23:42 ranjard Exp $
// Include files 

// from STL
#include <string>
#include <vector>
#include <list>

// from Gaudi

// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// from GiGa 
#include "GiGa/IGiGaSvc.h"

// from LHCbEvent 

#include "Event/MCHeader.h"

// local 
#include "PrintEventAlg.h"

// use std library instead of boost lexical_cast
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : PrintEventAlg
//
//            : Witold POKORSKI
// 2006-01-16 : Gloria CORTI
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrintEventAlg )

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrintEventAlg::initialize() 
{
  StatusCode sc = GaudiAlgorithm::initialize () ;
  if( sc.isFailure() ) { return sc ; }
  
  m_ppSvc = svc<LHCb::IParticlePropertySvc> ( "LHCb::ParticlePropertySvc", true );
  return StatusCode::SUCCESS; 
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrintEventAlg::execute() 
{
  m_liczevent++;
  
  //  typedef MCParticles Particles ;
  //typedef MCVertices  Vertices  ;
  int licz=0;

  
  // Header information
  SmartDataPtr<LHCb::MCHeader> hobj( eventSvc(), 
                                     LHCb::MCHeaderLocation::Default );  
  if( hobj ) { 
    info() << "Event number " << hobj->evtNumber() 
           << " Event time " <<  hobj->evtTime() << endmsg;
    info() << "Number of primary vertices from MCHeader "
           << (hobj->primaryVertices()).size() << endmsg;  
    for( SmartRefVector<LHCb::MCVertex>::const_iterator iv = 
           hobj->primaryVertices().begin();
         hobj->primaryVertices().end() != iv; ++iv ) {
      info() << " vertex " << (*iv)->position() << endmsg;
    }
  }
    
  // MCParticles
  if( !m_particles.value().empty() ) {
    const LHCb::MCParticles* obj = get<const LHCb::MCParticles>( m_particles.value() );
    
    for( LHCb::MCParticles::const_iterator ipart = obj->begin();
         ipart != obj->end(); ++ipart ) {
      if( !((*ipart)->mother())) {
        licz++;
        info() << " " << endmsg;
        printDecayTree( 0, " |", *ipart );
      }
    }
    
    info() << "Number of extracted particles '"
           << m_particles.value() << "' \t"
           << obj->size() 
           << endmsg;
    info() << "Number of 'primary' particles " 
           << licz << endmsg;
    
    m_licznik = m_licznik+licz;
    
  }
  
  // MCVertices
  if( !m_vertices.value().empty() ) {
    const LHCb::MCVertices* obj = get<const LHCb::MCVertices>( m_vertices.value() ) ;
    info() << "Number of extracted vertices  '"
           << m_vertices.value() << "'  \t" 
           << obj->size() 
           << endmsg;
    unsigned int nPV = 0;
    for( LHCb::MCVertices::const_iterator iv = obj->begin(); obj->end() != iv;
         ++iv ) {
      if( (*iv)->isPrimary() ) {
        nPV++;
        info() << "Primary " << nPV << " outgoing particles " 
               << (*iv)->products().size() << endmsg;
        info() << "Primary position " << (*iv)->position() << endmsg;
      }
    } 
    info() << "Number of extracted primary vertices " << nPV << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrintEventAlg::finalize()
{ 
  always () << "Average number of 'primary' particles "
            << m_licznik/m_liczevent << endmsg;
  
  return GaudiAlgorithm::finalize();
}

//=============================================================================
// Print the decay tree of a given mother particle
//=============================================================================
void PrintEventAlg::printDecayTree(long depth, const std::string& prefix,
                                   const LHCb::MCParticle* mother) {

  const SmartRefVector<LHCb::MCVertex>& decays = mother->endVertices();
  const LHCb::ParticleProperty* p = m_ppSvc->find( mother->particleID() );
  
  std::string name;
  
  if(!p) {
    Warning( " Particle not recognized " +
             std::to_string
             ( mother->particleID().pid() ) , StatusCode::FAILURE , 0 ).ignore() ;
    name="XXXX";
  }
  else {
    name= p->particle();
  }

  info() << depth << prefix.substr(0, prefix.length()-1)
         << "+--->" << std::setw(12) << std::setiosflags(std::ios::left) 
         << name 
         << "    En(MeV):"   << std::setw(12) << mother->momentum().e();
  
  if( mother->originVertex() ) {
    info() << " origin Vertex " << mother->originVertex()->position()
           << " vertex Type " << mother->originVertex()->type();
  }
  if( mother->primaryVertex() ) {
    info() << " - from primary vertex " << mother->primaryVertex()->position();
  } else {
    info() << " - no primary vertex!";
  }
  info() << endmsg;
  
  if( depth < m_depth.value() ) {
    SmartRefVector<LHCb::MCVertex>::const_iterator ivtx;
    for ( ivtx = decays.begin(); ivtx != decays.end(); ivtx++ )  {
      const SmartRefVector<LHCb::MCParticle>& daughters = (*ivtx)->products();
      SmartRefVector<LHCb::MCParticle>::const_iterator idau;
      for ( idau   = daughters.begin(); idau != daughters.end(); idau++ )  {
        printDecayTree( depth+1, prefix+" |", *idau );
      }
    }
  }
}

//=============================================================================








