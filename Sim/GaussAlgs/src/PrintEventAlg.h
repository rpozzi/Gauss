/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PrintEventAlg.h,v 1.4 2007-01-12 15:23:42 ranjard Exp $
#ifndef    GaussAlgs_PrintEventAlg_H
#define    GaussAlgs_PrintEventAlg_H 1

// Include files
#include "GaudiAlg/GaudiAlgorithm.h"

// From Event
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"

namespace LHCb {
  class IParticlePropertySvc;
}

/** @class PrintEventAlg.h
 *
 *  Almost empty/primitive algorithm to print debugging info for
 *  MC event
 *
 *  @author  Witold Pokorski
 *  @author  Gloria Corti
 *  @date    2002-12-09, last modified 2007-01-11
 */
class PrintEventAlg : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

private:

  Gaudi::Property<std::string>    m_particles{this,"Particles", LHCb::MCParticleLocation::Default,"Location in TES of MCParticles"};   ///< Location in TES of MCParticles
  Gaudi::Property<std::string>    m_vertices{this,"Vertices", LHCb::MCVertexLocation::Default ,"Location in TES of MCVertices"};    ///< Location in TES of MCVertices

  ///Depth to with to print the MCtrees (default = 99)
  Gaudi::Property<long> m_depth{this,"DecayDepth", 99 ,"Depth to with to print the MCtrees (default = 99)"};
  int  m_licznik{0};               ///< Total number of MCParticles (all events)
  int  m_liczevent{0};             ///< Number of events processed

  /** recursive method to print a decay tree from an MCParticle
   * @param depth  depth of tree to reach in printout
   * @param prefix prefix to add to a given line for an MCParticle
   * @param part   pointer to MCParticle to print
   */
  void printDecayTree(long depth, const std::string& prefix,
                      const LHCb::MCParticle*);

  LHCb::IParticlePropertySvc* m_ppSvc; ///<< Reference to particle property service

};
#endif  // GaussAlgs_PrintEventAlg_H
