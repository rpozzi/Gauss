/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaGetHitsAlg.cpp,v 1.13 2009-01-26 12:02:48 jonrob Exp $
// Include files

// from Gaudi
#include "GaudiKernel/Stat.h"


// local
#include "GiGaGetHitsAlg.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GiGaGetHitsAlg
//
// 2002-08-13 : Witold Pokorski
// 2004-11-22 : Gloria Corti
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GiGaGetHitsAlg )

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode GiGaGetHitsAlg::initialize() {

  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode GiGaGetHitsAlg::execute() {

  debug() << "==> Execute" << endmsg;

  // Velo hits
  hitsTracker( "Velo", m_velohits.value() );

  // Velo PileUp hits
  hitsTracker( "VeloPU", m_puvelohits.value() );

  // Trigger Tracker hits
  hitsTracker( "TT", m_tthits.value() );

  // Inner Tracker hits
  hitsTracker( "IT", m_ithits.value() );
  
  // SL  hits
  //hitsTracker( "SL", m_slhits.value() );

  // Outer Tracker hits
  hitsTracker( "OT", m_othits.value() );

  // RICH info: hits, optical photons, segments and
  infoRICH( );

  // Calorimeter hits
  unsigned int idet = 0;
  for(Addresses::const_iterator address = m_caloHits.value().begin() ;
      m_caloHits.value().end() != address ; ++address ) {
    if( (*address).empty() ) continue;
    LHCb::MCCaloHits* hits = get<LHCb::MCCaloHits>( *address );
    debug() << "Number of extracted MCCaloHits  '" << *address << "' \t"
            << hits->size() << endmsg ;
    Stat stat( chronoSvc(), "#"+m_caloDet[idet]+" MCHits", hits->size() );
    ++idet;
  }

  // Muon hits
  hitsTracker( "Muon", m_muonhits.value() );

  // Extra detector hits
  hitsTracker( "Extra" , m_extraHits.value() );

  return StatusCode::SUCCESS;

}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode GiGaGetHitsAlg::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
// Get hits for Trackers: Velo, PuVeto, TT, IT and OT
// If verbose print some details
//=============================================================================
void GiGaGetHitsAlg::hitsTracker(const std::string det,
                                 const std::string location)
{
  if( !location.empty() ) {
    LHCb::MCHits* obj = get<LHCb::MCHits>( location );
    std::string dethits = det+" MCHits";
    debug() << "Number of extracted MCHits      '"
            << location << "'  \t"
            << obj->size()
            << endmsg;
    Stat stat( chronoSvc(), "#"+dethits, obj->size() );
    if( msgLevel( MSG::VERBOSE ) ) {
      int icount = 0;
      for( LHCb::MCHits::const_iterator hiter=obj->begin(); hiter!=obj->end();
           ++hiter ) {
        verbose() << dethits << " " << icount++ << std::endl
                  << *(*hiter) << std::endl
                  << " from MCParticle " << (*hiter)->mcParticle()->key()
                  << "  pdg " << (*hiter)->mcParticle()->particleID().pid()
                  << "  E " << (*hiter)->mcParticle()-> momentum().e()
                  << endmsg;
      }
    }
  }
  return;
}

//=============================================================================
// Get hits for RICH: standard,
// If verbose print some details
//=============================================================================
void GiGaGetHitsAlg::infoRICH()
{
  // MCRichHits
  if( !m_richhits.value().empty() ) {
    LHCb::MCRichHits* obj = get<LHCb::MCRichHits>( m_richhits.value() );
    debug() << "Number of extracted MCRichHits  '"
            << m_richhits.value() << "' \t"
            << obj->size()
            << endmsg ;
    unsigned int nHitsRich1 = 0, nHitsRich2 = 0;
    int icount = 0;
    for( LHCb::MCRichHits::const_iterator hiter=obj->begin();
         hiter!=obj->end(); ++hiter ) {
      if( (*hiter)->rich() == Rich::Rich1 ) nHitsRich1++;
      if( (*hiter)->rich() == Rich::Rich2 ) nHitsRich2++;
      verbose() << "MCRichHit " << icount++ << std::endl
                << *(*hiter) << std::endl;
      //       verbose() << "Rich: " << (*hiter)->rich()
      //                 << " Radiator: " << (*hiter)->radiator()
      //                 << "  Entry point: " << (*hiter)->entry();
      if( (*hiter)->mcParticle() != NULL ) {
        verbose() << " from MCParticle: " << (*hiter)->mcParticle()->key()
                  << "  pdg " << (*hiter)->mcParticle()->particleID().pid()
                  << "  E " << (*hiter)->mcParticle()-> momentum().e()
                  <<  endmsg;
      } else {
        verbose() << "   No MCParticle " << endmsg;
      }
    }
    Stat stat  ( chronoSvc(), "#Rich MCHits", obj->size() );
    Stat statR1( chronoSvc(), "#Rich1 MCHits", nHitsRich1 );
    Stat statR2( chronoSvc(), "#Rich2 MCHits", nHitsRich2 );
  }


  // Optical photons
  if( !m_richop.value().empty() ) {
    LHCb::MCRichOpticalPhotons* obj =
      get<LHCb::MCRichOpticalPhotons>( m_richop.value() );
    debug() << "Number of extracted MCRichOpticalPhotons '"
            << m_richop.value() << "' \t"
            << obj->size()
            << endmsg;
    for( LHCb::MCRichOpticalPhotons::const_iterator hiter=obj->begin();
         hiter!=obj->end();  ++hiter ) {
      verbose() << "MCRichOpPhotons " << std::endl << *(*hiter) << endmsg;
    }
    Stat stat( chronoSvc() , "#MCRichOpPhotons" , obj->size() );
  }


  // Rich segments
  if( !m_richsegments.value().empty() ) {
    LHCb::MCRichSegments* obj = get<LHCb::MCRichSegments>( m_richsegments.value() );
    debug() << "Number of extracted MCRichSegments '"
            << m_richsegments.value() << "' \t"
            << obj->size()
            << endmsg;
    for( LHCb::MCRichSegments::const_iterator hiter=obj->begin();
         hiter!=obj->end();  ++hiter ) {
      verbose() << "MCRichSegments " << std::endl << *(*hiter) << endmsg;
    }
    Stat stat( chronoSvc(), "#MCRichSegments", obj->size() );
  }


  // Rich tracks
  if( !m_richtracks.value().empty() ) {
    LHCb::MCRichTracks* obj = get<LHCb::MCRichTracks>( m_richtracks.value() );
    debug() << "Number of extracted MCRichTracks '"
            << m_richtracks.value() << "' \t"
            << obj->size()
            << endmsg;
    Stat stat( chronoSvc(), "#MCRichTracks", obj->size() );
  }

  return;
}



//=============================================================================

