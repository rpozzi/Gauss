/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/MsgStream.h"

// from GiGa
#include "GiGa/IGiGaSvc.h"
#include "GiGa/DumpG4Event.h"

// local
#include "GiGaFlushAlgorithm.h"

// From Geant4 
class G4Event;

//-----------------------------------------------------------------------------
// Implementation file for class : GiGaFlushAlgorithm
//
// 
// 2002-01-22 : Vanya Belyaev
// 2007-01-11 : Gloria Corti
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GiGaFlushAlgorithm )


//=============================================================================
// Initialization
//=============================================================================
StatusCode GiGaFlushAlgorithm::initialize() 
{
  StatusCode sc = GaudiAlgorithm::initialize() ;
  if( sc.isFailure() ) { return sc ; }
  
  m_gigaSvc = svc<IGiGaSvc>( m_gigaSvcName.value() , true ) ;
  
  return StatusCode::SUCCESS ;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode GiGaFlushAlgorithm::execute() 
{  
  if ( 0 == gigaSvc() ) 
  { m_gigaSvc = svc<IGiGaSvc>( m_gigaSvcName.value() , true ) ; }
  
  if ( 0 == gigaSvc() ) 
  { return Error ( " execute(): IGiGaSvc* points to NULL" ) ;}
  
  // extract the event ( "flush the GiGa" )
  const G4Event* event = 0 ;
  //*gigaSvc()  >> event     ;
  // equivalent to retrieveEvent(), behind the scene this uses tool 
  // GiGaRunManager to prepareTheEvent() if not prepared, processTheEvent()
  // and then retrieveTheEvent().
  StatusCode sc = gigaSvc()->retrieveEvent(event);
  if( !sc.isSuccess() ) {
    return StatusCode::FAILURE;
  }
  
  if ( msgLevel( MSG::DEBUG ) ) 
  { 
    debug() << " Dump G4 event object " << endmsg ;
    if( info().isActive() ) 
    { GiGaUtil::DumpG4Event ( info().stream() , event  ) ; }
    info() << endmsg ;
  };

  return StatusCode::SUCCESS;
}


//=============================================================================
