/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef GAUSSCHERENKOV_CHERENKOVG4PMTREFLTAG_H 
#define GAUSSCHERENKOV_CHERENKOVG4PMTREFLTAG_H 1

// Include files
#include "globals.hh"
#include "G4Track.hh"
#include "G4ThreeVector.hh"


/** @class CherenkovG4PmtReflTag CherenkovG4PmtReflTag.h CherenkovMisc/CherenkovG4PmtReflTag.h
 *  
 *
 *  @author Sajan Easo
 *  @date   2011-04-19
 */
extern void RichG4PmtQWIncidentTag(const G4Track& aPhotonTk, const G4ThreeVector & aPmtQWPoint, int aQWLensFlag );


#endif // GAUSSCHERENKOV_CHERENKOVG4PMTREFLTAG_H
