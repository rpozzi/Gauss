/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CkvG4TrackActionPhotOpt.h,v 1.5 2009-07-03 11:59:49 seaso Exp $
#ifndef RICHACTION_CKVG4TRACKACTIONPHOTOPT_HH
#define RICHACTION_CKVG4TRACKACTIONPHOTOPT_HH 1

// Include files
// STL
#include <string>
#include <vector>
// GiGa
#include "GiGa/GiGaTrackActionBase.h"

/// Geant4
#include "globals.hh"

/** @class CkvG4TrackActionPhotOpt
 * CkvG4TrackActionPhotOpt.hh
 * RichAction/CkvG4TrackActionPhotOpt.hh
 *
 *
 *  @author Sajan EASO
 *  @author Gloria Corti (adapt to Gaudi v19)
 *  @date   2003-04-29, last modified 01
 */

// forward declarations
class G4Track;
class G4particleDefinition;

class CkvG4TrackActionPhotOpt: virtual public GiGaTrackActionBase
{
public:

  /// useful typedefs
  typedef  std::vector<std::string>                  TypeNames;
  typedef  std::vector<const G4ParticleDefinition*>  PartDefs;
  ///

  StatusCode initialize () override;   ///< initialize

  /// Standard constructor
  CkvG4TrackActionPhotOpt ( const std::string& type   ,
                             const std::string& name   ,
                             const IInterface*  parent ) ;


  void PreUserTrackingAction  ( const G4Track* ) override;

  //  virtual void PostUserTrackingAction ( const G4Track* ) ;

private:

  // no default constructor.
  CkvG4TrackActionPhotOpt ();
  // no copy
  CkvG4TrackActionPhotOpt (const  CkvG4TrackActionPhotOpt& );
  CkvG4TrackActionPhotOpt& operator=(const  CkvG4TrackActionPhotOpt& );

  G4double  m_MaxPhdQuantumEffFromDB{0.};
  G4double  m_MaxRich1Mirror1Reflect{0.};
  G4double  m_MaxRich1Mirror2Reflect{0.};
  G4double  m_MaxRich2Mirror1Reflect{0.};
  G4double  m_MaxRich2Mirror2Reflect{0.};
  G4double  m_ZDownstreamOfRich1{0.};
  G4double  m_Rich1TotPhotonSuppressFactor{0.};
  G4double  m_Rich2TotPhotonSuppressFactor{0.};
  Gaudi::Property<G4bool>    m_RichPhdMaxQEOverRideDB{this,"RichPhdMaxQEOverRideDB",false,"RichPhdMaxQEOverRideDB"};
  Gaudi::Property<G4double>  m_RichPhdMaxQEValueWhenOverRideDB{this,"RichPhdMaxQEValueWhenOverRideDB",0.60,"RichPhdMaxQEValueWhenOverRideDB"};

};

#endif // RICHACTION_RICHG4TRACKACTIONPHOTOPT_HH
