/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GaussSensPlaneDet.h,v 1.4 2007-03-18 21:33:19 gcorti Exp $
#ifndef       GAUSS_GaussSensPlaneDet_H
#define       GAUSS_GaussSensPlaneDet_H 1

// GaudiKernel
// Ntuple Svc
#include "GaudiKernel/INTuple.h"
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiKernel/NTuple.h"
// CLHEP
#include "CLHEP/Units/PhysicalConstants.h"
// GiGa
#include "GiGa/GiGaSensDetBase.h"

// local
#include "GaussCalo/CaloHit.h"


/** @class GaussSensPlaneDet GaussSensPlaneDet.h GaussSensPlaneDet.h
 *
 *  @author  Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date    23/01/2001
 */

class GaussSensPlaneDet: public GiGaSensDetBase
{
  /// friend factory
  //  friend class GiGaFactory<GaussSensPlaneDet>;

public :

  /** standard initialization (Gaudi)
   *  @see GiGaSensDetBase
   *  @see GiGaBase
   *  @see   AlgTool
   *  @see  IAlgTool
   *  @return status code
   */
  StatusCode initialize   () override;

  /** standard finalization (Gaudi)
   *  @see GiGaSensDetBase
   *  @see GiGaBase
   *  @see   AlgTool
   *  @see  IAlgTool
   *  @return status code
   */
  StatusCode finalize    () override;

public:

  /** process the hit.
   *  The method is invoked by G4 for each step in the
   *  sensitive detector. This implementation performs the
   *  generic (sub-detector) independent action:
   *    - decode G4Step, G4Track information
   *    - determine the Calo::CellID of the active cell
   *    - determine is the hit to be associated with
   *      the track or its parent track
   *    - find/create CaloHit object for the given cell
   *    - find/create CaloSubHit object for given track
   *    - invoke fillHitInfo method for filling the hit with
   *      the actual (subdetector-specific) informaton
   *      (Birk's corrections, local/global non-uniformity
   *      corrections, timing, etc)
   *  @param step     pointer to current Geant4 step
   *  @param history  pointert to touchable history
   *  @attention One should not redefine this method for specific sub-detectors.
   */
  bool ProcessHits
  ( G4Step*             step    ,
    G4TouchableHistory* history ) override;

  /** method from G4
   *  (Called at the begin of each event)
   *  @see G4VSensitiveDetector
   *  @param HCE pointer to hit collection of current event
   */
  void Initialize( G4HCofThisEvent* HCE ) override;

  /** method from G4
   *  (Called at the end of each event)
   *  @see G4VSensitiveDetector
   *  @param HCE pointer to hit collection of current event
   */
  void EndOfEvent( G4HCofThisEvent* HCE ) override;


  /** standard constructor
   *  @see GiGaSensDetBase
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  GaussSensPlaneDet
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent );
  //using GiGaSensDetBase::GiGaSensDetBase;
  /// destructor (virtual and protected)
  

protected:

  /// keep all original links to G4Tracks/MCParticles
  inline bool   keepLinks       () const { return m_keepLinks.value()       ; }
  /// only one entry ?
  inline bool   oneEntry        () const { return m_oneEntry.value()        ; }

  /// cut for photons to create hit
  inline double cutForPhoton    () const { return m_cutForPhoton.value()    ; }

  /// cut for e-   to create hit
  inline double cutForElectron  () const { return m_cutForElectron.value()  ; }

  /// cut for e+   to create hit
  inline double cutForPositron  () const { return m_cutForPositron.value()  ; }

  /// cut for muon to create hit
  inline double cutForMuon      () const { return m_cutForMuon.value()      ; }

  /// cut for other charged particle to create hit
  inline double cutForCharged   () const { return m_cutForCharged.value()   ; }

  /// cut for othe rneutral particle to create hit
  inline double cutForNeutral   () const { return m_cutForNeutral.value()   ; }

private:

  // no default constructor
  GaussSensPlaneDet() =default;
  // no copy constructor
  GaussSensPlaneDet           ( const GaussSensPlaneDet& ) = default ;
  // no assignement
  GaussSensPlaneDet& operator=( const GaussSensPlaneDet& ) = default ;

protected:

  Gaudi::Property<std::string>                   m_collectionName{this,"CollectionName","Hits","CollectionName"} ;
  GaussSensPlaneHitsCollection* m_collection{nullptr}     ;

  Gaudi::Property<bool>   m_keepLinks{this,"KeepAllLinks",false,"KeepAllLinks"}       ;
  Gaudi::Property<bool>   m_oneEntry{this,"OneEntry",true ,"OneEntry"}        ;

  Gaudi::Property<double> m_cutForPhoton{this,"CutForPhoton",50 * CLHEP::MeV,"CutForPhoton"}    ;
  Gaudi::Property<double> m_cutForElectron{this,"CutForElectron",10 * CLHEP::MeV,"CutForElectron"}  ;
  Gaudi::Property<double> m_cutForPositron{this,"CutForPositron",10 * CLHEP::MeV,"CutForPositron"}  ;
  Gaudi::Property<double> m_cutForMuon{this,"CutForMuon",-1 * CLHEP::MeV,"CutForMuon"}      ;
  Gaudi::Property<double> m_cutForCharged{this,"CutForCharged",10 * CLHEP::MeV,"CutForCharged"}   ;
  Gaudi::Property<double> m_cutForNeutral{this,"CutForNeutral",10 * CLHEP::MeV,"CutForNeutral"}   ;

private:

  // final statistics
  bool   m_stat{true};
  long   m_events{0};
  double m_hits{0};
  double m_hits2{0};
  double m_hitsMin {1.e+10};
  double m_hitsMax {-1.e+10};

};
// ============================================================================


// ============================================================================
// The END
// ============================================================================
#endif  ///< GAUSSCALO_GaussSensPlaneDet_H
// ============================================================================
