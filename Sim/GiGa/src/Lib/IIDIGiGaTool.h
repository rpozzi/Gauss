/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IIDIGiGaTool.h,v 1.1 2004-02-20 18:58:24 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.1  2003/04/06 18:49:47  ibelyaev
//  see $GIGAROOT/doc/release.notes
//
// ============================================================================
#ifndef GIGA_IIDIGIGATOOL_H 
#define GIGA_IIDIGIGATOOL_H 1
// ============================================================================

// ============================================================================
/** @file 
 *  declaration of the unique interface identifier for class 
 *  IGiGaTool
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2003-04-06
 */
// ============================================================================

// ============================================================================
/** @var IID_IGiGaTool
 *  declaration of the unique interface identifier 
 *  for class IGiGaTool
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2003-04-06
 */
// ============================================================================
static const InterfaceID IID_IGiGaTool ( "IGiGaTool" , 1 , 0 ) ;
// ============================================================================

// ============================================================================
// The END
// ============================================================================
#endif // GIGA_IIDIGIGATOOL_H
// ============================================================================
