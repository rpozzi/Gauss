/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GaussRedecayPrintMCParticles_H
#define GaussRedecayPrintMCParticles_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class GaussRedecayPrintMCParticles GaussRedecayPrintMCParticles.h
 *
 * Simple algorithm to debug redecayed MCParticle output
 *
 *  @author Dominik Muller
 *  @date   2016-3-30
 */

namespace LHCb {
class MCVertex;
class MCParticle;
}
class GaussRedecayPrintMCParticles : public GaudiAlgorithm {
  public:
  /// Standard constructor
  GaussRedecayPrintMCParticles(const std::string& Name, ISvcLocator* SvcLoc);

  StatusCode initialize() override;  ///< Algorithm initialization
  StatusCode execute() override;     ///< Algorithm execution

  protected:
  /** accessor to GaussRedecay Service
   *  @return pointer to GaussRedecay Service
   */

  private:
  std::string m_particlesLocation;  ///< Location in TES of output MCParticles.
  std::string m_verticeLocation;  ///< Location in TES of output MCParticles.
  std::string m_mcHeaderLocation;   ///< Location in TES of MCHeader for PV.
  std::set<LHCb::MCVertex*> m_vertices;
  std::set<LHCb::MCParticle*> m_particles;

  int printMCParticlesTree(LHCb::MCVertex* vtx, int level = 0, int counter = 0);
};

#endif  // GaussRedecayPrintMCParticles_H
