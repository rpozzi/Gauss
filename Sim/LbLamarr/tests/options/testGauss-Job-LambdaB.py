###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
####################################################
# simple test options for Lambda_b events for Lamarr
#####################################################
from Gaudi.Configuration import *
from Configurables import LbLamarr 
from LbLamarr import GeneratorConfigWizard 
#Lamarr configuration
eventType="15874000"
importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")
GeneratorConfigWizard.configureGenerator ( 'ParticleGun', eventType ) 

LbLamarr().EventType = eventType
LbLamarr().EvtMax    = 1000
LbLamarr().DataType  = '2016'
LbLamarr().Polarity  = 'MagDown' 
#beam config
importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py")
#database tags
importOptions("$GAUSSOPTS/DBTags-2016.py")

