/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
void BuildResEffHistos(const Char_t *fileTree = "trackRes_Small.root"){
  TFile *fileIn = new TFile(fileTree,"OPEN");
  // TFile *fileGauss= new TFile("Gauss_30000000_histos_20180119.root","OPEN");
  TFile *fileGauss= new TFile("Gauss_30000000_histos_SimDelphesWithEff.root","OPEN");
  TFile *fileDelphes = new TFile("GaussTuple_30000000_SimDelphesWithEff.root","OPEN");

  double pcut = 0;
  TH3D *hTxTyP_Gauss = (TH3D*)fileGauss->Get("DelphesHist/TxTyOneOverPRec");
  hTxTyP_Gauss->SetName("hTxTyP_Gauss");
  hTxTyP_Gauss->SetDirectory(0);
  int nbinsX = hTxTyP_Gauss->GetNbinsX();
  int nbinsY = hTxTyP_Gauss->GetNbinsY();
  int nbinsZ = hTxTyP_Gauss->GetNbinsZ();
  double xmin = hTxTyP_Gauss->GetXaxis()->GetXmin();
  double xmax = hTxTyP_Gauss->GetXaxis()->GetXmax();
  double ymin = hTxTyP_Gauss->GetYaxis()->GetXmin();
  double ymax = hTxTyP_Gauss->GetYaxis()->GetXmax();
  double zmin = hTxTyP_Gauss->GetZaxis()->GetXmin();
  double zmax = hTxTyP_Gauss->GetZaxis()->GetXmax();
  
  TH3D *hTxTyP_Recoed_EndVelo = new TH3D("hReconstructed_EndVelo","hReconstructed_EndVelo",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  TH3D *hTxTyP_Recoble_EndVelo = new TH3D("hReconstructible_EndVelo","hReconstructible_EndVelo",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  TH3D *hTxTyP_Recoed_EndRich2  = new TH3D("hReconstructed_EndRich2","hReconstructed_EndRich2",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  TH3D *hTxTyP_Recoble_EndRich2 = new TH3D("hReconstructible_EndRich2","hReconstructible_EndRich2",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  TH3D *hTxTyP_True_EndVelo = new TH3D("hTxTyP_True_EndVelo","hTxTyP_True_EndVelo",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  TH3D *hTxTyP_MC_EndVelo = new TH3D("hTxTyP_MC_EndVelo","hTxTyP_MC_EndVelo",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  TH3D *hTxTyP_Delphes_EndVelo = new TH3D("hTxTyP_Delphes_EndVelo","hTxTyP_Delphes_EndVelo",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  // TH3D *hTxTyP_True_EndRich2 = new TH3D("hTxTyP_True_EndRich2","hTxTyP_True_EndRich2",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  // TH3D *hTxTyP_MC_EndRich2 = new TH3D("hTxTyP_MC_EndRich2","hTxTyP_MC_EndRich2",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  // TH3D *hTxTyP_Delphes_EndRich2 = new TH3D("hTxTyP_Delphes_EndRich2","hTxTyP_Delphes_EndRich2",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);

  
  TTree *tree_recoed = (TTree*)fileIn->Get("TrackEffRes/TrackTree");
  int nentries_recoed = tree_recoed->GetEntries();
  TTree *tree_recoble = (TTree*)fileIn->Get("TrackEffRes/TrackTree_reconstructible");
  int nentries_recoble = tree_recoble->GetEntries();
  TTree *tree_mc = (TTree*)fileIn->Get("TrackEffRes/TrackTree_mc");
  int nentries_mc = tree_mc->GetEntries();
  TTree *tree_delphes_EndVelo = (TTree*)fileDelphes->Get("DelphesTuple/Rec");
  int nentries_delphes_EndVelo = tree_delphes_EndVelo->GetEntries();

  double tx_true_EndVelo, ty_true_EndVelo, p_true_EndVelo;
  double tx_true_EndRich2, ty_true_EndRich2, p_true_EndRich2;
  double tx_mc_EndVelo, ty_mc_EndVelo, p_mc_EndVelo;
  double tx_delphes_EndVelo, ty_delphes_EndVelo, p_delphes_EndVelo;
  double dtx_EndVelo, dty_EndVelo, dp_EndVelo;
  double dtx_EndRich2, dty_EndRich2, dp_EndRich2;
  double tx_recoed_EndVelo, ty_recoed_EndVelo, p_recoed_EndVelo;
  double tx_recoble_EndVelo, ty_recoble_EndVelo, p_recoble_EndVelo;
  double tx_recoed_EndRich2, ty_recoed_EndRich2, p_recoed_EndRich2;
  double tx_recoble_EndRich2, ty_recoble_EndRich2, p_recoble_EndRich2;


  tree_recoed->SetBranchAddress("true_tx_endvelo",&tx_true_EndVelo);
  tree_recoed->SetBranchAddress("true_ty_endvelo",&ty_true_EndVelo);
  tree_recoed->SetBranchAddress("true_p_endvelo",&p_true_EndVelo);
  tree_recoed->SetBranchAddress("true_tx_endEndRich2",&tx_true_EndRich2);
  tree_recoed->SetBranchAddress("true_ty_endEndRich2",&ty_true_EndRich2);
  tree_recoed->SetBranchAddress("true_p_endEndRich2",&p_true_EndRich2);
  tree_recoed->SetBranchAddress("tx_endvelo",&tx_recoed_EndVelo);
  tree_recoed->SetBranchAddress("ty_endvelo",&ty_recoed_EndVelo);
  tree_recoed->SetBranchAddress("p_endvelo",&p_recoed_EndVelo);
  tree_recoed->SetBranchAddress("tx_endEndRich2",&tx_recoed_EndRich2);
  tree_recoed->SetBranchAddress("ty_endEndRich2",&ty_recoed_EndRich2);
  tree_recoed->SetBranchAddress("p_endEndRich2",&p_recoed_EndRich2);

  tree_mc->SetBranchAddress("mc_tx_velo",&tx_mc_EndVelo);
  tree_mc->SetBranchAddress("mc_ty_velo",&ty_mc_EndVelo);
  tree_mc->SetBranchAddress("mc_p_velo",&p_mc_EndVelo);

  tree_delphes_EndVelo->SetBranchAddress("thetaX",&tx_delphes_EndVelo);
  tree_delphes_EndVelo->SetBranchAddress("thetaY",&ty_delphes_EndVelo);
  tree_delphes_EndVelo->SetBranchAddress("p",&p_delphes_EndVelo);

  tree_recoble->SetBranchAddress("reco_true_tx_velo",&tx_recoble_EndVelo);
  tree_recoble->SetBranchAddress("reco_true_ty_velo",&ty_recoble_EndVelo);
  tree_recoble->SetBranchAddress("reco_true_p_velo",&p_recoble_EndVelo);
  tree_recoble->SetBranchAddress("reco_true_tx_EndRich2",&tx_recoble_EndRich2);
  tree_recoble->SetBranchAddress("reco_true_ty_EndRich2",&ty_recoble_EndRich2);
  tree_recoble->SetBranchAddress("reco_true_p_EndRich2",&p_recoble_EndRich2);

  int nbins[6] = {100, 100, 500, 100, 100, 500};
  double xminv[6] = {-1, -0.3, 0, -0.02, -0.02, -2};
  double xmaxv[6] = {1, 0.3, 100, 0.02, 0.02, 2};

  THnSparseD *sparseResEndVelo = new THnSparseD("sparseResEndVelo","sparseResEndVelo",6,nbins,xminv,xmaxv);
  THnSparseD *sparseResEndRich2 = new THnSparseD("sparseResEndRich2","sparseResEndRich2",6,nbins,xminv,xmaxv);

  for(int i=0; i<nentries_recoed; i++){

    tree_recoed->GetEntry(i);
    dtx_EndVelo = tx_recoed_EndVelo - tx_true_EndVelo;
    dty_EndVelo = ty_recoed_EndVelo - ty_true_EndVelo;
    dp_EndVelo  = p_recoed_EndVelo - p_true_EndVelo;
    Double_t vfill[6] =
      {
	tx_true_EndVelo,
	ty_true_EndVelo,
	p_true_EndVelo/1000.,
	dtx_EndVelo,
	dty_EndVelo,
	dp_EndVelo
      };
    Double_t vfillEndRich2[6] =
      {
	tx_true_EndRich2,
	ty_true_EndRich2,
	p_true_EndRich2/1000.,
	dtx_EndRich2,
	dty_EndRich2,
	dp_EndRich2
      };
    sparseResEndVelo->Fill(vfill);
    sparseResEndRich2->Fill(vfill);
    if(p_recoed_EndVelo<pcut)continue;
    hTxTyP_Recoed_EndVelo->Fill(tx_recoed_EndVelo,ty_recoed_EndVelo,1./p_recoed_EndVelo);
    hTxTyP_True_EndVelo->Fill(tx_true_EndVelo,ty_true_EndVelo,1./p_true_EndVelo);
    hTxTyP_Recoed_EndRich2->Fill(tx_recoed_EndRich2,ty_recoed_EndRich2,1./p_recoed_EndRich2);
  }

  for(int i=0; i<nentries_recoble; i++){
    tree_recoble->GetEntry(i);
    if(p_recoble_EndVelo<pcut)continue;
    hTxTyP_Recoble_EndVelo->Fill(tx_recoble_EndVelo,ty_recoble_EndVelo,1./p_recoble_EndVelo);
    hTxTyP_Recoble_EndRich2->Fill(tx_recoble_EndRich2,ty_recoble_EndRich2,1./p_recoble_EndRich2);
  }
  
  for(int i=0; i<nentries_mc; i++){
    tree_mc->GetEntry(i);
    if(p_mc_EndVelo<pcut)continue;
    hTxTyP_MC_EndVelo->Fill(tx_mc_EndVelo,ty_mc_EndVelo,1./p_mc_EndVelo);
  }
  for(int i=0; i<nentries_delphes_EndVelo; i++){
    tree_delphes_EndVelo->GetEntry(i);
    if(p_delphes_EndVelo<pcut)continue;
    hTxTyP_Delphes_EndVelo->Fill(tx_delphes_EndVelo,ty_delphes_EndVelo,1./p_delphes_EndVelo);
  }


  // TH3D *hTxTyP_MC_EndVelo_weight = (TH3D*)hTxTyP_MC_EndVelo->Clone("hTxTyP_MC_EndVelo_weight");
  // TH3D *hTxTyP_Reco_weight = (TH3D*)hTxTyP_Reco->Clone("hTxTyP_Reco_weight");
  // TH3D *hTxTyP_delphes_weight = (TH3D*)hTxTyP_Reco->Clone("hTxTyP_delphes_weight");
  TH3D *hWeight = new TH3D("hWeight","hWeight",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
  // hWeight->Reset();


  double cMC, cReco, cDelphes, eFull, eDelphes, w;
  for(int i = 1; i<=hTxTyP_MC_EndVelo->GetNbinsX(); i++)
    for(int j = 1; j<=hTxTyP_MC_EndVelo->GetNbinsY(); j++)
      for(int k = 1; k<=hTxTyP_MC_EndVelo->GetNbinsZ(); k++){
  	cMC= hTxTyP_MC_EndVelo->GetBinContent(i,j,k); 
  	cReco= hTxTyP_Recoble_EndVelo->GetBinContent(i,j,k);
  	cDelphes = hTxTyP_Delphes_EndVelo->GetBinContent(i,j,k);
	if (cDelphes==0) w = 1;
	else w = cReco/cDelphes;
  	hWeight->SetBinContent(i,j,k,w);
	double x = hTxTyP_Recoble_EndVelo->GetXaxis()->GetBinCenter(i);
	double y = hTxTyP_Recoble_EndVelo->GetYaxis()->GetBinCenter(j);
	double z = hTxTyP_Recoble_EndVelo->GetZaxis()->GetBinCenter(k);
	// hTxTyP_Eff1->Fill(x,y,z,cDelphes/cReco);
      }
  // hTxTyP_Eff1->Add(hTxTyP_Delphes_EndVelo);
  // hTxTyP_Eff1->Divide(hTxTyP_Recoble_EndVelo);


  // TH3D *hTxTyP_full_weight = (TH3D*)hTxTyP_Recoed_EndVelo->Clone("hTxTyP_full_weight");
  // TH3D *hTxTyP_Reco_weight = (TH3D*)hTxTyP_Reco->Clone("hTxTyP_Reco_weight");
  // hTxTyP_full_weight->Scale(1./hTxTyP_full_weight->Integral());
  // hTxTyP_Reco_weight->Scale(1./hTxTyP_Reco_weight->Integral());
  
  // hTxTyP_full_weight->Divide(hTxTyP_Reco_weight);

  
  // TH3D *hTxTyP_delphes_weight = (TH3D*)hTxTyP_Reco->Clone("hTxTyP_delphes_weight");
  // hTxTyP_delphes_weight->Scale(1./hTxTyP_delphes_weight->Integral());
  // hTxTyP_delphes_weight->Divide(hTxTyP_Gauss);

  // TH3D *hWeight = (TH3D*)hTxTyP_full_weight->Clone("hWeight");
  // hWeight->Multiply(hTxTyP_delphes_weight);
  // hWeight->SetDirectory(0);
  
  TFile *fileOut = new TFile("ResSparse.root","RECREATE");
  sparseResEndVelo->Write();
  sparseResEndRich2->Write();
  // hTxTyP_Eff1->Write();
  hTxTyP_MC_EndVelo->Write();
  hTxTyP_Delphes_EndVelo->Write();
  // hTxTyP_full_weight->Write();
  // hTxTyP_delphes_weight->Write();
  hTxTyP_Recoed_EndVelo->Write();
  hTxTyP_Recoble_EndVelo->Write();
  hTxTyP_Recoed_EndRich2->Write();
  hTxTyP_Recoble_EndRich2->Write();
  hTxTyP_True_EndVelo->Write();
  hTxTyP_Gauss->Write();
  hWeight->Write();
  fileOut->Close();
  fileIn->Close();
  fileGauss->Close();
}
