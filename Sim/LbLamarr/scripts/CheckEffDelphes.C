/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
void CheckEffDelphes(){
  // TFile *file = new TFile("Brunel-2000ev-histos_9400new.root","OPEN");
  // TFile *file = new TFile("Brunel-2000ev-histos_noGhosts.root","OPEN");
  TFile *file = new TFile("ResSparse.root","OPEN");
  // TFile *file = new TFile("Brunel-100ev-histos.root","OPEN");
  // TH3D *hreconstructible = (TH3D*)file->Get("Track/PrChecker/BestLong/longXYPhi_reconstructible");
  // TH3D *hreconstructed = (TH3D*)file->Get("hReconstructible");
  TH3D *hreconstructed = (TH3D*)file->Get("hTxTyP_Delphes_EndVelo");
  TH3D *hreconstructed_txtyp = (TH3D*)file->Get("hReconstructed_EndRich2");
  // TH3D *hreconstructed = (TH3D*)file->Get("Track/PrChecker/BestLong/LongXYPhi_Closestlt10");

  TFile *file2_delphes = new TFile("ResSparse.root","OPEN");
  // TFile *file2_delphes = new TFile("ResSparse.root","OPEN");
  TH3D *hreconstructible = (TH3D*)file2_delphes->Get("hReconstructed_EndRich2");
  // TH3D *hreconstructible = (TH3D*)file2_delphes->Get("hTxTyP_MC");
  // TH3D *hreconstructible_txtyp = (TH3D*)file2_delphes->Get("hReconstructible");
  TH3D *hreconstructible_txtyp = (TH3D*)file2_delphes->Get("hReconstructible_EndRich2");
  // TH3D *hreconstructible_txtyp = (TH3D*)file2_delphes->Get("DelphesHist/TxTyOneOverPRec");
  
  // TFile *file2_delphes = new TFile("Gauss_30000000_histos_20180119.root","OPEN");
  // TH3D *hreconstructible = (TH3D*)file2_delphes->Get("DelphesHist/XYPhiRec");
  // TH3D *hreconstructible_txtyp = (TH3D*)file2_delphes->Get("DelphesHist/TxTyPRec");



  cout<<"Integral: "<<hreconstructible->Integral()<<endl;
  hreconstructible->SetName("MC");
  hreconstructible_txtyp->SetName("Reconstructible");
  hreconstructed->SetName("Delphes");
  hreconstructed_txtyp->SetName("Reconstructed");
  
  // hreconstructed->Scale(1./hreconstructed->Integral());
  // hreconstructed_txtyp->Scale(1./hreconstructed_txtyp->Integral());
  // hreconstructible->Scale(hreconstructed->Integral()/hreconstructible->Integral());
  // hreconstructible_txtyp->Scale(hreconstructed_txtyp->Integral()/hreconstructible_txtyp->Integral());
  
  TH1D *hreconstructible_hx = (TH1D*)hreconstructible->ProjectionX("MC_hx");
  TH1D *hreconstructible_hy = (TH1D*)hreconstructible->ProjectionY("MC_hy");
  TH1D *hreconstructible_hz = (TH1D*)hreconstructible->ProjectionZ("MC_hz");

  TH1D *hreconstructible_txtyp_hx = (TH1D*)hreconstructible_txtyp->ProjectionX("hreconstructible_txtyp_hx");
  TH1D *hreconstructible_txtyp_hy = (TH1D*)hreconstructible_txtyp->ProjectionY("hreconstructible_txtyp_hy");
  TH1D *hreconstructible_txtyp_hz = (TH1D*)hreconstructible_txtyp->ProjectionZ("hreconstructible_txtyp_hz");

  TH1D *hreconstructed_hx = (TH1D*)hreconstructed->ProjectionX("Delphes_hx");
  TH1D *hreconstructed_hy = (TH1D*)hreconstructed->ProjectionY("Delphes_hy");
  TH1D *hreconstructed_hz = (TH1D*)hreconstructed->ProjectionZ("Delphes_hz");

  TH1D *hreconstructed_txtyp_hx = (TH1D*)hreconstructed_txtyp->ProjectionX("hreconstructed_txtyp_hx");
  TH1D *hreconstructed_txtyp_hy = (TH1D*)hreconstructed_txtyp->ProjectionY("hreconstructed_txtyp_hy");
  TH1D *hreconstructed_txtyp_hz = (TH1D*)hreconstructed_txtyp->ProjectionZ("hreconstructed_txtyp_hz");

  cout<<"Integral: "<<hreconstructible->Integral()<<endl;
  
  hreconstructed_hx->Divide(hreconstructible_hx);
  hreconstructed_hy->Divide(hreconstructible_hy);
  hreconstructed_hz->Divide(hreconstructible_hz);

  hreconstructed_txtyp_hx->Divide(hreconstructible_txtyp_hx);
  hreconstructed_txtyp_hy->Divide(hreconstructible_txtyp_hy);
  hreconstructed_txtyp_hz->Divide(hreconstructible_txtyp_hz);

  TH1D *hreconstructible_full_hx = (TH1D*)hreconstructible_txtyp->ProjectionX("hreconstructible_full_hx");
  TH1D *hreconstructible_full_hy = (TH1D*)hreconstructible_txtyp->ProjectionY("hreconstructible_full_hy");
  TH1D *hreconstructible_full_hz = (TH1D*)hreconstructible_txtyp->ProjectionZ("hreconstructible_full_hz");
  
  hreconstructible_full_hx->Divide(hreconstructible_hx);
  hreconstructible_full_hy->Divide(hreconstructible_hy);
  hreconstructible_full_hz->Divide(hreconstructible_hz);


  TCanvas *cMCRecble = new TCanvas("cMCRecble","cMCRecble");
  cMCRecble->Divide(3,3);
  cMCRecble->cd(1);
  hreconstructible_hx->Draw();
  cMCRecble->cd(2);
  hreconstructible_hy->Draw();
  cMCRecble->cd(3);
  hreconstructible_hz->Draw();
  cMCRecble->cd(4);
  hreconstructible_txtyp->ProjectionX()->Draw();
  cMCRecble->cd(5);
  hreconstructible_txtyp->ProjectionY()->Draw();
  cMCRecble->cd(6);
  hreconstructible_txtyp->ProjectionZ()->Draw();
  cMCRecble->cd(7);
  hreconstructible_full_hx->Draw();
  cMCRecble->cd(8);
  hreconstructible_full_hy->Draw();
  cMCRecble->cd(9);
  hreconstructible_full_hz->Draw();
  
  
  TCanvas *cEff = new TCanvas("cEff","cEff");
  cEff->Divide(3,3);
  cEff->cd(1);
  hreconstructible->ProjectionX()->Draw();
  cEff->cd(2);
  hreconstructible->ProjectionY()->Draw();
  cEff->cd(3);
  hreconstructible->ProjectionZ()->Draw();
  cEff->cd(4);
  hreconstructed->ProjectionX()->Draw();
  cEff->cd(5);
  hreconstructed->ProjectionY()->Draw();
  cEff->cd(6);
  hreconstructed->ProjectionZ()->Draw();
  cEff->cd(7);
  hreconstructed_hx->Draw();
  cEff->cd(8);
  hreconstructed_hy->Draw();
  cEff->cd(9);
  hreconstructed_hz->Draw();

  TCanvas *cEff_txtyp = new TCanvas("cEff_txtyp","cEff_txtyp");
  cEff_txtyp->Divide(3,3);
  cEff_txtyp->cd(1);
  hreconstructible_txtyp->ProjectionX()->Draw();
  cEff_txtyp->cd(2);
  hreconstructible_txtyp->ProjectionY()->Draw();
  cEff_txtyp->cd(3);
  hreconstructible_txtyp->ProjectionZ()->Draw();
  cEff_txtyp->cd(4);
  hreconstructed_txtyp->ProjectionX()->Draw();
  cEff_txtyp->cd(5);
  hreconstructed_txtyp->ProjectionY()->Draw();
  cEff_txtyp->cd(6);
  hreconstructed_txtyp->ProjectionZ()->Draw();
  cEff_txtyp->cd(7);
  hreconstructed_txtyp_hx->Draw();
  cEff_txtyp->cd(8);
  hreconstructed_txtyp_hy->Draw();
  cEff_txtyp->cd(9);
  hreconstructed_txtyp_hz->Draw();

  TCanvas *cfg = new TCanvas("cfg");
  TH1F*hx_delphes = (TH1F*)hreconstructed->ProjectionX();
  TH1F*hx = (TH1F*)hreconstructible->ProjectionX();
  hx_delphes->Draw();
  hx->SetLineColor(kRed);
  hx->Draw("same");
  hx_delphes->SetTitle("X distribution");
  hx_delphes->GetXaxis()->SetTitle("x [mm]");

  TCanvas *cfgy = new TCanvas("cfgy");
  TH1F*hy_delphes = (TH1F*)hreconstructed->ProjectionY();
  TH1F*hy = (TH1F*)hreconstructible->ProjectionY();
  hy_delphes->Draw();
  hy->SetLineColor(kRed);
  hy->Draw("same");
  hy_delphes->SetTitle("Y distribution");
  hy_delphes->GetXaxis()->SetTitle("y [mm]");
  
  TCanvas *cfgz = new TCanvas("cfgz");
  TH1F*hz_delphes = (TH1F*)hreconstructed->ProjectionZ();
  TH1F*hz = (TH1F*)hreconstructible->ProjectionZ();
  hz_delphes->Draw();
  hz->SetLineColor(kRed);
  hz->Draw("same");

  TCanvas *cfg_txtyp = new TCanvas("cfg_txtyp");
  TH1F*hx_txtyp_delphes = (TH1F*)hreconstructible_txtyp->ProjectionX();
  TH1F*hx_txtyp = (TH1F*)hreconstructed_txtyp->ProjectionX();
  hx_txtyp_delphes->Draw();
  hx_txtyp->SetLineColor(kRed);
  hx_txtyp->Draw("same");
  hx_txtyp_delphes->SetTitle("X distribution");
  hx_txtyp_delphes->GetXaxis()->SetTitle("x [mm]");

  TCanvas *cfg_txtypy = new TCanvas("cfg_txtypy");
  TH1F*hy_txtyp_delphes = (TH1F*)hreconstructible_txtyp->ProjectionY();
  TH1F*hy_txtyp = (TH1F*)hreconstructed_txtyp->ProjectionY();
  hy_txtyp_delphes->Draw();
  hy_txtyp->SetLineColor(kRed);
  hy_txtyp->Draw("same");
  hy_txtyp_delphes->SetTitle("Y distribution");
  hy_txtyp_delphes->GetXaxis()->SetTitle("y [mm]");
  
  TCanvas *cfg_txtypz = new TCanvas("cfg_txtypz");
  TH1F*hz_txtyp_delphes = (TH1F*)hreconstructible_txtyp->ProjectionZ();
  TH1F*hz_txtyp = (TH1F*)hreconstructed_txtyp->ProjectionZ();
  hz_txtyp_delphes->Draw();
  hz_txtyp->SetLineColor(kRed);
  hz_txtyp->Draw("same");


  TH1D *heff_hx = (TH1D*)hreconstructed_hx->Clone("heff_hx");
  TH1D *heff_hy = (TH1D*)hreconstructed_hy->Clone("heff_hy");
  TH1D *heff_hz = (TH1D*)hreconstructed_hz->Clone("heff_hz");

  TH1D *heff_txtyp_hx = (TH1D*)hreconstructed_txtyp_hx->Clone("heff_txtyp_hx");
  TH1D *heff_txtyp_hy = (TH1D*)hreconstructed_txtyp_hy->Clone("heff_txtyp_hy");
  TH1D *heff_txtyp_hz = (TH1D*)hreconstructed_txtyp_hz->Clone("heff_txtyp_hz");

  heff_hx->SetDirectory(0);
  heff_hy->SetDirectory(0);
  heff_hz->SetDirectory(0);
  heff_txtyp_hx->SetDirectory(0);
  heff_txtyp_hy->SetDirectory(0);
  heff_txtyp_hz->SetDirectory(0);
  
  heff_hx->Divide(heff_txtyp_hx);
  heff_hy->Divide(heff_txtyp_hy);
  heff_hz->Divide(heff_txtyp_hz);
  
  
  TCanvas *cweight = new TCanvas("cweight");
  cweight->Divide(3);
  cweight->cd(1);
  heff_hx->Draw();
  cweight->cd(2);
  heff_hy->Draw();
  cweight->cd(3);
  heff_hz->Draw();



  // TH1D *hratio_hx = (TH1D*)hreconstructed_txtyp_hx->Clone("hratio_hx");
  // TH1D *hratio_hy = (TH1D*)hreconstructed_txtyp_hy->Clone("hratio_hy");
  // TH1D *hratio_hz = (TH1D*)hreconstructed_txtyp_hz->Clone("hratio_hz");
  
  // hratio_hx->Divide(hreconstructed_hx);
  // hratio_hy->Divide(hreconstructed_hy);
  // hratio_hz->Divide(hreconstructed_hz);

  TH1D *hratio_hx = (TH1D*)hreconstructible_full_hx->Clone("hratio_hx");
  TH1D *hratio_hy = (TH1D*)hreconstructible_full_hy->Clone("hratio_hy");
  TH1D *hratio_hz = (TH1D*)hreconstructible_full_hz->Clone("hratio_hz");
  
  hratio_hx->Divide(hreconstructed_hx);
  hratio_hy->Divide(hreconstructed_hy);
  hratio_hz->Divide(hreconstructed_hz);


  
  TCanvas *cw = new TCanvas("cw");
  cw->Divide(3);
  cw->cd(1);
  hratio_hx->Draw();
  cw->cd(2);
  hratio_hy->Draw();
  cw->cd(3);
  hratio_hz->Draw();

  
}
