#!/opt/local/bin/python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import sys, os

if (len(sys.argv)<2):
    print("Specify a file please")
    sys.exit()

import ROOT as R
import pandas
import numpy as np
import root_pandas as rp


def LUTCreatorList(histos, fout):
    nbins = histos[0].GetNbinsX()
    fOut = open(fout,"w")
    for ibin in range(1,nbins):
        xmin = histos[0].GetBinLowEdge(ibin)
        xmax = histos[0].GetBinLowEdge(ibin) + histos[0].GetBinWidth(ibin)
        entry = "{0}\t{1}".format(xmin,xmax)
        for i in histos:
            mean = i.GetBinContent(ibin)
            err  = i.GetBinError(ibin)
            entry += "\t{0}\t{1}".format(mean,err)            
        entry += "\n"

        fOut.write(entry)
    fOut.close()
        


printFreq = 1000
tr_recoed  = rp.read_root(sys.argv[1],"TrackEffRes/TrackTree_recoed", where="type==3")

statesInTuple = [
    "EndVelo",
    "BegRich1",
    "EndRich1",
    "EndT",
    "BegRich2"
]

states = []

if(len(sys.argv)>2):
    if(sys.argv[2] == "all"):
        states = statesInTuple
    else:
        for i in range(2,len(sys.argv)):
            states.append(sys.argv[i])



nbinsP = 100
nbinsCov = 100

covProf = [ 0 for x in range(25)]    
for state in states:
    for i in range(25):
        covProf[i] = R.TProfile("covProf{0}_{1}".format(i,state),"covProf{0}_{1}".format(i,state),nbinsP,0,0.001)


for state in states:
    for i in range(len(tr_recoed)):
        if(i%printFreq == 0):
            print("event: {0}".format(i))
        cov = tr_recoed["cov_{0}".format(state)][i]
        index = 0
        for x in range(len(cov)):
            for y in range(len(cov[x])):
                covProf[index].Fill(1./tr_recoed["p_{0}".format(state)][i],cov[x][y],1)
                index+=1
    LUTCreatorList(covProf,"lutCovarianceProf_{0}.dat".format(state))
