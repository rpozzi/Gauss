/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Standard C
#include <dlfcn.h>
#include <iostream>

namespace Lamarr
{
  // external machine learning function 
  typedef float *(*mlfun) (float *, const float*); 
  typedef float *(*ganfun) (float *, const float*, const float*); 
  
  //////////////////////////////////////////////////////////////////////////////
  // External ML functions 
  template <typename fun_T>
  fun_T load_mlfun (
    const std::string& libname, 
    const std::string& entrypoint
    ) 
  {
    void* handle = dlopen (libname.c_str(), RTLD_LAZY);
    if (!handle)
    {
      std::cerr << "ERROR while loading library " << libname << std::endl;
      return nullptr;
    }

    fun_T ret = fun_T(dlsym(handle, entrypoint.c_str()));
    if (!ret)
    {
      std::cerr << "ERROR while loading symbol " << entrypoint << std::endl;
      return nullptr;
    }

    return ret;
  }
}