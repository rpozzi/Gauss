###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Sim/GiGaVisUI
-------------
#]=======================================================================]

gaudi_add_module(GiGaVisUI
    SOURCES
        src/GiGaSetVisAttributes.cpp
        src/GiGaUIsession.cpp
        src/GiGaVisManager.cpp
    LINK
        Geant4::G4Tree
        Geant4::G4RayTracer
        Geant4::G4interfaces
        Geant4::G4FR
        Geant4::G4VRML
        Geant4::G4OpenGL
        Geant4::G4vis_management
        LHCb::DetDescLib
        Gauss::GiGaLib
        Gauss::VisSvcLib
)

lhcb_env(SET G4DAWNFILE_DEST_DIR ../job/)
lhcb_env(SET G4DAWNFILE_MULTI_WINDOW 1)
lhcb_env(SET G4DAWNFILE_PS_VIEWER gv)
lhcb_env(SET G4DAWN_MULTI_WINDOW 1)
