#!env python
###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import GiGaSensDetTracker
from GaudiPython.Bindings import AppMgr
import cppyy
sensdet = GiGaSensDetTracker( 'ToolSvc.Hello', OutputLevel = -10)
bye = sensdet.addTool(GiGaSensDetTracker, 'Bye')
bye.OutputLevel = -10

gaudi = AppMgr()
blub = gaudi.toolSvc().create('GiGaSensDetTracker', 'Hello')
blub = gaudi.toolSvc().create('GiGaSensDetTracker', 'Hello.Bye')
gaudi.initialize()
