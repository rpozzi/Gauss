/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from Gaudi
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/PhysicalConstants.h"

// G4 
#include "G4Region.hh"
#include "G4LogicalVolume.hh"
#include "G4ProductionCuts.hh"
#include "G4FastSimulationManager.hh"

/// GiGaCnv 
#include "GiGaCnv/GiGaVolumeUtils.h"


#include "GiGa/GiGaToolBase.h"
#include "CaloDet/DeCalorimeter.h"
#include "GiGaCnv/GiGaVolumeUtils.h"

#include "GaussFastCaloPointlib/FastsimPointlibModel.h"
 

// ============================================================================
/*
 *
 *  Implementation file for the class GiGaFastRegionTool
 * 
 *  author: Matteo Rama - INFN Pisa
 *          Giacomo Vitali - SNS Pisa                                                                                                                              
 *          3 Mar 2020
 */
// ============================================================================

class GiGaFastRegionTool : public GiGaToolBase 
{
 public:  
  StatusCode process ( const std::string& region  = "" ) const override;
  GiGaFastRegionTool
    ( const std::string& type   , 
      const std::string& name   , 
      const IInterface*  parent ) ;
  
  /// virtual destructor 
  virtual ~GiGaFastRegionTool()= default;
  
 protected:
  // default constructor  is disabled 
  GiGaFastRegionTool();
  // copy    constructor  is disabled 
  GiGaFastRegionTool           ( const GiGaFastRegionTool& ) ;
  // assignement operator is disabled 
  GiGaFastRegionTool& operator=( const GiGaFastRegionTool& ) ;
  
private:
  typedef std::vector<std::string> Volumes;
  Gaudi::Property<std::string>     m_region{ this, "Region", "UNKNOWN" };
  Gaudi::Property<std::string>     m_libray{ this, "Library", "" };
  Gaudi::Property<Volumes>         m_volumes{ this, "Volumes", Volumes() };
  Gaudi::Property<bool>            m_overwrite{ this, "Overwrite", true };
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( GiGaFastRegionTool )

GiGaFastRegionTool::GiGaFastRegionTool
( const std::string& type   , 
  const std::string& name   , 
  const IInterface*  parent ) 
: GiGaToolBase( type , name , parent ) 
{
}
  

StatusCode GiGaFastRegionTool::process ( const std::string& /* region */ ) const 
{
  info()<<"*********** Initialising GiGaFastRegionTool ************"<<endmsg;

  // check the existence of the region
  G4Region* region = GiGaVolumeUtils::findRegion( m_region.value(), false ) ;
  if ( region ) 
    { 
      Warning ( " The Region '" + m_region + "'  already exist, skip " ).ignore() ;
      return StatusCode::SUCCESS ;
    }
  
  // create the new region
  region = new G4Region( m_region.value() ) ;
  
  // add volumes to the region 
  for( Volumes::const_iterator ivolume = m_volumes.value().begin() ;
       m_volumes.value().end() != ivolume ; ++ivolume ) 
    {
      G4LogicalVolume* volume = GiGaVolumeUtils::findLVolume( *ivolume ) ;
      if ( !volume ) { return Error( " G4LogicalVolume '" + ( *ivolume ) + "' is invalid " ); }
      if ( volume->GetRegion() && !m_overwrite.value() ) {
        Warning( " G4LogicalVolume '" + ( *ivolume ) + "' already belongs to region '" +
                 volume->GetRegion()->GetName() + "' , skip " )
            .ignore();
        continue;
      } else if ( volume->GetRegion() && m_overwrite.value() ) {
        Warning( " G4LogicalVolume '" + ( *ivolume ) + "' already belongs to region '" +
                 volume->GetRegion()->GetName() + "', overwrite " )
            .ignore();
      }
      // set region 
      volume -> SetRegion            ( region ) ;
      region -> AddRootLogicalVolume ( volume ) ;
    }
  
    //add the Fast Simulation Model
    FastsimPointlibModel* themodel = new FastsimPointlibModel( m_region.value(), region, m_libray.value() );
    themodel->initialize(); // NB: currently it does nothing
    // For now we just hardcode momentum range in which we want to apply
    // model, but might want to make it configurable in principle.
    themodel->setPRange( 20 * Gaudi::Units::MeV, 100000 * Gaudi::Units::MeV );

    // This would be nice to do using volume names we already hold.
    auto EcalDet = getDet<DeCalorimeter>( "/dd/Structure/LHCb/DownstreamRegion/Ecal" );
    if ( !EcalDet ) { error() << "Problem with getting ECAL DetDesc in GiGaFastRegionTool" << endmsg; }
    auto HcalDet = getDet<DeCalorimeter>( "/dd/Structure/LHCb/DownstreamRegion/Hcal" );
    if ( !HcalDet ) { error() << "Problem with getting HCAL DetDesc in GiGaFastRegionTool" << endmsg; }
    auto PrsDet = getDet<DeCalorimeter>( "/dd/Structure/LHCb/DownstreamRegion/Prs" );
    if ( !PrsDet ) { error() << "Problem with getting PRS DetDesc in GiGaFastRegionTool" << endmsg; }
    auto SpdDet = getDet<DeCalorimeter>( "/dd/Structure/LHCb/DownstreamRegion/Spd" );
    if ( !SpdDet ) { error() << "Problem with getting SPD DetDesc in GiGaFastRegionTool" << endmsg; }
    themodel->setCaloDB( SpdDet, PrsDet, EcalDet, HcalDet );

    info() << "*********** End Initialising GiGaFastRegionTool ************" << endmsg;

    return StatusCode::SUCCESS;
}

