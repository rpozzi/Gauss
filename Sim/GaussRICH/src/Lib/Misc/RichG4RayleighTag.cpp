/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichG4RayleighTag.cpp,v 1.2 2006-11-02 10:23:29 seaso Exp $
// Include files 

#include "GaussTools/GaussTrackInformation.h"
#include "GaussRICH/RichInfo.h"
#include "GaussRICH/RichPhotInfo.h"


// local
#include "GaussRICH/RichG4RayleighTag.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RichG4RayleighTag
//
// 2003-10-14 : Sajan EASO
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
G4int RichG4RayleighTag(const G4Track& aPhotTrack  ) {

   G4int NumRayleighTag=1;
   G4VUserTrackInformation* aUserTrackInfo=aPhotTrack.GetUserInformation();
   GaussTrackInformation* aRichPhotTrackInfo
        = (GaussTrackInformation*)aUserTrackInfo;
   
   if( aRichPhotTrackInfo) {
     if( aRichPhotTrackInfo->detInfo() ){

       RichInfo* aRichTypeInfo =
        ( RichInfo*) (aRichPhotTrackInfo->detInfo());
       if( aRichTypeInfo && aRichTypeInfo->HasUserPhotInfo() ){
         RichPhotInfo* aRichPhotInfo = 
                    aRichTypeInfo-> RichPhotInformation();
         if( aRichPhotInfo ) {
           aRichPhotInfo->bumpPhotonRayleighScatFlag();
	   NumRayleighTag =   aRichPhotInfo-> PhotonRayleighScatFlag();      
         }
         
         
       }
       
       
     }
     
     
   }
   
   return  NumRayleighTag;   
}

//=============================================================================
