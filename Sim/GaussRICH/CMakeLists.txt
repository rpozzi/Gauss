###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Sim/GaussRICH
-------------
#]=======================================================================]

gaudi_add_library(GaussRICHLib
    SOURCES
        src/Lib/Misc/RichG4AgelExitTag.cpp
        src/Lib/Misc/RichG4AnalysisPhotElec.cpp
        src/Lib/Misc/RichG4CherenkovPhotProdTag.cpp
        src/Lib/Misc/RichG4Counters.cpp
        src/Lib/Misc/RichG4CkvRecon.cpp
        src/Lib/Misc/RichG4EventHitCount.cpp
        src/Lib/Misc/RichG4HistoDefineSet1.cpp
        src/Lib/Misc/RichG4HistoDefineSet3.cpp
        src/Lib/Misc/RichG4HistoDefineTimer.cpp
        src/Lib/Misc/RichG4HistoFillSet1.cpp
        src/Lib/Misc/RichG4HistoFillSet2.cpp
        src/Lib/Misc/RichG4HistoFillSet3.cpp
        src/Lib/Misc/RichG4HistoFillSet4.cpp
        src/Lib/Misc/RichG4HistoFillSet5.cpp
        src/Lib/Misc/RichG4HistoFillTimer.cpp
        src/Lib/Misc/RichG4Hit.cpp
        src/Lib/Misc/RichG4HitCollName.cpp
        src/Lib/Misc/RichG4HitCoordResult.cpp
        src/Lib/Misc/RichG4HitRecon.cpp
        src/Lib/Misc/RichG4HpdReflTag.cpp
        src/Lib/Misc/RichG4HpdReflectionFlag.cpp
        src/Lib/Misc/RichG4InputMon.cpp
        src/Lib/Misc/RichG4MatRadIdentifier.cpp
        src/Lib/Misc/RichG4MirrorReflPointTag.cpp
        src/Lib/Misc/RichG4QwAnalysis.cpp
        src/Lib/Misc/RichG4RadiatorMaterialIdValues.cpp
        src/Lib/Misc/RichG4RayleighTag.cpp
        src/Lib/Misc/RichG4ReconFlatMirr.cpp
        src/Lib/Misc/RichG4ReconHpd.cpp
        src/Lib/Misc/RichG4ReconResult.cpp
        src/Lib/Misc/RichG4ReconTransformHpd.cpp
        src/Lib/Misc/RichG4StepAnalysis15.cpp
        src/Lib/Misc/RichG4StepAnalysis3.cpp
        src/Lib/Misc/RichG4StepAnalysis4.cpp
        src/Lib/Misc/RichG4StepAnalysis5.cpp
        src/Lib/Misc/RichG4StepAnalysis6.cpp
        src/Lib/Misc/RichG4SvcLocator.cpp
        src/Lib/Misc/RichG4TransformPhDet.cpp
        src/Lib/Misc/RichInfo.cpp
        src/Lib/Misc/RichPEInfo.cpp
        src/Lib/Misc/RichPEInfoAttach.cpp
        src/Lib/Misc/RichPhotInfo.cpp
        src/Lib/Misc/RichPhotInfoAttach.cpp
        src/Lib/Misc/RichScintilParamAdmin.cpp
        src/Lib/Misc/RichSolveQuarticEqn.cpp
        src/Lib/PhysPhotDet/RichHpdDeMag.cpp
        src/Lib/PhysPhotDet/RichHpdPSF.cpp
        src/Lib/PhysPhotDet/RichHpdPhotoElectricEffect.cpp
        src/Lib/PhysPhotDet/RichHpdProperties.cpp
        src/Lib/PhysPhotDet/RichHpdQE.cpp
        src/Lib/PhysPhotDet/RichHpdSiEnergyLoss.cpp
        src/Lib/PhysPhotDet/RichPhotoElectron.cpp
        src/Lib/srcG4/RichG4Cerenkov.cc
        src/Lib/srcG4/RichG4OpBoundaryProcess.cc
        src/Lib/srcG4/RichG4OpRayleigh.cc
        src/Lib/srcG4/RichG4Scintillation.cc
        src/Lib/SensDet/GetMCRichHitsAlg.cpp
        src/Lib/SensDet/GetMCRichInfoBase.cpp
        src/Lib/SensDet/GetMCRichOpticalPhotonsAlg.cpp
        src/Lib/SensDet/GetMCRichSegmentsAlg.cpp
        src/Lib/SensDet/GetMCRichTracksAlg.cpp
        src/Lib/SensDet/RichG4GeomProp.cpp
        src/Lib/SensDet/RichSensDet.cpp
    LINK
        PUBLIC
            ROOT::Tree
            LHCb::RichDetLib
            LHCb::LinkerEvent
            LHCb::MCEvent
            LHCb::RelationsLib
            LHCb::RichKernelLib
            Gauss::GaussToolsLib
)

gaudi_add_module(GaussRICH
    SOURCES
        src/Components/Factories.cpp
        src/Components/PhysProcess/GiGaPhysConstructorHpd.cpp
        src/Components/PhysProcess/GiGaPhysConstructorOp.cpp
        src/Components/RichAnalysis/RichG4CherenkovAnalysis.cpp
        src/Components/RichAnalysis/RichG4HistoDefineSet2.cpp
        src/Components/RichAnalysis/RichG4HistoDefineSet4.cpp
        src/Components/RichAnalysis/RichG4HistoDefineSet5.cpp
        src/Components/RichAnalysis/RichG4ScintAnalysis.cpp
        src/Components/RichAction/Rich1G4TrackActionUpstrPhoton.cpp
        src/Components/RichAction/RichG4EventAction.cpp
        src/Components/RichAction/RichG4RunAction.cpp
        src/Components/RichAction/RichG4TrackAction.cpp
        src/Components/RichAction/RichG4TrackActionAerogelPhoton.cpp
        src/Components/RichAction/RichG4TrackActionPhotOpt.cpp
        src/Components/RichAction/RichG4TrackActionRich2DbgPhotonTrack.cpp
        src/Components/RichAction/RichG4TrackActionRich2Photon.cpp
        src/Components/Assoc/MCPartToMCRichTrackAlg.cpp
        src/Components/Assoc/MCRichHitToMCRichOpPhotAlg.cpp
    LINK
        Gauss::GaussRICHLib
    GENCONF_OPTIONS
        --load-library=GaussToolsGenConfHelperLib
)
add_dependencies(GaussRICH Gauss::GaussToolsGenConfHelperLib)

gaudi_install(PYTHON)

gaudi_generate_confuserdb()
lhcb_add_confuser_dependencies(
    :GaussRICH
    Sim/GiGa:GiGa
)
