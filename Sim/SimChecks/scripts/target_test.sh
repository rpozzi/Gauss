#!/usr/bin/env sh
###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
echo "Running Target Test with All Options"

make_events_script=$SIMCHECKSROOT/options/Target/MakeEvents.py

python $make_events_script --physList "[QGSP_BERT, FTFP_BERT]" --materialList "[Al,Si,Be]" --thicknessList "[1,5,10]" --pgunList "[Piminus, Piplus, Kminus, Kplus, p, pbar]" --energyList "[1,2,5,10,50,100]"
echo "WARNING: THIS TEST RETURNS 0 REGARDLESS OF ERRORS. FIX REQUIRED."
exit 0
