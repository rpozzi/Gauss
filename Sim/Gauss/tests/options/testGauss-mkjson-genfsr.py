###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Configuration import *

importOptions("$GAUSSOPTS/Gauss-2018.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
importOptions("$DECFILESROOT/options/22960000.py")
#importOptions("$DECFILESROOT/options/10063000.py")
importOptions("$GAUSSOPTS/GenStandAlone.py")

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = 1955

from Configurables import GaudiSequencer, GenFSRJson, GenFSRLog
seqGenFSR = GaudiSequencer("GenFSRSeq")
gfsrWriter = GenFSRJson()
gfsrWriter.prodID = "17"
gfsrWriter.appConfigFile = "Sim10-Beam6500GeV-md100-2018-nu1.6"
gfsrWriter.appConfigVersion = "v3r409"
gfsrWriter.gaussVersion = "v55r3"
gfsrWriter.simCond = LHCbApp().CondDBtag
gfsrWriter.dddb = LHCbApp().DDDBtag
gfsrWriter.jsonOutputName = "GenerationFSR-noref.json"

gfsrXmlWriter = GenFSRLog()

seqGenFSR.Members += [gfsrWriter, gfsrXmlWriter]
    
ApplicationMgr().TopAlg += [seqGenFSR]

#--Number of events
nEvts = 10

from Configurables import LHCbApp
LHCbApp().EvtMax = nEvts
LHCbApp().Simulation = True

idFile = "genFSR_2018_Gauss_mkjson"
OutputStream("GaussTape").Output = "DATAFILE='PFN:%s.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'" %idFile
HistogramPersistencySvc().OutputFile = ""   # no histos
