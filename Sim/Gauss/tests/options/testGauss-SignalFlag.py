###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
importOptions('$GAUSSOPTS/Gauss-2018.py')
importOptions('$DECFILESROOT/options/12113001.py')
importOptions('$LBPYTHIA8ROOT/options/Pythia8.py')

from Configurables import GaudiSequencer
GenMonitor = GaudiSequencer( "GenMonitor" )
SimMonitor = GaudiSequencer( "SimMonitor" )
GenMonitor.Members.append( "DumpHepMC" )
SimMonitor.Members.append( "PrintMCDecayTreeAlg" )
from Configurables import PrintMCDecayTreeAlg, PrintMCDecayTreeTool
printAlg = PrintMCDecayTreeAlg()
printAlg.addTool(PrintMCDecayTreeTool)
printAlg.PrintMCDecayTreeTool.Information = "Name E M P Pt phi Vz flags fromSignal"

from Configurables import LHCbApp
LHCbApp().EvtMax = 3
