###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# File for running Gauss with latest tags, using the default configuration
#
# Syntax is:
#   gaudirun.py Gauss-DEV.py <someInputJobConfiguration>.py
##############################################################################

from Configurables import Gauss
theApp = Gauss()

#--Use latest changes in DDDB and SIMCOND for 2018 
from Configurables import LHCbApp

LHCbApp().DDDBtag   = "dt-2018"   # Must be set for selecting VELO
                                  # geometry list in Gauss()
LHCbApp().CondDBtag = "Sim10/2018" 
Gauss().DataType    = "2018"

