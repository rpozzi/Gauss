/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4AntiXiccMinus_h
#define G4AntiXiccMinus_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"

// ######################################################################
// ###                         AntiXiccMinus                        ###
// ######################################################################

class G4AntiXiccMinus : public G4ParticleDefinition
{
 private:
  static G4AntiXiccMinus * theInstance ;
  G4AntiXiccMinus( ) { }
  ~G4AntiXiccMinus( ) { }


 public:
  static G4AntiXiccMinus * Definition() ;
  static G4AntiXiccMinus * AntiXiccMinusDefinition() ;
  static G4AntiXiccMinus * AntiXiccMinus() ;
};


#endif
