/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/Vector4DTypes.h"

// LHCb
#include "Event/MCHit.h"
#include "Event/MCParticle.h"

/** @class VPGaussMoni VPGaussMoni.h
 *  Algorithm run in Gauss for monitoring VP MCHits.
 *
 *  @author Victor Coco based on VeloGaussMoni
 *  @date   2009-06-05
 */

class VPGaussMoni : public GaudiTupleAlg {
public:
  /// Standard constructor
  VPGaussMoni( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiTupleAlg( name, pSvcLocator ){};

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  /// Create monitoring plots
  void monitor();

  /// Create monitoring tuple if required
  void makeTuple();

  /// TES location of VP MCHits container
  Gaudi::Property<std::string> m_hitsLocation{this, "HitLocation", LHCb::MCHitLocation::VP};
  /// Flag to activate printout of each hit
  Gaudi::Property<bool> m_printInfo{this, "PrintInfo", false};
  /// Flag to activate additional set of histograms
  Gaudi::Property<bool> m_detailedMonitor{this, "DetailedMonitor", true};
  /// Make a Tuple of the MCHit info (x,y,z,dx,dy,dz and SensID)
  Gaudi::Property<bool> m_makeTuple{this, "MakeTuple", false};
  /// Total number of MC hits
  double m_nMCH = 0.;
  /// Total number of MC hits squared
  double m_nMCH2 = 0.;
  /// Number of events
  unsigned int m_nEvent = 0.;
  /// Container of VP MCHits
  LHCb::MCHits* m_hits = {};
};

DECLARE_COMPONENT( VPGaussMoni )

//=============================================================================
// Initialization
//=============================================================================
StatusCode VPGaussMoni::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if (sc.isFailure()) return sc;
  setHistoTopDir("VP/");
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VPGaussMoni::execute() {

  ++m_nEvent;
  // Get data.
  m_hits = getIfExists<LHCb::MCHits>(m_hitsLocation);
  if (!m_hits) {
    error() << "No MCHits at " << m_hitsLocation << endmsg;
  } else {
    if (m_printInfo) {
      info() << "--------------------------------------------------" << endmsg;
      info() << " ==> Found " << m_hits->size() << " VP MCHits" << endmsg;
      info() << "--------------------------------------------------" << endmsg;
    }
    monitor();
    if ( m_makeTuple ) makeTuple();
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VPGaussMoni::finalize() {

  m_nMCH /= m_nEvent;
  m_nMCH2 /= m_nEvent;
  const double err = sqrt((m_nMCH2 - (m_nMCH * m_nMCH)) / m_nEvent);

  info() << "------------------------------------------------------" << endmsg;
  info() << "                - VPGaussMoni table -                 " << endmsg;
  info() << "------------------------------------------------------" << endmsg;
  if (m_nMCH > 0) {
    info() << " Number of MCHits/Event: " << m_nMCH << "+/-" << err << endmsg;
  } else {
    info() << " ==> No MCHits found! " << endmsg;
  }
  info() << "------------------------------------------------------" << endmsg;
  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
// Create monitoring plots
//=============================================================================
void VPGaussMoni::monitor() {

  const double size = m_hits->size();
  plot( size, "nMCHits", "Number of hits in VP per event", 0., 3000., 100 );
  if ( size == 0 ) return; // skip empty events
  m_nMCH += size;
  m_nMCH2 += size * size;
  plot( log10( size ), "lognMCHits", "log10(Number of hits in VP per event)", 0., 8., 80 );
  if (!m_detailedMonitor) return;
  // Loop over all MCHits in the container.
  LHCb::MCHits::iterator it;
  for (it = m_hits->begin(); it != m_hits->end(); ++it) {
    if (m_printInfo) {
      info() << " ==> Test MCHit:\n"
             << " sensor: " << ((*it)->sensDetID())
             << "\nentry: x= " << ((*it)->entry()).x()/Gaudi::Units::mm << " mm"
             << "\nentry: y= " << ((*it)->entry()).y()/Gaudi::Units::mm << " mm"
             << "\nentry: z= " << ((*it)->entry()).z()/Gaudi::Units::mm << " mm"
             << "\nexit: x= " << ((*it)->exit()).x()/Gaudi::Units::mm << " mm"
             << "\nexit: y= " << ((*it)->exit()).y()/Gaudi::Units::mm << " mm"
             << "\nexit: z= " << ((*it)->exit()).z()/Gaudi::Units::mm << " mm"
             << "energy: " << ((*it)->energy())/Gaudi::Units::eV << " eV"
             << "TOF: " << ((*it)->time())/Gaudi::Units::ns << " ns"
             << endmsg;
    }
    plot((*it)->energy() / Gaudi::Units::eV, "eDepSi",
         "Energy deposited in Si [eV]",
         0., 300000., 100);
    double x = (*it)->entry().x() / Gaudi::Units::cm;
    double y = (*it)->entry().y() / Gaudi::Units::cm;
    double z = (*it)->entry().z() / Gaudi::Units::cm;
    plot2D(z, x, "entryZX",
           "Particle entry point in Si [cm] - ZX plane",
           -30., 80., -5., 5., 1100, 50);
    plot2D(x, y, "entryXY",
           "Particle entry point in Si [cm] - XY plane",
           -5., 5., -5., 5., 500, 500);
    x = (*it)->exit().x() / Gaudi::Units::cm;
    y = (*it)->exit().y() / Gaudi::Units::cm;
    z = (*it)->exit().z() / Gaudi::Units::cm;
    plot2D(z, x, "exitZX",
           "Particle exit point in Si [cm] - ZX plane",
           -30., 80., -5., 5., 1100, 50);
    plot2D(x, y, "exitXY",
           "Particle exit point in Si [cm] - XY plane",
           -5., 5., -5., 5., 500, 500);
    plot((*it)->time() / Gaudi::Units::ns, "TOF",
         "Time Of Flight [ns]", 
         0., 50., 100);
    // Get access to the MCParticle which made the hit, and write out 4-mom
    const LHCb::MCParticle* particle = (*it)->mcParticle();
    if (!particle) continue;
    Gaudi::LorentzVector fMom = particle->momentum();
    plot(fMom.e() /Gaudi::Units::GeV, "eMCPart",
         "Particle energy [GeV]",
         0., 50., 100);       
    // Get the production vertex position
    const LHCb::MCVertex* vert = particle->originVertex();
    x = vert->position().x() / Gaudi::Units::cm;
    y = vert->position().y() / Gaudi::Units::cm;
    z = vert->position().z() / Gaudi::Units::cm;
    plot2D(x, y, "MCVertexPosXY",
           "Position of production MC Vertex of MCParticles giving hits - XY [cm]",
           -5., 5., -5., 5., 500, 500);      
    plot2D(z, x, "MCVertexPosZX",
           "Position of production MC Vertex of MCParticles giving hits - ZX [cm]",
           -30., 80., -5., 5., 500, 500); 
    plot2D(z, y, "MCVertexPosZY",
           "Position of production MC Vertex of MCParticles giving hits - ZY [cm]",
           -30., 80., -5., 5., 500, 500);
    
    // Get the distance traversed through silicon for each particle
    double xdist2 = pow((*it)->entry().x()-(*it)->exit().x(),2);
    double ydist2 = pow((*it)->entry().y()-(*it)->exit().y(),2);
    double zdist2 = pow((*it)->entry().z()-(*it)->exit().z(),2);
    double dist = sqrt(xdist2+ydist2+zdist2) / Gaudi::Units::cm;
    plot(dist, "SiDist", "Distance traversed through silicon by particle [cm]", 0., .05, 500.);

    if (m_printInfo) {
      info() << " ==> MCHit - MCParticle: "
             << "\np_x = " << fMom.px() / Gaudi::Units::GeV
             << "\np_y = " << fMom.py() / Gaudi::Units::GeV
             << "\np_z = " << fMom.pz() / Gaudi::Units::GeV
             << endmsg;
    }
  }
}

//=============================================================================
// Create monitoring tuple
//=============================================================================
void VPGaussMoni::makeTuple() {
  Tuple tuple = nTuple( "MCHitInfo" );
  for ( auto it = m_hits->begin(); it != m_hits->end(); ++it ) {
    auto const& hit = **it;
    // ignore all errors from tuple filling, nothing we can do here
    tuple->column( "Hitx", hit.midPoint().x() ).ignore();
    tuple->column( "Hity", hit.midPoint().y() ).ignore();
    tuple->column( "Hitz", hit.midPoint().z() ).ignore();
    tuple->column( "HitDx", hit.displacement().x() ).ignore();
    tuple->column( "HitDy", hit.displacement().y() ).ignore();
    tuple->column( "HitDz", hit.displacement().z() ).ignore();
    tuple->column( "sensID", hit.sensDetID() ).ignore();
    tuple->write().ignore();
  }
}
