###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Try to find LHAPDF
# Defines:
#
#  LHAPDF_FOUND
#  LHAPDF_INCLUDE_DIR
#  LHAPDF_INCLUDE_DIRS (not cached)
#  LHAPDF_LIBRARY
#  LHAPDF_LIBRARIES (not cached)
#  LHAPDF_LIBRARY_DIRS (not cached)
#
#  Targets:
#    LHAPDF::LHAPDF

find_library(LHAPDF_LIBRARY NAMES LHAPDF
             HINTS $ENV{LHAPDF_ROOT_DIR}/lib ${LHAPDF_ROOT_DIR}/lib)

find_path(LHAPDF_INCLUDE_DIR LHAPDF/LHAPDF.h
          HINTS $ENV{LHAPDF_ROOT_DIR}/include ${LHAPDF_ROOT_DIR}/include)

find_program(LHAPDF_EXECUTABLE NAMES lhapdf-config)

mark_as_advanced(LHAPDF_INCLUDE_DIR LHAPDF_LIBRARY LHAPDF_EXECUTABLE)

# handle the QUIETLY and REQUIRED arguments and set LHAPDF_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LHAPDF DEFAULT_MSG LHAPDF_INCLUDE_DIR LHAPDF_LIBRARY LHAPDF_EXECUTABLE)

set(LHAPDF_LIBRARIES ${LHAPDF_LIBRARY})
get_filename_component(LHAPDF_LIBRARY_DIRS ${LHAPDF_LIBRARY} PATH)

set(LHAPDF_INCLUDE_DIRS ${LHAPDF_INCLUDE_DIR})

get_filename_component(LHAPDF_BINARY_PATH ${LHAPDF_EXECUTABLE} PATH)

mark_as_advanced(LHAPDF_FOUND)

if(LHAPDF_FOUND AND NOT TARGET LHAPDF::LHAPDF)
  add_library(LHAPDF::LHAPDF UNKNOWN IMPORTED)
  set_target_properties(LHAPDF::LHAPDF PROPERTIES IMPORTED_LOCATION ${LHAPDF_LIBRARIES})
  target_include_directories(LHAPDF::LHAPDF SYSTEM INTERFACE ${LHAPDF_INCLUDE_DIR})
endif()
