###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Try to find Rivet
# Defines:
#
#  RIVET_FOUND
#  RIVET_INCLUDE_DIR
#  RIVET_INCLUDE_DIRS (not cached)
#  RIVET_LIBRARY
#  RIVET_LIBRARIES (not cached)
#  RIVET_LIBRARY_DIRS (not cached)
#
#  Targets:
#    Rivet::Rivet

find_library(RIVET_LIBRARY NAMES Rivet)

find_path(RIVET_INCLUDE_DIR Rivet/Rivet.hh)

find_program(RIVET_EXECUTABLE NAMES rivet)
find_program(RIVET_build_EXECUTABLE NAMES rivet-build rivet-buildplugin)

mark_as_advanced(RIVET_INCLUDE_DIR RIVET_LIBRARY RIVET_EXECUTABLE RIVET_build_EXECUTABLE)

# handle the QUIETLY and REQUIRED arguments and set Rivet_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Rivet DEFAULT_MSG RIVET_INCLUDE_DIR RIVET_LIBRARY RIVET_EXECUTABLE RIVET_build_EXECUTABLE)

set(RIVET_LIBRARIES ${RIVET_LIBRARY})
get_filename_component(RIVET_LIBRARY_DIRS ${RIVET_LIBRARY} PATH)

set(RIVET_INCLUDE_DIRS ${RIVET_INCLUDE_DIR})

get_filename_component(RIVET_BINARY_PATH ${RIVET_EXECUTABLE} PATH)
get_filename_component(RIVET_HOME ${RIVET_BINARY_PATH} PATH)

mark_as_advanced(Rivet_FOUND)

# add RIVET interface to PYTHONPATH
set(RIVET_PYTHON_PATH ${RIVET_HOME}/lib/python${Python_config_version_twodigit}/site-packages)

if(Rivet_FOUND AND NOT TARGET Rivet::Rivet)
  add_library(Rivet::Rivet UNKNOWN IMPORTED)
  set_target_properties(Rivet::Rivet PROPERTIES IMPORTED_LOCATION ${RIVET_LIBRARIES})
  target_include_directories(Rivet::Rivet SYSTEM INTERFACE ${RIVET_INCLUDE_DIR})
endif()
