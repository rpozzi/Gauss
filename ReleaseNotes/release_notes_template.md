{% set categories = [
    'Configuration',
    'Detector simulation', 'Muon', 'Calo', 'RICH',
    'PID', 'Composites', 'Filters', 'Functors',
    'Event model', 'Persistency',
    'MC checking', 'Monitoring',
    'Core', 'Conditions', 'Utilities',
    'Simulation',  'Tuples', 'Accelerators',
    'Geant4',
    'Fast Simulations', 
    'Generators', 'Decays', 'Pythia8', 'IFT',
    'Upgrade', 'Run1 and 2',
    'Build',
    ] -%}
{% set used_mrs = [] -%}
{% macro section(labels, used=used_mrs) -%}
{% for mr in order_by_label(select_mrs(merge_requests, labels, used), categories) -%}
  {% set mr_labels = categories|select("in", mr.labels)|list -%}
- {% if mr_labels %}{{mr_labels|map('label_ref')|join(' ')}} | {% endif -%}
  {{mr.title|sentence}}, {{mr.reference}} (@{{mr.author.username}}){% if mr.issue_refs %} [{{mr.issue_refs|join(',')}}]{% endif %}{% if 'highlight' in mr.labels %} :star:{% endif %}
{# {{mr.description|mdindent(2)}} -#}
{% endfor -%}
{% endmacro -%}
{{date}} {{project}} {{version}}
===

This version uses 
{{project_deps[:-1]|join(',\n')}} and
{{project_deps|last}}.

The following generators are used from {{project_deps|last}}:
- pythia8   244.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- madgraph  2.9.3.atlas1
- herwig++  7.2.1,      thepeg 2.2.1
- crmc      1.8.0.lhcb, starlight r313
- rivet     3.1.4,     yoda   1.9.0   

while the followings are built privately:
- EvtGen with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB
- Hijing (1.383bs.2)


This version is released on `master` branch.
Built relative to {{project}} [{{project_prev_tag}}](../-/tags/{{project_prev_tag}}), with the following changes:

### New features ~"new feature"

{{ section(['new feature']) }}

### Fixes ~"bug fix" ~workaround

{{ section(['bug fix', 'workaround']) }}

### Enhancements ~enhancement

{{ section(['enhancement']) }}

### Code optimisation and speed up ~optimisation

{{ section(['optimisation']) }} 

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

{{ section(['cleanup', 'modernisation', 'testing']) }}

### Documentation ~Documentation

{# Collect documentation independently, may lead to repeated entries -#}
{{ section(['Documentation'], used=None) }}
{# Mark as used such documentation does not appear under Other -#}
{% set dummy = section(['Documentation']) -%}

### Other

{{ section([[]]) }}
