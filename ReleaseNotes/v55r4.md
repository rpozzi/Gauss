2022-03-25 Gauss v55r4
===

This version uses 
Geant4 [v106r2p5](../../../../Geant4/-/tags/v106r2p5),
Run2Support [v2r1](../../../../Run2Support/-/tags/v2r1),
LHCb [v53r6](../../../../LHCb/-/tags/v53r6),
Gaudi [v36r4](../../../../Gaudi/-/tags/v36r4) and
LCG [101_LHCB_7](http://lcginfo.cern.ch/release/101_LHCB_7/) with ROOT 6.24.06.

The following generators are used from LCG [101_LHCB_7](http://lcginfo.cern.ch/release/101_LHCB_7/) with ROOT 6.24.06:
- pythia8   244.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- madgraph  2.9.3.atlas1
- herwig++  7.2.1,      thepeg 2.2.1
- crmc      1.8.0.lhcb, starlight r313
- rivet     3.1.4,     yoda   1.9.0   

while the followings are built privately:
- EvtGen with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB
- Hijing (1.383bs.2)


This version is released on `master` branch.
Built relative to Gauss [v55r3](../-/tags/v55r3), with the following changes:

### New features ~"new feature"

- ~"Detector simulation" | Add a possibility to export a GDML with extra info about regions, !803 (@mimazure)
- ~Simulation | Preliminary QMtest for GenFSRJson component, !821 (@agrecu)
- ~Generators ~Pythia8 | Added a new userhook to Pythia8, !770 (@msingla)


### Fixes ~"bug fix" ~workaround

- ~"Detector simulation" ~Geant4 | Fix MaterialEval.h due to m_xmax->Ymin, m_ymin->Xmax typo, !835 (@bsiddi)
- ~Generators | Fix initialisation of generator counters in Sim10, !834 (@kreps)
- ~Generators | Fix the modernization of properties in MaterialEval, !826 (@mimazure)
- ~Generators ~Decays | Fix ExtraParticlesInAcceptance for AllFromSameB = True, !797 (@gcorti)


### Enhancements ~enhancement

- ~"Detector simulation" ~Calo ~Monitoring ~Simulation ~Accelerators | Extend EcalCollector to CaloCollector with multiple calorimeters, !808 (@mimazure)
- ~"Detector simulation" ~Conditions ~Geant4 ~Upgrade | Make Gauss compatible with VP misalignments, !734 (@hcroft)
- ~Generators ~IFT | Mother particle for non-resonant di-leptons, !795 (@gcorti)
- ~Build | Get libtensorflow from LHCb mirror, !806 (@clemenci)


### Code optimisation and speed up ~optimisation

 

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Calo | Adapted to changes in the Calo coming from the move to DD4hep, !766 (@sponce)
- ~Geant4 | Do not use 'Geant4/' prefix when including Geant4 headers, !807 (@clemenci)
- ~"Fast Simulations" | Remove GaudiTensorFlow no longer used by Lamarr, !832 (@gcorti) [#45]
- ~"Fast Simulations" | Adapt Lamarr to changes in LHCb > v53r1, !791 (@landerli)
- ~Generators | Update referecences due to changes in log files, !825 (@gcorti)
- ~Generators | Upgrade Gaudi algorithm to new properties. Event number issues solved in RIVET 3. Enhance tests., !815 (@agrecu)
- ~Generators ~Decays | AmpGen / LbAmpGen cleanup, !670 (@tevans)
- Update tests for new Sim10 release, !818 (@gcorti)
- Move away from using CERN-SWTEST, !805 (@rmatev)
- First part of the modernization changes from MR !711, !790 (@kreps)
- Adapted to removal of GaudiAlgorithm in LHCb packers, !763 (@sponce)
- Follow changes in LHCb!3150, !759 (@graven)


### Documentation ~Documentation


### Other

