/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MuonMultipleScatteringChecker.h,v 1.1 2009-04-29 14:23:21 svecchi Exp $
#ifndef MUONMULTIPLESCATTERINGCHECKER_H
#define MUONMULTIPLESCATTERINGCHECKER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
// for Muons
#include "Event/MCHit.h"
#include "Event/MCHeader.h"

/** @class MuonMultipleScatteringChecker MuonMultipleScatteringChecker.h
 *
 *
 *  @author Stefania Vecchi
 *  @date   2009-03-12
 */
class MuonMultipleScatteringChecker : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

  Gaudi::Property<bool> m_fillNtuple{this,"fillNtuple",false,"Fill the ntuple"};

protected:

private:
  Gaudi::Property<std::string> m_mcHeader{this,"MCHeader",LHCb::MCHeaderLocation::Default ,"Location of MC Header"} ; ///< Location of MC Header
  Gaudi::Property<std::string> m_muonHits{this,"MuonHits",LHCb::MCHitLocation::Muon ,"Location of muon hits"} ; ///< Location of muon hits

};
#endif // MUONMULTIPLESCATTERINGCHECKER_H
