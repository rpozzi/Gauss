/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MuonHitChecker.h,v 1.6 2009-03-26 21:59:02 robbep Exp $
#ifndef MuonHitChecker_H
#define MuonHitChecker_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
// Event
#include "Event/MCHeader.h"
// for Muons
#include "Event/MCHit.h"

/** @class MuonHitChecker MuonHitChecker.h
 *
 *
 *  @author A Sarti
 *  @date   2005-05-20
 */
class MuonHitChecker : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

  Gaudi::Property<bool> m_detailedMonitor{this,"DetailedMonitor",false,"Detailed Monitor"}; //was in private

protected:

  std::vector<int> m_numberOfGaps;

private:

  int nhit[5][4],cnt[5][4];
  int nhit_ri[5],cnt_ri[5];


  int m_hit_outside_gaps{0};

  Gaudi::Property<std::string> m_mcHeader{this,"MCHeader",LHCb::MCHeaderLocation::Default,"Location of MC Header"} ; ///< Location of MC Header
  Gaudi::Property<std::string> m_muonHits{this,"MuonHits",LHCb::MCHitLocation::Muon,"Location of muon hits"} ; ///< Location of muon hits
};
#endif // MuonHitChecker_H
