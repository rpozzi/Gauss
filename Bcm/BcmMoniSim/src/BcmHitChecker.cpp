/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiAlg/Tuples.h"
#include "Event/MCParticle.h"
#include "Event/MCHeader.h"

// local
#include "BcmHitChecker.h"

DECLARE_COMPONENT( BcmHitChecker )


//=============================================================================
// Initialization
//=============================================================================
StatusCode BcmHitChecker::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  m_bcmDet=( getDet<DeBcm>( m_bcmDetLocation.value() ) );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode BcmHitChecker::execute() {

  debug() << "==> Execute" << endmsg;

  getData().ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  bcmHitMonitor().ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode BcmHitChecker::finalize() {
  return GaudiAlgorithm::finalize();
}

//=============================================================================
//  GetData
//=============================================================================
StatusCode BcmHitChecker::getData() {
  debug()<< " ==> BcmHitChecker::getData" <<endmsg;

  if(!exist<LHCb::MCHits>(m_bcmHitsLocation.value())){ // Should be replaced with LHCb::MCHitLocation::Bcm
    error()<< "There is no MCHits at " << m_bcmHitsLocation.value() << " in TES!"
           << endmsg;
  }else{
    m_bcmMCHits=get<LHCb::MCHits>(m_bcmHitsLocation.value());
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Plot
//=============================================================================
StatusCode BcmHitChecker::bcmHitMonitor(){
  debug()<< " ==> BcmHitChecker::bcmHitMonitor" <<endmsg;

  LHCb::MCHits::iterator It;
  for(It=m_bcmMCHits->begin(); It!=m_bcmMCHits->end(); It++){
    // Check if hit is in the correct bcm station
    if( checkStation(*It) ){
      plot((*It)->energy()/Gaudi::Units::eV, "enDep",
          "Energy deposited in Diamond [eV]", 0., 500000., 100);
      plot2D((*It)->entry().x()/Gaudi::Units::mm,
          (*It)->entry().y()/Gaudi::Units::mm, "entryXY",
          "Particle entry point in Diamond [mm] - XY plane",
          -80., 80., -80., 80., 160, 160);
      // MC particle energy
      const LHCb::MCParticle* myMCParticle=(*It)->mcParticle();
      if(0!=myMCParticle){
        Gaudi::LorentzVector fMom=myMCParticle->momentum();
        plot(fMom.e()/Gaudi::Units::GeV, "eMCPart",
            "Particle energy [GeV]", 0., 50., 100);
      }

      // Tuple
      if( m_detailedMonitor.value() ){
        const LHCb::MCHeader* evt = get<LHCb::MCHeader>(LHCb::MCHeaderLocation::Default);
        long nEvt = evt->evtNumber();

        Tuple tuple = nTuple ( 100 , "Hit info" ) ;

        // fill N-Tuple with hit data:
        tuple -> column ( "Event", nEvt ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        // "~(~0u << N)" is equivalent to "(1 << N) - 1", i.e. a mask selecting the bits from 0 to N-1
        tuple -> column ( "Sensor", (~(~0u<<DeBcmShifts::shiftStationID))&((*It)->sensDetID()) ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "Path", (*It)->pathLength() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "EnDep", (*It)->energy() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "xIn", (*It)->entry().x() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "yIn", (*It)->entry().y() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "zIn", (*It)->entry().z() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "rIn", ( sqrt( (*It)->entry().x()*(*It)->entry().x() + (*It)->entry().y()*(*It)->entry().y() ) ) ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "xOut", (*It)->exit().x() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "yOut", (*It)->exit().y() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "zOut", (*It)->exit().z() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "rOut", ( sqrt( (*It)->exit().x()*(*It)->exit().x() + (*It)->exit().y()*(*It)->exit().y() ) ) ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "arTime", (*It)->time() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

        // fill N-Tuple with particle data
        tuple -> column ( "pid", (*It)->mcParticle()->particleID().pid() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "charge", (*It)->mcParticle()->particleID().threeCharge()/3 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "px", (*It)->mcParticle()->momentum().px() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "py", (*It)->mcParticle()->momentum().py() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "pz", (*It)->mcParticle()->momentum().pz() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "E", (*It)->mcParticle()->momentum().E() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

        // fill N-tuple with primary vertex data
        tuple -> column ( "primx", (*It)->mcParticle()->primaryVertex()->position().x() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "primy", (*It)->mcParticle()->primaryVertex()->position().y() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> column ( "primz", (*It)->mcParticle()->primaryVertex()->position().z() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        tuple -> write ().ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      }
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Check if hit is in the monitored BCM station
//=============================================================================
bool BcmHitChecker::checkStation(LHCb::MCHit* Hit){
  int nStation = m_bcmDet->stationNumber();
  int hitSensor = Hit->sensDetID();
  int hitStation = (hitSensor>>DeBcmShifts::shiftStationID);
  return nStation == hitStation;
}






